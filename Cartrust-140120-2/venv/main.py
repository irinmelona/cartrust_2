# -*- coding: utf-8 -*-
from app.app import app
from routes.startRoutes import *
import yaml

with open(r'..\configs\configs.yaml','r') as f:
    data_configs = yaml.safe_load(f)

init_routes()
init_routes_bank()
init_routes_customer()
init_routes_login()
init_routes_team()
init_routes_user()
init_routes_car()
init_routes_case()
app.run(host=str(data_configs['url']['host']), port=int(data_configs['url']['port']))
