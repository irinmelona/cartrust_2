# -*- coding: utf-8 -*-

from app.app import app
from routes.startRoutes import *
import yaml

with open(r'configs/configs.yaml','r') as f:
    data_configs = yaml.safe_load(f)

init_routes()
init_routes_bank()
init_routes_customer()
init_routes_login()
init_routes_team()
init_routes_user()
init_routes_car()
init_routes_case()
init_routes_excel()
init_routes_dashboard()
init_routes_dropdown()
init_routes_other()
init_routes_cardd()

app.run(debug=True, host=str(data_configs['url']['host']), port=int(data_configs['url']['port']))

