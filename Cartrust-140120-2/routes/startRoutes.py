# -*- coding: utf-8 -*-
from app.app import app
from routes.routes import vm_routes as vm_route
from model.bank.routing import vm_routes_bank as vm_route_bank
from model.customer.routing import vm_routes_customer as vm_route_customer
from model.login.routing import vm_routes_login as vm_route_login
from model.team.routing import vm_routes_team as vm_route_team
from model.user.routing import vm_routes_user as vm_route_user
from model.car.routing import vm_routes_car as vm_route_car
from model.customer_case.routing import vm_routes_case as vm_route_case
from controller.excel_file.routing import vm_routes_excel as vm_route_excel
from model.dashboard.routing import vm_routes_dashboard as vm_route_dashboard
from model.dropdown.routing import vm_routes_dropdown as vm_route_dropdown
from model.other_function.routing import vm_routes_other as vm_route_other
from model.car_dropdown.routing import vm_routes_cardd as vm_route_cardd

def root_endpoint():
    return 'Test'


def init_routes():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route, url_prefix="/api/v1/")

def init_routes_bank():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route_bank, url_prefix="/api/v1/")

def init_routes_customer():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route_customer, url_prefix="/api/v1/")

def init_routes_login():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route_login, url_prefix="/api/v1/")

def init_routes_team():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route_team, url_prefix="/api/v1/")

def init_routes_user():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route_user, url_prefix="/api/v1/")

def init_routes_car():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route_car, url_prefix="/api/v1/")

def init_routes_case():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route_case, url_prefix="/api/v1/")

def init_routes_excel():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route_excel, url_prefix="/api/v1/")

def init_routes_dashboard():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route_dashboard, url_prefix="/api/v1/")

def init_routes_dropdown():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route_dropdown, url_prefix="/api/v1/")

def init_routes_other():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route_other, url_prefix="/api/v1/")

def init_routes_cardd():
    app.add_url_rule('/','root_endpoint',root_endpoint,methods=['GET'],)
    app.register_blueprint(vm_route_cardd, url_prefix="/api/v1/")

