# -*- coding: utf-8 -*-
import uuid

import mysql.connector
import binascii
import hashlib
import os
from datetime import datetime
import random
import string

def timeStampToDatetime(timestamp):
    # timestamp is str
    date_time = datetime.fromtimestamp(float(timestamp))
    return date_time

def connectsql():
    mydb = mysql.connector.connect(
        host='127.0.0.1',
        database='cartrust',
        user='root',
        # port='3307',
        port='3306',
        password='cartrust2020><',
    )
    return mydb

def decmicalToBinary(num):
    return "{0:b}".format(num)

def binaryToDecimal(n):
    return int(n,2)

def checkData(column, value, table):
    if value == "" :
        return True
    mydb = connectsql()
    mycursor = mydb.cursor(dictionary=True)
    sql = "SELECT * FROM {} WHERE {} = '{}';".format(table, column, value)
    mycursor.execute(sql, )
    data = mycursor.fetchone()
    mydb.close()
    if data == None:
        return True
    return False

def checkDataEdit(column, table, check, id_col, id_value):
    if check == "" :
        return True
    mydb = connectsql()
    mycursor = mydb.cursor(dictionary=True)
    sql = "SELECT {} FROM {} WHERE {} = '{}';".format(id_col,table,column,check)
    mycursor.execute(sql, )
    data = mycursor.fetchone()
    mydb.close()
    if data == None:
        return True
    if str(data[id_col]) == str(id_value):
        return True
    return False

def hash_password(password):
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                  salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')

def verify_password(stored_password, provided_password):
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512',
                                  provided_password.encode('utf-8'),
                                  salt.encode('ascii'),
                                  100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password

def createId(id,column, table):
    now = datetime.now()
    mydb = connectsql()
    mycursor = mydb.cursor(dictionary=True)
    sql = "SELECT {} FROM {} ORDER BY id DESC".format(column, table)
    mycursor.execute(sql, )
    data = mycursor.fetchone()
    mydb.close()
    if data == None:
        return id + now.strftime("%y") + "-" + "000001"
    else:
        return id + now.strftime("%y") + "-" + str(int(data[column].split("-")[1])+1).rjust(6).replace(" ","0")

def createRole(data) :
    for key, val in data.items():
        if key.startswith("role_") == True:
            val = str(decmicalToBinary(val)).rjust(4).replace(" ","0")
            val_list = []
            for i in val :
                if i=="1":
                    val_list.append(True)
                else:
                    val_list.append(False)
            data[key] = {
                "read": val_list[0],
                "write": val_list[1],
                "update": val_list[2],
                "delete": val_list[3]
            }
    return data

def booleanToDemical(data):
    for key, val in data.items():
        if key.startswith("role_") == True:
            val_str = ""
            for item in val:
                if item == True:
                    val_str += "1"
                else :
                    val_str += "0"
            data[key] = binaryToDecimal(val_str)
    return data

def checkKey(data,key_list) :
    key_lost = []
    for key in key_list:
        if key not in data.keys():
            key_lost.append(key)
    if key_lost != []:
        return {"key_lost" : key_lost}
    return {"key_lost" : key_lost}

def checkVal(data):
    val_lost = []
    for key,val in data.items():
        if val == "":
            val_lost.append(key)
    if val_lost != []:
        return {"val_lost" : val_lost}
    return {"val_lost" : val_lost}

def caseStatusToYesNo(case_status) :
    case_dict = {
        "ติดต่อลูกค้าไม่ได้": 1,
        "ติดต่อลูกค้าได้": 2,
        "รอเอกสาร": 2,
        "ปิดสินเชื่อเดิม": 2,
        "ยื่นชุดโอน": 3,
        "รอรับชุดโอน": 5,
        "รอรับเล่ม": 5,
        "ส่งงานโอนทะเบียน": 7,
        "ตรวจสภาพรถ": 7,
        "โอนเล่มทะเบียน": 8,
        "รับสำเนาเล่มจาก KM": 10,
        "ส่งสำเนาเล่มเบิกมัดจำคืน": 12,
        "ส่งสำเนาเล่มให้ Finance": 11,
        "รับเล่มคืนจาก KM": 12,
        "ส่งเล่มจริงให้ Finance": 13,
        "ไฟแนนซ์โอนเงินให้ลูกค้า": 13,
        "รอเงินเข้าบัญชี CT": 13,
        "รอเงินมัดจำคืน": 13,
        "Case Close / ปิด Job": 16,
        "ลูกค้ายกเลิก": 0
    }

    case_list = ["receive", "contact_customer", "account_closing", "transfer_doc_received", "transfer_doc_submitted",
                 "book_received", "submit_book_transfer", "car_check_up", "book_transfer", "book_copy_received",
                 "deposit_doc_to_new_bank", "submit_book_deposit_return", "book_received_back", "cash_received",
                 "book_deposit_received", "submit_book_to_new_finance"]

    case_number = case_dict[case_status]
    string_column = ""
    string_value = ""
    for i in range(1,case_number):
        string_column += "," + case_list[i] + "_yn"
        string_value += ",'yes'"

    return string_column,string_value

def createJobId(case_source,mydb,mycursor) :
    now = datetime.now()
    now_year = now.strftime("%y")
    # now_year = '19'
    sql = "SELECT MIN(no) as no,job_id FROM cancel_case WHERE year = '{}'".format(now_year)
    mycursor.execute(sql, )
    data = mycursor.fetchone()
    job_id_cancel = data['job_id']
    print(data)
    if data['no'] != None :
        if len(str(data['no'])) < 3 :
            no = str(data['no']).rjust(3).replace(" ", "0")
        else:
            no = str(data['no'])
        sql = "DELETE FROM cancel_case WHERE job_id = '{}';".format(job_id_cancel)
        mycursor.execute(sql, )
        mydb.commit()
    else:
        sql2 = "SELECT MAX(no) as no FROM customercase WHERE year = '{}'".format(now_year)
        mycursor.execute(sql2, )
        data2 = mycursor.fetchone()
        if data2['no'] != None :
            if len(str(data2['no'])) < 3 :
                no = str(data2['no'] + 1).rjust(3).replace(" ", "0")
            else:
                no = str(data2['no'] + 1)
        else:
            no = "001"

    case_source = case_source.replace(" ","").lower()

    if case_source == 'kiatnakin':
        cs = 'KK'
    elif case_source == 'thanachart':
        cs = 'TB'
    elif case_source == 'cartrust':
        cs = 'CT'
    else:
        cs = 'XX'

    job_id = cs + no + "/" + now_year

    return job_id

def createDocId():
    pool = string.ascii_letters + string.digits
    return ''.join(random.choice(pool) for i in range(10))

def createPassword():
    pool = string.ascii_letters + string.digits
    return ''.join(random.choice(pool) for i in range(10))

