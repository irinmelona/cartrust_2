# -*- coding: utf-8 -*-
import sys
import datetime
from flask_api import status
from flask import request, jsonify,Flask
from werkzeug.utils import secure_filename
import excel2dict
from controller.all_function import *

UPLOAD_FOLDER = r'./controller/excel_file'

def createId2(year,id,column, table):
    mydb = connectsql()
    mycursor = mydb.cursor(dictionary=True)
    sql = "SELECT {} FROM {} ORDER BY id DESC".format(column, table)
    mycursor.execute(sql, )
    data = mycursor.fetchone()
    mydb.close()
    if data == None:
        return id + year + "-" + "000001"
    else:
        return id + year + "-" + str(int(data[column].split("-")[1])+1).rjust(6).replace(" ","0")

def excelToDate2562(excel_date):
    if type(excel_date) == float:
        excel_date = int(excel_date)
        date = datetime.fromordinal(datetime(1900, 1, 1).toordinal() + excel_date - 2)
        if int(date.strftime("%Y")) > 2500:
            date_str = date.strftime("%d/%m/") + str(int(date.strftime("%Y")) - 543)
            date = datetime.strptime(date_str, "%d/%m/%Y")
        return date
    elif excel_date == "":
        return None

def import_excel_sql_KK():
    try:
        file = request.files["excel_file"]
        filename = secure_filename(file.filename)
        file.save(os.path.join(UPLOAD_FOLDER, filename))
        data = excel2dict.to_json(os.path.join(UPLOAD_FOLDER, filename))
        os.remove(os.path.join(UPLOAD_FOLDER, filename))
        sheet = request.args['sheet']
        year = str(int(sheet) - 2543)
        print(year)
        data = data[sheet]

        # return jsonify({ "data" : data})

        list_key = []
        list_ans = []

        for key in data[2].keys():
            if str(key).startswith("c") and len(key) < 4:
                list_key.append(int(key.replace("c","")))

        #     for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]:
        #         if i not in list_key:
        #             list_ans.append(i)
            # return jsonify({"data" : list_ans})



        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)

        count = 0
        for case in data:
            if case['No'] != "":
                print("------------------------------------------------------")
                print("No :" + str(case['No']) +":" ,type(case['No']))
                print(case['ชื่อ-นามสกุล'].split())
                if len(case['ชื่อ-นามสกุล'].split()) == 3:
                    firstname = case['ชื่อ-นามสกุล'].split()[1]
                    lastname = case['ชื่อ-นามสกุล'].split()[2]
                elif len(case['ชื่อ-นามสกุล'].split()) == 2:
                    firstname = case['ชื่อ-นามสกุล'].split()[0]
                    lastname = case['ชื่อ-นามสกุล'].split()[1]
                else:
                    firstname = case['ชื่อ-นามสกุล']
                    lastname = ""


                create = datetime.now()

                customer_id = createId2(year,"CTM", "customer_id", "customer")
                check = True
                while check == True:
                    customer_id = createId2(year,"CTM", "customer_id", "customer")
                    if checkData('customer_id', customer_id, 'customer') == True:
                        check = False

                sql_customer = """INSERT INTO customer (                    customer_id,
                                                                                    firstname,
                                                                                    lastname,
                                                                                    date_create,
                                                                                    date_update)
                                                    VALUES ('{}','{}','{}','{}','{}');""".format(
                    customer_id,
                    firstname,
                    lastname,
                    create,
                    create
                )

                car_license = case['ทะเบียนรถยนต์']
                province = case['ทะเบียนรถยนต์\nจังหวัด']

                car_id = createId2(year,"CAR", "car_id", "car")
                check = True
                while check == True:
                    car_id = createId2(year,"CAR", "car_id", "car")
                    if checkData('car_id', car_id, 'car') == True:
                        check = False
                sql_car = """INSERT INTO car (                              car_id,
                                                                                    car_license,
                                                                                    car_province,
                                                                                    date_create,
                                                                                    date_update)
                                                VALUES('{}','{}','{}','{}','{}')""".format(
                    car_id,
                    car_license,
                    province,
                    create,
                    create
                )

                case_dict = {
                    "1": "receive",
                    "2": "contact_customer",
                    "3": "account_closing",
                    "4": "transfer_doc_submitted",
                    "5": "transfer_doc_received",
                    "6": "book_received",
                    "7": "submit_book_transfer",
                    "8": "car_check_up",
                    "9": "book_transfer",
                    "10": "book_copy_received",
                    "11": "deposit_doc_to_new_bank",
                    "12": "submit_book_deposit_return",
                    "13": "book_received_back",
                    "14": "cash_received",
                    "15": "book_deposit_received",
                    "16": "submit_book_to_new_finance"
                }

                date_dict = {}
                date_list = []

                str_date = ""
                for i in range(1, 17):
                    str_date += case_dict[str(i)] + "_date" + ","
                str_date = str_date[0:len(str_date) - 1]

                string_column_yn = ""
                string_value_yn = ""

                for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]:
                    if i not in list_key:
                        if i == 5:
                            date = excelToDate2562(case['c4'])
                            if date != None:
                                date_dict[str(i)] = date
                                date_list.append(case_dict[str(i)])
                                if case_dict[str(i)] != "receive":
                                    string_column_yn += "," + case_dict[str(i)] + "_yn"
                                    string_value_yn += ",'yes'"
                            else:
                                date_dict[str(i)] = "0000-00-00"
                        else:
                            date_dict[str(i)] = "0000-00-00"
                    else:
                        date = excelToDate2562(case['c{}'.format(i)])
                        if date != None:
                            date_dict[str(i)] = date
                            date_list.append(case_dict[str(i)])
                            if case_dict[str(i)] != "receive":
                                string_column_yn += "," + case_dict[str(i)] +"_yn"
                                string_value_yn += ",'yes'"
                        else:
                            date_dict[str(i)] = "0000-00-00"

                print("date_list" , date_list , case['Job\nNumber'].replace(" ",""))
                data_status = date_list[len(date_list) - 1]
                approve_amount = case['ยอดอนุมัติ']
                if approve_amount == "":
                    approve_amount = 0
                case_status = case['สถานะ']
                finance_staff = case['จนท.ติดต่อลูกค้า']
                document_id = case['AOL']
                if type(document_id) == float:
                    document_id = int(document_id)
                job_id = case['Job\nNumber'].replace(" ","")
                team = filename.split(".")[0]
                note_status = case['หมายเหตุ'].replace("'", " ")
                old_bank = case['สถาบันการเงินเดิม']
                contract_officer = case['contract_officer']

                case_id = createId2(year,"C", "case_id", "customercase")
                check = True
                while check == True:
                    case_id = createId2(year,"C", "case_id", "customercase")
                    if checkData('case_id', case_id, 'customercase') == True:
                        check = False

                if case['สถานะ'] == "ลูกค้ายกเลิก" or case['สถานะ'] == "ไม่สำเร็จ":
                    process = "cancel"
                elif case['สถานะ'] == 'สำเร็จ':
                    process = "complete"
                else:
                    process = "process"

                print("job_id :", job_id)
                if len(job_id.split("/")) == 2:
                    print(job_id.split("/"))
                    no = int(job_id.split("/")[0][2:])
                    year2 = job_id.split("/")[1]
                    cs = job_id[0:2]
                    if cs == 'KK' or cs == 'kk':
                        case_source = "Kiatnakin"
                    else:
                        print("job_id 1 :", job_id)
                        case_source = ""
                else:
                    no = 0
                    year2 = ""
                    case_source = ""

                sql_case = """INSERT INTO customercase (        case_id,
                                                                                    customer_id,
                                                                                    car_id,
                                                                                    approve_amount,
                                                                                    case_status,
                                                                                    status,
                                                                                    contract_officer,
                                                                                    document_id,
                                                                                    finance_staff,
                                                                                    job_id,no,year,case_source,
                                                                                    team,
                                                                                    note_status,
                                                                                    old_bank,
                                                                                    date_create,date_update,
                                                                                    process
                                                                                    )
                                                VALUES('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')""".format(
                    case_id,
                    customer_id,
                    car_id,
                    approve_amount,
                    case_status,
                    data_status,
                    contract_officer,
                    document_id,
                    finance_staff,
                    job_id, no, year2, case_source,
                    team,
                    note_status,
                    old_bank,
                    create, create,
                    process)


                sql_tracking = """INSERT INTO tracking  (       case_id,
                                                                                    {}{}
                                                                                    )
                                                                                    VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}'{});""".format(
                    str_date,
                    string_column_yn,
                    case_id,
                    date_dict["1"],
                    date_dict["2"],
                    date_dict["3"],
                    date_dict["4"],
                    date_dict["5"],
                    date_dict["6"],
                    date_dict["7"],
                    date_dict["8"],
                    date_dict["9"],
                    date_dict["10"],
                    date_dict["11"],
                    date_dict["12"],
                    date_dict["13"],
                    date_dict["14"],
                    date_dict["15"],
                    date_dict["16"],
                    string_value_yn
                )

                mycursor.execute(sql_customer, )
                mydb.commit()
                mycursor.execute(sql_car, )
                mydb.commit()
                mycursor.execute(sql_case, )
                mydb.commit()
                mycursor.execute(sql_tracking, )
                mydb.commit()
                count += 1
        sql_status = "UPDATE customercase SET status = 'transfer_doc_submitted' WHERE status = 'transfer_doc_received'"
        mycursor.execute(sql_status, )
        mydb.commit()
        mydb.close()

        return "success \ndata = {}\nyear = {}\nteam = {}".format(count,year,team)

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(e)), status.HTTP_500_INTERNAL_SERVER_ERROR

def import_excel_sql_TB():
    try:
        file = request.files["excel_file"]
        filename = secure_filename(file.filename)
        file.save(os.path.join(UPLOAD_FOLDER, filename))
        data = excel2dict.to_json(os.path.join(UPLOAD_FOLDER, filename))
        os.remove(os.path.join(UPLOAD_FOLDER, filename))
        data = data['Daily Report']

        # return jsonify({ "data" : data})

        list_key = []
        list_ans = []

        for key in data[2].keys():
            if str(key).startswith("c") and len(key) < 4:
                list_key.append(int(key.replace("c", "")))

        #     for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]:
        #         if i not in list_key:
        #             list_ans.append(i)
        # return jsonify({"data" : list_ans})

        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)

        count = 0
        for case in data:
            if case['No'] != "":
                print("------------------------------------------------------")
                print("No :" + str(case['No']) + ":", type(case['No']))
                if len(case['ชื่อ-นามสกุล'].split()) == 2:
                    firstname = case['ชื่อ-นามสกุล'].split()[0]
                    lastname = case['ชื่อ-นามสกุล'].split()[1]
                elif len(case['ชื่อ-นามสกุล'].split()) == 4:
                    firstname = case['ชื่อ-นามสกุล'].split()[0] + case['ชื่อ-นามสกุล'].split()[1] + \
                                case['ชื่อ-นามสกุล'].split()[2]
                    lastname = case['ชื่อ-นามสกุล'].split()[3]
                else:
                    return jsonify({"name": case['ชื่อ-นามสกุล'].split()})
                    # firstname = case['ชื่อ-นามสกุล']
                    # lastname = ""

                create = datetime.now()

                case_dict = {
                    "1": "receive",
                    "2": "contact_customer",
                    "3": "account_closing",
                    "4": "transfer_doc_submitted",
                    "5": "transfer_doc_received",
                    "6": "book_received",
                    "7": "submit_book_transfer",
                    "8": "car_check_up",
                    "9": "book_transfer",
                    "10": "book_copy_received",
                    "11": "deposit_doc_to_new_bank",
                    "12": "submit_book_deposit_return",
                    "13": "book_received_back",
                    "14": "cash_received",
                    "15": "book_deposit_received",
                    "16": "submit_book_to_new_finance"
                }

                date_dict = {}
                date_list = []

                str_date = ""
                for i in range(1, 17):
                    str_date += case_dict[str(i)] + "_date" + ","
                str_date = str_date[0:len(str_date) - 1]

                string_column_yn = ""
                string_value_yn = ""

                for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]:
                    if i not in list_key:
                        if i == 6:
                            if excelToDate2562(case['c6_1']) != None:
                                date_dict[str(i)] = excelToDate2562(case['c6_1'])
                            else:
                                if excelToDate2562(case['c6_2']) != None:
                                    date_dict[str(i)] = excelToDate2562(case['c6_2'])
                                else:
                                    date_dict[str(i)] = "0000-00-00"

                        else:
                            date_dict[str(i)] = "0000-00-00"
                    else:
                        date = excelToDate2562(case['c{}'.format(i)])
                        if date != None:
                            date_dict[str(i)] = date
                            date_list.append(case_dict[str(i)])
                            if case_dict[str(i)] != "receive":
                                string_column_yn += "," + case_dict[str(i)] + "_yn"
                                string_value_yn += ",'yes'"
                        else:
                            date_dict[str(i)] = "0000-00-00"

                # print("date_list", date_list, case['job_id'].replace(" ", ""))
                data_status = date_list[len(date_list) - 1]

                document_id = case['AOL']
                if type(document_id) == float:
                    document_id = int(document_id)

                car_license = case['license']
                province = case['province']

                approve_amount = case['ยอดอนุมัติ']
                close_amount = case['ยอดปิด']
                if approve_amount == "":
                    approve_amount = 0
                if close_amount == "":
                    close_amount = 0

                tel = case['tel']
                contract_officer = case['contract_officer']
                case_source = "Thanachart"
                hub = case['hub']
                old_bank = case['ชื่อไฟแนนซ์เดิม']
                case_status = case['case_status']
                difference = case['difference']
                if difference == "2 รับเงินก่อนโอนรถ":
                    difference = "true"
                elif difference == "1 รับเงินหลังโอนรถ":
                    difference = "false"
                elif difference == "":
                    difference = None
                else:
                    return jsonify({"case": case})
                difference_fee = case['difference_fee']
                if difference_fee == "" or difference_fee == "-":
                    difference_fee = 0
                difference_date = excelToDate2562(case['difference_date'])
                finance_staff = case['finance_staff']
                case_receiver = finance_staff
                note_status = case['หมายเหตุ']
                job_id = case['job_id']

                if job_id != "":
                    print(job_id.split("/"))
                    no = int(job_id.split("/")[0][2:])
                    year2 = job_id.split("/")[1]
                    cs = job_id[0:2]
                    if cs == 'TB' or cs == 'tb':
                        case_source = "Thanachart"
                    else:
                        case_source = ""
                else:
                    no = 0
                    year2 = ""
                    case_source = ""

                if case_status == "ยกเลิก":
                    process = "cancel"
                elif type(case_status) == float:
                    process = "complete"
                else:
                    process = "process"

                year = date_dict['1'].strftime("%y")
                customer_id = createId2(year, "CTM", "customer_id", "customer")
                check = True
                while check:
                    customer_id = createId2(year, "CTM", "customer_id", "customer")
                    if checkData('customer_id', customer_id, 'customer'):
                        check = False

                case_id = createId2(year, "C", "case_id", "customercase")
                check = True
                while check:
                    case_id = createId2(year, "C", "case_id", "customercase")
                    if checkData('case_id', case_id, 'customercase'):
                        check = False

                car_id = createId2(year, "CAR", "car_id", "car")
                check = True
                while check == True:
                    car_id = createId2(year, "CAR", "car_id", "car")
                    if checkData('car_id', car_id, 'car'):
                        check = False

                sql_car = """INSERT INTO car (                              car_id,
                                                                                    car_license,
                                                                                    car_province,
                                                                                    date_create,
                                                                                    date_update)
                                                VALUES('{}','{}','{}','{}','{}')""".format(
                    car_id,
                    car_license,
                    province,
                    create,
                    create
                )

                #
                sql_customer = """INSERT INTO customer (                    customer_id,
                                                                                    firstname,
                                                                                    lastname,
                                                                                    tel,
                                                                                    date_create,
                                                                                    date_update)
                                                    VALUES ('{}','{}','{}','{}','{}','{}');""".format(
                    customer_id,
                    firstname,
                    lastname,
                    tel,
                    create,
                    create
                )


                sql_case = """INSERT INTO customercase (        case_id,
                                                                                    case_receiver,
                                                                                    hub,
                                                                                    difference,
                                                                                    difference_fee,
                                                                                    difference_date,

                                                                                    customer_id,
                                                                                    car_id,
                                                                                    approve_amount,
                                                                                    case_status,
                                                                                    status,
                                                                                    contract_officer,
                                                                                    document_id,
                                                                                    finance_staff,
                                                                                    job_id,no,year,case_source,
                                                                                    note_status,
                                                                                    old_bank,
                                                                                    date_create,date_update,
                                                                                    process
                                                                                    )
                                                VALUES('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')""".format(
                    case_id,
                    case_receiver,
                    hub,
                    difference,
                    difference_fee,
                    difference_date,
                    customer_id,
                    car_id,
                    approve_amount,
                    case_status,
                    data_status,
                    contract_officer,
                    document_id,
                    finance_staff,
                    job_id, no, year2, case_source,
                    note_status,
                    old_bank,
                    create, create,
                    process)

                sql_tracking = """INSERT INTO tracking  (       case_id,
                                                                                    {}{}
                                                                                    )
                                                                                    VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}'{});""".format(
                    str_date,
                    string_column_yn,
                    case_id,
                    date_dict["1"],
                    date_dict["2"],
                    date_dict["3"],
                    date_dict["4"],
                    date_dict["5"],
                    date_dict["6"],
                    date_dict["7"],
                    date_dict["8"],
                    date_dict["9"],
                    date_dict["10"],
                    date_dict["11"],
                    date_dict["12"],
                    date_dict["13"],
                    date_dict["14"],
                    date_dict["15"],
                    date_dict["16"],
                    string_value_yn
                )


                mycursor.execute(sql_customer, )
                mydb.commit()
                mycursor.execute(sql_car, )
                mydb.commit()
                mycursor.execute(sql_case, )
                mydb.commit()
                mycursor.execute(sql_tracking, )
                mydb.commit()
                count += 1
        sql_status = "UPDATE customercase SET status = 'transfer_doc_submitted' WHERE status = 'transfer_doc_received'"
        mycursor.execute(sql_status, )
        mydb.commit()
        mydb.close()

        return "success \ndata = {}".format(count)

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR

def import_excel_sql_CT():
    try:
        file = request.files["excel_file"]
        filename = secure_filename(file.filename)
        file.save(os.path.join(UPLOAD_FOLDER, filename))
        data = excel2dict.to_json(os.path.join(UPLOAD_FOLDER, filename))
        os.remove(os.path.join(UPLOAD_FOLDER, filename))
        sheet = request.args['sheet']
        year = request.args['year']
        data = data[sheet]

        # return jsonify({ "data" : data})

        list_key = []
        list_ans = []

        for key in data[2].keys():
            if str(key).startswith("c") and len(key) < 4:
                list_key.append(int(key.replace("c","")))

        #     for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]:
        #         if i not in list_key:
        #             list_ans.append(i)
            # return jsonify({"data" : list_ans})



        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)

        count = 0
        for case in data:
            if case['No'] != "":
                print("------------------------------------------------------")
                print("No :" + str(case['No']) +":" ,type(case['No']))
                print(case['customer_name'].split())
                if len(case['customer_name'].split()) == 3:
                    firstname = case['customer_name'].split()[1]
                    lastname = case['customer_name'].split()[2]
                elif len(case['customer_name'].split()) == 2:
                    firstname = case['customer_name'].split()[0]
                    lastname = case['customer_name'].split()[1]
                else:
                    firstname = case['customer_name']
                    lastname = ""


                create = datetime.now()

                customer_id = createId2(year,"CTM", "customer_id", "customer")
                check = True
                while check == True:
                    customer_id = createId2(year,"CTM", "customer_id", "customer")
                    if checkData('customer_id', customer_id, 'customer') == True:
                        check = False

                sql_customer = """INSERT INTO customer (                    customer_id,
                                                                                    firstname,
                                                                                    lastname,
                                                                                    date_create,
                                                                                    date_update)
                                                    VALUES ('{}','{}','{}','{}','{}');""".format(
                    customer_id,
                    firstname,
                    lastname,
                    create,
                    create
                )

                car_license = case['license']
                province = case['province']
                brand = case['brand']
                model = case['model']

                car_id = createId2(year,"CAR", "car_id", "car")
                check = True
                while check == True:
                    car_id = createId2(year,"CAR", "car_id", "car")
                    if checkData('car_id', car_id, 'car') == True:
                        check = False
                sql_car = """INSERT INTO car (                                      car_id,
                                                                                    car_license,
                                                                                    car_province,
                                                                                    car_brand,
                                                                                    car_model,
                                                                                    date_create,
                                                                                    date_update)
                                                VALUES('{}','{}','{}','{}','{}','{}','{}')""".format(
                    car_id,
                    car_license,
                    province,
                    brand,
                    model,
                    create,
                    create
                )

                case_dict = {
                    "1": "receive",
                    "2": "contact_customer",
                    "3": "account_closing",
                    "4": "transfer_doc_submitted",
                    "5": "transfer_doc_received",
                    "6": "book_received",
                    "7": "submit_book_transfer",
                    "8": "car_check_up",
                    "9": "book_transfer",
                    "10": "book_copy_received",
                    "11": "deposit_doc_to_new_bank",
                    "12": "submit_book_deposit_return",
                    "13": "book_received_back",
                    "14": "cash_received",
                    "15": "book_deposit_received",
                    "16": "submit_book_to_new_finance"
                }

                date_dict = {}
                date_list = []

                str_date = ""
                for i in range(1, 17):
                    str_date += case_dict[str(i)] + "_date" + ","
                str_date = str_date[0:len(str_date) - 1]

                string_column_yn = ""
                string_value_yn = ""

                for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]:
                    if i not in list_key:
                        date_dict[str(i)] = "0000-00-00"
                    else:
                        date = excelToDate2562(case['c{}'.format(i)])
                        if date != None:
                            date_dict[str(i)] = date
                            date_list.append(case_dict[str(i)])
                            if case_dict[str(i)] != "receive":
                                string_column_yn += "," + case_dict[str(i)] +"_yn"
                                string_value_yn += ",'yes'"
                        else:
                            date_dict[str(i)] = "0000-00-00"

                print("date_list" , date_list , case['job_id'].replace(" ",""))

                print(date_list)

                data_status = date_list[len(date_list) - 1]
                finance_staff = case['case_receiver']
                case_receiver = case['case_receiver']
                job_id = case['job_id'].replace(" ","")
                note_status = case['note_status'].replace("'", " ")
                old_bank = case['old_bank']
                new_bank = case['new_bank']
                job_id_old = case['job_id_old']

                if case['c14'] == "" or case['c14'] == None:
                    process = 'process'
                else:
                    process = "complete"
                if case['status'] == "ยกเลิก":
                    process = "cancel"

                if case['case_type'] == "รีไฟแนนซ์" :
                    case_type = 'Refinance'
                elif case['case_type'] == "ซื้อ-ขาย" :
                    case_type = 'Buy - Sell / ซื้อขาย'
                elif case['case_type'] == "จำนำ" :
                    case_type = 'PAWN  / จำนำเล่ม'
                else:
                    case_type = ""

                print("job_id :", job_id)
                if len(job_id.split("/")) == 2:
                    print(job_id.split("/"))
                    no = int(job_id.split("/")[0][2:])
                    year2 = job_id.split("/")[1]
                    cs = job_id[0:2]
                    if cs == 'CT' or cs == 'CT':
                        case_source = "Cartrust"
                    else:
                        print("job_id 1 :", job_id)
                        case_source = ""
                else:
                    no = 0
                    year2 = ""
                    case_source = ""

                case_id = createId2(year, "C", "case_id", "customercase")
                check = True
                while check == True:
                    case_id = createId2(year, "C", "case_id", "customercase")
                    if checkData('case_id', case_id, 'customercase') == True:
                        check = False

                sql_case = """INSERT INTO customercase (                            case_id,
                                                                                    customer_id,
                                                                                    car_id,
                                                                                    status,
                                                                                    finance_staff,
                                                                                    case_receiver,
                                                                                    job_id,no,year,case_source,
                                                                                    note_status,
                                                                                    old_bank,
                                                                                    new_bank,
                                                                                    job_id_old,
                                                                                    date_create,date_update,
                                                                                    process,
                                                                                    case_type
                                                                                    )
                                                VALUES('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')""".format(
                    case_id,
                    customer_id,
                    car_id,
                    data_status,
                    finance_staff,
                    case_receiver,
                    job_id, no, year2, case_source,
                    note_status,
                    old_bank,
                    new_bank,
                    job_id_old,
                    create, create,
                    process,
                    case_type)


                sql_tracking = """INSERT INTO tracking  (                           case_id,
                                                                                    {}{}
                                                                                    )
                                                                                    VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}'{});""".format(
                    str_date,
                    string_column_yn,
                    case_id,
                    date_dict["1"],
                    date_dict["2"],
                    date_dict["3"],
                    date_dict["4"],
                    date_dict["5"],
                    date_dict["6"],
                    date_dict["7"],
                    date_dict["8"],
                    date_dict["9"],
                    date_dict["10"],
                    date_dict["11"],
                    date_dict["12"],
                    date_dict["13"],
                    date_dict["14"],
                    date_dict["15"],
                    date_dict["16"],
                    string_value_yn
                )

                mycursor.execute(sql_customer, )
                mydb.commit()
                mycursor.execute(sql_car, )
                mydb.commit()
                mycursor.execute(sql_case, )
                mydb.commit()
                mycursor.execute(sql_tracking, )
                mydb.commit()
                count += 1
        sql_status = "UPDATE customercase SET status = 'transfer_doc_submitted' WHERE status = 'transfer_doc_received'"
        mycursor.execute(sql_status, )
        mydb.commit()
        mydb.close()

        return "success \ndata = {}\nyear = {}\n".format(count,year)

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(e)), status.HTTP_500_INTERNAL_SERVER_ERROR

def import_excel_sql_DL():
    try:
        file = request.files["excel_file"]
        filename = secure_filename(file.filename)
        file.save(os.path.join(UPLOAD_FOLDER, filename))
        data = excel2dict.to_json(os.path.join(UPLOAD_FOLDER, filename))
        os.remove(os.path.join(UPLOAD_FOLDER, filename))
        sheet = request.args['sheet']
        year = request.args['year']
        data = data[sheet]

        # return jsonify({ "data" : data})

        list_key = []
        list_ans = []

        for key in data[2].keys():
            if str(key).startswith("c") and len(key) < 4:
                list_key.append(int(key.replace("c","")))

        #     for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]:
        #         if i not in list_key:
        #             list_ans.append(i)
            # return jsonify({"data" : list_ans})



        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)

        count = 0
        for case in data:
            if case['No'] != "":
                print("------------------------------------------------------")
                print("No :" + str(case['No']) +":" ,type(case['No']))
                print(case['customer_name'].split())
                if len(case['customer_name'].split()) == 3:
                    firstname = case['customer_name'].split()[1]
                    lastname = case['customer_name'].split()[2]
                elif len(case['customer_name'].split()) == 2:
                    firstname = case['customer_name'].split()[0]
                    lastname = case['customer_name'].split()[1]
                else:
                    firstname = case['customer_name']
                    lastname = ""


                create = datetime.now()

                customer_id = createId2(year,"CTM", "customer_id", "customer")
                check = True
                while check == True:
                    customer_id = createId2(year,"CTM", "customer_id", "customer")
                    if checkData('customer_id', customer_id, 'customer') == True:
                        check = False

                sql_customer = """INSERT INTO customer (                    customer_id,
                                                                                    firstname,
                                                                                    lastname,
                                                                                    date_create,
                                                                                    date_update)
                                                    VALUES ('{}','{}','{}','{}','{}');""".format(
                    customer_id,
                    firstname,
                    lastname,
                    create,
                    create
                )

                car_license = case['license']
                province = case['province']
                brand = case['brand']
                model = case['model']

                car_id = createId2(year,"CAR", "car_id", "car")
                check = True
                while check == True:
                    car_id = createId2(year,"CAR", "car_id", "car")
                    if checkData('car_id', car_id, 'car') == True:
                        check = False
                sql_car = """INSERT INTO car (                                      car_id,
                                                                                    car_license,
                                                                                    car_province,
                                                                                    car_brand,
                                                                                    car_model,
                                                                                    date_create,
                                                                                    date_update)
                                                VALUES('{}','{}','{}','{}','{}','{}','{}')""".format(
                    car_id,
                    car_license,
                    province,
                    brand,
                    model,
                    create,
                    create
                )

                case_dict = {
                    "1": "receive",
                    "2": "contact_customer",
                    "3": "account_closing",
                    "4": "transfer_doc_submitted",
                    "5": "transfer_doc_received",
                    "6": "book_received",
                    "7": "submit_book_transfer",
                    "8": "car_check_up",
                    "9": "book_transfer",
                    "10": "book_copy_received",
                    "11": "deposit_doc_to_new_bank",
                    "12": "submit_book_deposit_return",
                    "13": "book_received_back",
                    "14": "cash_received",
                    "15": "book_deposit_received",
                    "16": "submit_book_to_new_finance"
                }

                date_dict = {}
                date_list = []

                str_date = ""
                for i in range(1, 17):
                    str_date += case_dict[str(i)] + "_date" + ","
                str_date = str_date[0:len(str_date) - 1]

                string_column_yn = ""
                string_value_yn = ""

                for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]:
                    if i not in list_key:
                        date_dict[str(i)] = "0000-00-00"
                    else:
                        date = excelToDate2562(case['c{}'.format(i)])
                        if date != None:
                            date_dict[str(i)] = date
                            date_list.append(case_dict[str(i)])
                            if case_dict[str(i)] != "receive":
                                string_column_yn += "," + case_dict[str(i)] +"_yn"
                                string_value_yn += ",'yes'"
                        else:
                            date_dict[str(i)] = "0000-00-00"

                print("date_list" , date_list , case['job_id'].replace(" ",""))

                print(date_list)

                data_status = date_list[len(date_list) - 1]
                finance_staff = case['case_receiver']
                case_receiver = case['case_receiver']
                job_id = case['job_id'].replace(" ","")
                if job_id != "":
                    job_id = "DL" + job_id
                note_status = case['note_status'].replace("'", " ")
                old_bank = case['old_bank']
                new_bank = case['new_bank']
                job_id_old = case['job_id_old']

                if int(sheet) == 18:
                    if case['status'] == "ยกเลิก":
                        process = "cancel"
                    elif case['status'] == "สำเร็จ":
                        process = "complete"
                    else:
                        process = 'process'
                else:
                    if case['c14'] == "" or case['c14'] == None:
                        process = 'process'
                    else:
                        process = "complete"
                    if case['status'] == "ยกเลิก":
                        process = "cancel"

                if case['case_type'] == "รีไฟแนนซ์" :
                    case_type = 'Refinance'
                elif case['case_type'] == "ซื้อ-ขาย" :
                    case_type = 'Buy - Sell / ซื้อขาย'
                elif case['case_type'] == "จำนำ" :
                    case_type = 'PAWN  / จำนำเล่ม'
                else:
                    case_type = ""

                print("job_id :", job_id)
                if len(job_id.split("/")) == 2:
                    print(job_id.split("/"))
                    no = int(job_id.split("/")[0][2:])
                    year2 = job_id.split("/")[1]
                    cs = job_id[0:2]
                    if cs == 'DL':
                        case_source = "Dealer"
                    else:
                        print("job_id 1 :", job_id)
                        case_source = ""
                else:
                    no = 0
                    year2 = ""
                    case_source = ""

                case_id = createId2(year, "C", "case_id", "customercase")
                check = True
                while check == True:
                    case_id = createId2(year, "C", "case_id", "customercase")
                    if checkData('case_id', case_id, 'customercase') == True:
                        check = False

                sql_case = """INSERT INTO customercase (                            case_id,
                                                                                    customer_id,
                                                                                    car_id,
                                                                                    status,
                                                                                    finance_staff,
                                                                                    case_receiver,
                                                                                    job_id,no,year,case_source,
                                                                                    note_status,
                                                                                    old_bank,
                                                                                    new_bank,
                                                                                    job_id_old,
                                                                                    date_create,date_update,
                                                                                    process,
                                                                                    case_type
                                                                                    )
                                                VALUES('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')""".format(
                    case_id,
                    customer_id,
                    car_id,
                    data_status,
                    finance_staff,
                    case_receiver,
                    job_id, no, year2, case_source,
                    note_status,
                    old_bank,
                    new_bank,
                    job_id_old,
                    create, create,
                    process,
                    case_type)


                sql_tracking = """INSERT INTO tracking  (                           case_id,
                                                                                    {}{}
                                                                                    )
                                                                                    VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}'{});""".format(
                    str_date,
                    string_column_yn,
                    case_id,
                    date_dict["1"],
                    date_dict["2"],
                    date_dict["3"],
                    date_dict["4"],
                    date_dict["5"],
                    date_dict["6"],
                    date_dict["7"],
                    date_dict["8"],
                    date_dict["9"],
                    date_dict["10"],
                    date_dict["11"],
                    date_dict["12"],
                    date_dict["13"],
                    date_dict["14"],
                    date_dict["15"],
                    date_dict["16"],
                    string_value_yn
                )

                mycursor.execute(sql_customer, )
                mydb.commit()
                mycursor.execute(sql_car, )
                mydb.commit()
                mycursor.execute(sql_case, )
                mydb.commit()
                mycursor.execute(sql_tracking, )
                mydb.commit()
                count += 1
        sql_status = "UPDATE customercase SET status = 'transfer_doc_submitted' WHERE status = 'transfer_doc_received'"
        mycursor.execute(sql_status, )
        mydb.commit()
        mydb.close()

        return "success \ndata = {}\nyear = {}\n".format(count,year)

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(e)), status.HTTP_500_INTERNAL_SERVER_ERROR
