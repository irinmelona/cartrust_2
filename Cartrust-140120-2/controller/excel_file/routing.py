from flask import Blueprint
from controller.excel_file import sql_to_excelfile, sql_to_excelfile_mail,get_excel_file

vm_routes_excel = Blueprint('/controller_excel', __name__)

# ----------------------------------------------------------------------------------------------------------------WEEK_1

vm_routes_excel.add_url_rule('case_excel_file', 'case_excel/get', sql_to_excelfile.sqlToExcel,
                             methods=['GET'],)

vm_routes_excel.add_url_rule('case_excel_mail', 'case_excel/mail', sql_to_excelfile_mail.sqlToExcelMail,
                             methods=['POST'],)

vm_routes_excel.add_url_rule('excel_KK', 'excel/KK', get_excel_file.import_excel_sql_KK,methods=['POST'],)
vm_routes_excel.add_url_rule('excel_TB', 'excel/TB', get_excel_file.import_excel_sql_TB,methods=['POST'],)
vm_routes_excel.add_url_rule('excel_CT', 'excel/CT', get_excel_file.import_excel_sql_CT,methods=['POST'],)
vm_routes_excel.add_url_rule('excel_DL', 'excel/DL', get_excel_file.import_excel_sql_DL,methods=['POST'],)