import datetime
from controller.all_function import connectsql
import calendar

def summary(sql_where):
    if sql_where == "":
        sql_where = "WHERE customercase.id IS NOT NULL"
    mydb = connectsql()
    mycursor = mydb.cursor(dictionary=True)
    sql = """SELECT tracking.receive_date AS date,tracking.case_id AS case_id FROM customercase
                LEFT JOIN tracking ON tracking.case_id = customercase.case_id
                {}""".format(sql_where)
    mycursor.execute(sql, )
    receive_date = mycursor.fetchall()
    # print(receive_date)
    b = []
    e = []
    for i in range(len(receive_date)):
        if receive_date[i]['date'] is not None:
            month_year = [receive_date[i]['date'].strftime("%d"), receive_date[i]['date'].strftime("%m"),
                          receive_date[i]['date'].strftime("%Y")]
            if month_year[1:3] not in e:
                e.append(month_year[1:3])
                b.append([int(month_year[2]), int(month_year[1])])

    b = sorted(b)

    for i in b:
        i.insert(0, i[1])
        i.pop(2)

    # print(b)

    list_excel2 = []
    month_list = ['01','02','03','04','05','06','07','08','09','10','11','12']
    list_excel2_i = []

    g = []
    for i in b:
        if len(str(i[0])) == 1:
            g.append(["0" + str(i[0]),i[1]])
        else:
            g.append([(i[0]),i[1]])

    # print(g)
    # print(b)

    for item_b in g:
        list_excel2_i = ["{}-{}".format(calendar.month_name[int(item_b[0])][0:3],item_b[1])]
        sql = """SELECT COUNT(customercase.case_id) as count FROM customercase
            LEFT JOIN tracking ON tracking.case_id = customercase.case_id
            {}
            AND tracking.receive_date LIKE '%{}-{}%'""".format(sql_where,item_b[1],item_b[0])
        mycursor.execute(sql, )
        count = mycursor.fetchone()['count']
        # print(item_b[1],item_b[0],count)
        list_excel2_i.append(count)
        for process in ['cancel','process','complete']:
            sql = """SELECT COUNT(customercase.case_id) as count FROM customercase
                    LEFT JOIN tracking ON tracking.case_id = customercase.case_id
                    {} AND
                    tracking.receive_date LIKE '%{}-{}%' AND process = '{}'""".format(sql_where,item_b[1],item_b[0],process)
            mycursor.execute(sql, )
            count = mycursor.fetchone()['count']
            list_excel2_i.append(count)
        list_excel2.append(list_excel2_i)
    mydb.close()

    list_sum_day = ["รวม"]
    for j in range(len(list_excel2[0])):
        if j > 0 :
            sum_day = 0
            for i in range(len(list_excel2)):
                sum_day += list_excel2[i][j]
            list_sum_day.append(sum_day)
    list_sum_day.append("")
    list_sum_day.append("")

    for i in list_excel2:
        i.append("")
        i.append("")

    list_excel2.append(list_sum_day)
    list_excel2.append(['', '', '', '', '', '', '', ''])
    list_excel2.insert(0,["เดือน","เคสที่ได้รับ","เคสที่ยกเลิก","เคสที่ยังไม่สำเร็จ","เคสที่สำเร็จ","ส่งงาน-เงินเข้า","ปิดเล่ม-เงินเข้า","ส่งงาน-ส่งเล่ม"])

    first_day = datetime.datetime.strptime('{}-{}-{}'.format(2,1,datetime.datetime.now().year),"%d-%m-%Y")
    next_day = first_day + datetime.timedelta(days=6)

    week_dict = {}
    while first_day <= datetime.datetime.strptime('{}-{}-{}'.format(30,12,datetime.datetime.now().year),"%d-%m-%Y"):
        if next_day.year == datetime.datetime.now().year:
            if next_day.month not in week_dict.keys():
                week_dict[next_day.month] = [[first_day,next_day]]
            else:
                week_list = week_dict[next_day.month]
                week_list.append([first_day,next_day])
                week_dict[next_day.month] = week_list
        first_day = first_day + datetime.timedelta(days=7)
        next_day = first_day + datetime.timedelta(days=6)

    first_day = datetime.datetime.strptime('{}-{}-{}'.format(2, 1, datetime.datetime.now().year), "%d-%m-%Y")
    next_day = first_day + datetime.timedelta(days=6)
    week_dict2 = {}
    while first_day <= datetime.datetime.strptime('{}-{}-{}'.format(30, 12, datetime.datetime.now().year), "%d-%m-%Y"):
        if next_day.year == datetime.datetime.now().year:
            if next_day.month not in week_dict2.keys():
                week_dict2[next_day.month] = [[str(first_day.day)+"-"+str(first_day.month), str(next_day.day)+"-"+str(next_day.month)]]
            else:
                week_list = week_dict2[next_day.month]
                week_list.append([str(first_day.day)+"-"+str(first_day.month), str(next_day.day)+"-"+str(next_day.month)])
                week_dict2[next_day.month] = week_list
        first_day = first_day + datetime.timedelta(days=7)
        next_day = first_day + datetime.timedelta(days=6)

    # print(week_dict2)

    week_dict3 = {}
    for i in b:
        list_week_dict = []
        for j in week_dict2[i[0]]:
            list_week_dict.append([datetime.datetime.strptime('{}-{}-{}'.format(j[0].split("-")[0], j[0].split("-")[1], i[1]), "%d-%m-%Y"),
                                   datetime.datetime.strptime('{}-{}-{}'.format(j[1].split("-")[0], j[1].split("-")[1], i[1]), "%d-%m-%Y")])
        if i[1] not in week_dict3.keys():
            week_dict3[i[1]] = { i[0] : list_week_dict}
        else:
            week_dict3[i[1]][i[0]] = list_week_dict

    # for key,val in week_dict3.items():
    #     for key2,val2 in val.items():
    #         # print(val[key2])
    # for key,val in week_dict.items():
    #     # print(key,val)

    mydb = connectsql()
    mycursor = mydb.cursor(dictionary=True)
    list_in = []
    list_close = []
    list_submit = []
    list_cash = []

    list_col = []
    for key,val in week_dict3.items():
        for key2,val2 in val.items():
            list_col_i = [calendar.month_name[key2]+"-"+str(key)]
            for i in range(0, len(val2)):
                list_col_i.append("W{}({}-{})".format(i+1,val[key2][i][0].day, val[key2][i][1].day))
            list_col.append(list_col_i)

    for i in list_col:
        if len(i) < 6:
            for j in range(6 - len(i)):
                i.append("")
        i.append("รวม")

    for key,val in week_dict3.items():
        for key2,val2 in val.items():
            list_in_week = []
            list_close_week = []
            list_submit_week = []
            list_cash_week = []
            for i in range(0,len(val2)):
                sql_car = """SELECT count(tracking.id) AS count FROM tracking LEFT JOIN customercase ON tracking.case_id = customercase.case_id
                      {} AND
                      tracking.receive_date BETWEEN '{}' AND '{}'""".format(sql_where,val[key2][i][0],val[key2][i][1],)
                mycursor.execute(sql_car, )
                count = mycursor.fetchone()['count']
                list_in_week.append(count)

                sql_car = """SELECT count(tracking.id) AS count FROM tracking LEFT JOIN customercase ON tracking.case_id = customercase.case_id
                              {} AND
                              tracking.account_closing_date BETWEEN '{}' AND '{}'""".format(sql_where,val[key2][i][0],
                                                                                    val[key2][i][1], )
                mycursor.execute(sql_car, )
                count = mycursor.fetchone()['count']
                list_close_week.append(count)

                sql_car = """SELECT count(tracking.id) AS count FROM tracking LEFT JOIN customercase ON tracking.case_id = customercase.case_id
                              {} AND
                              tracking.deposit_doc_to_new_bank_date BETWEEN '{}' AND '{}'""".format(sql_where,val[key2][i][0],
                                                                                    val[key2][i][1], )
                mycursor.execute(sql_car, )
                count = mycursor.fetchone()['count']
                list_submit_week.append(count)

                sql_car = """SELECT count(tracking.id) AS count FROM tracking LEFT JOIN customercase ON tracking.case_id = customercase.case_id
                              {} AND
                              tracking.cash_received_date BETWEEN '{}' AND '{}'""".format(sql_where,val[key2][i][0],
                                                                                    val[key2][i][1], )
                mycursor.execute(sql_car, )
                count = mycursor.fetchone()['count']
                list_cash_week.append(count)
            list_in.append(list_in_week)
            list_close.append(list_close_week)
            list_submit.append(list_submit_week)
            list_cash.append(list_cash_week)

    mydb.close()

    list_excel = []
    for i in range(0,len(list_in)):
        list_excel.append(list_in[i])
        list_excel.append(list_close[i])
        list_excel.append(list_submit[i])
        list_excel.append(list_cash[i])

    for i in range(len(list_excel)):
        sum_list = sum(list_excel[i])
        if len(list_excel[i]) < 5:
            for j in range(5-len(list_excel[i])):
                list_excel[i].append("")
        list_excel[i].append(sum_list)

    for i in range(len(list_excel)):
        if i%4 == 0:
            list_excel[i].insert(0,"In")
        elif i%4 == 1:
            list_excel[i].insert(0,"Close")
        elif i%4 == 2:
            list_excel[i].insert(0,"Submit")
        else:
            list_excel[i].insert(0,"Cash")

    for i in range(len(list_excel)//4):
        list_excel.insert(i*6,list_col[i])
        list_excel.insert((i*6)+5, ['','','','','','','',''])

    len_data_1 = len(list_excel2)

    for i in list_excel:
        list_excel2.append(i)

    list_col_fill = []
    for i in range(len(list_excel2)):
        if list_excel2[i][0] == "" :
            list_col_fill.append(i+3)

    return {"summary" : list_excel2, "len_data_1" : len_data_1, "list_col_fill" : list_col_fill[:-1], "len": len(list_excel2)}

