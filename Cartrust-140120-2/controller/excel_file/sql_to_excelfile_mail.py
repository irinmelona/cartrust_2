import io
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import pandas as pd
import os
import wget
from flask_api import status
from openpyxl.comments import Comment
from openpyxl.formatting.rule import ColorScaleRule
from openpyxl.styles import PatternFill, Font, Alignment
from tkinter import *
from flask import Flask, Response, request, send_file, jsonify
import datetime
from controller.all_function import *
import yaml

with open(r'configs/configs.yaml',"r") as data:
    data_configs = yaml.load(data, Loader=yaml.FullLoader)



def editExcelDate(real_data):
    data = []
    for item in real_data:
        lst = list(item)
        for i in range(len(lst)):
            if type(lst[i]) == None:
                pass
            elif type(lst[i]) == datetime:
                str_date = lst[i].strftime("%d/%m/%Y")
                lst[i] = str_date
            elif type(lst[i]) == str:
                if lst[i].endswith(".0"):
                    value = lst[i].replace(".0", "")
                    lst[i] = int(value)
        t = tuple(lst)
        data.append(t)
    return data

def allColumn(row, column):
    list_column = []
    for c in range(ord("A"), ord("A") + column + 1):
        if c > 90:
            a = (c - 90) // 26
            b = (c - 90) % 26
            if b == 0:
                column_name = chr(64 + a) + chr(90)
            else:
                column_name = chr(65 + a) + chr(64 + b)
        else:
            column_name = chr(c)
        list_column.append(column_name)
    return list_column

def allCell(row, column):
    all_cell = []
    for ch in allColumn(row, column):
        for i in range(1, row + 1):
            cell = ch + str(i)
            all_cell.append(cell)
    return all_cell

def sqlToExcelMail():
    try:
        data_email = request.json['email']
        parameter = request.args['parameter']
        value = request.args['value']
        sql_where = ""
        if parameter == "all":
            sql_where = ""
            case_str = "เคสทั้งหมดในระบบ"
        elif parameter == "date_update":
            value = value.split("-")
            value[0] = datetime.strptime(value[0], "%d/%m/%Y")
            value[1] = datetime.strptime(str(value[1] + " 23:59:59"), "%d/%m/%Y %H:%M:%S")
            sql_where = "WHERE customercase.date_update BETWEEN '{}' AND '{}'".format(value[0], value[1])
            if value[0] == value[1]:
                case_str = "เคสที่มีการอัพเดตในวันที่ {}".format(value[0])
            else:
                case_str = "เคสที่มีการอัพเดตในช่วงวันที่ {} ถึง {}".format(value[0],value[1])
        elif parameter.endswith("_date") == True:
            value = value.split("-")
            value[0] = datetime.strptime(value[0], "%d/%m/%Y")
            value[1] = datetime.strptime(str(value[1] + " 23:59:59"), "%d/%m/%Y %H:%M:%S")
            sql_where = "WHERE tracking.{} BETWEEN '{}' AND '{}'".format(parameter,value[0], value[1])
            if value[0] == value[1]:
                case_str = "เคสที่ทำขั้นตอน {} ในวันที่ {}".format(parameter,value[0])
            else:
                case_str = "เคสที่ทำขั้นตอน {} ในช่วงวันที่ {} ถึง {}".format(parameter,value[0], value[1])
        elif parameter in ["team","contract_officer","process","status","old_bank","case_type","case_receiver","case_source"]:
            sql_where = "WHERE customercase.{} = '{}' ".format(parameter, value)
            case_str = "เคสที่มี {} เป็น {}".format(parameter,value)
        else:
            return "parameter not found"

        condition = parameter.upper()+"_"+value

        sql = """SELECT 
                customercase.id as case_id,
                customercase.team as team,
                customercase.document_id as document_id,
                customercase.contract_officer as contract_officer,
                customercase.job_id as job_id,
                CONCAT (customer.firstname," ",customer.lastname) as name,
                car.car_license as car_license,
                car.car_province as car_province,
                customercase.case_status as case_status,
                customercase.status as status,
                tracking.receive_date as receive_date,
                 tracking.contact_customer_date as contact_customer_date,
                 tracking.account_closing_date as account_closing_date,
                 tracking.transfer_doc_received_date as transfer_doc_received_date,
                 tracking.transfer_doc_submitted_date as transfer_doc_submitted_date,
                 tracking.book_received_date as book_received_date,
                 tracking.submit_book_transfer_date as submit_book_transfer_date,
                 tracking.car_check_up_date as car_check_up_date,
                 tracking.book_transfer_date as book_transfer_date,
                 tracking.book_copy_received_date as book_copy_received_date,
                 tracking.deposit_doc_to_new_bank_date as deposit_doc_to_new_bank_date,
                 tracking.submit_book_deposit_return_date as submit_book_deposit_return_date,
                 tracking.book_received_back_date as book_received_back_date,
                 tracking.cash_received_date as cash_received_date,
                 tracking.book_deposit_received_date as book_deposit_received_date,
                 tracking.submit_book_to_new_finance_date as submit_book_to_new_finance_date,
                 customercase.old_bank as old_bank,
                 customercase.finance_staff as finance_staff,
                 customercase.approve_amount as approve_amount,
                  customercase.note_status as note_status,
                 customercase.case_type as case_type,
                 customercase.case_receiver as case_receiver,
                 customercase.case_source as case_source,
                 car.car_brand as car_brand,
                 car.car_model as car_model,
                 customercase.date_create as date_create,
                 customercase.date_update as date_update

                     FROM customercase 
                     LEFT JOIN tracking ON customercase.case_id = tracking.case_id 
                     LEFT JOIN car ON customercase.car_id = car.car_id 
                     LEFT JOIN customer ON customercase.customer_id = customer.customer_id {} ;""".format(sql_where)

        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        mycursor.execute(sql, )
        case = mycursor.fetchall()
        if case == []:
            return "no data"

        sql_tracking = """SELECT 
                            customercase.id as id,
                            customercase.case_id as case_id,
                            tracking.contact_customer_note as contact_customer,
                            tracking.account_closing_note as account_closing,
                            tracking.transfer_doc_received_note as transfer_doc_received,
                            tracking.transfer_doc_submitted_note as transfer_doc_submitted,
                            tracking.book_received_note as book_received,
                            tracking.submit_book_transfer_note as submit_book_transfer,
                            tracking.car_check_up_note as car_check_up,
                            tracking.book_transfer_note as book_transfer,
                            tracking.book_copy_received_note as book_copy_received,
                            tracking.deposit_doc_to_new_bank_note as deposit_doc_to_new_bank,
                            tracking.submit_book_deposit_return_note as submit_book_deposit_return,
                            tracking.book_received_back_note as book_received_back,
                            tracking.cash_received_note as cash_received,
                            tracking.book_deposit_received_note as book_deposit_received,
                            tracking.submit_book_to_new_finance_note as submit_book_to_new_finance
                                FROM customercase 
                             LEFT JOIN tracking ON customercase.case_id = tracking.case_id {} ;""".format(
            sql_where)

        mycursor.execute(sql_tracking, )
        note = mycursor.fetchall()

        sql_conditiondate = "SELECT * FROM conditiondate"
        mycursor.execute(sql_conditiondate, )
        condition_date = mycursor.fetchall()

        sql_all_case = "SELECT count(id) as 'all' FROM customercase"
        mycursor.execute(sql_all_case, )
        all_case = mycursor.fetchall()
        sql_status_count = "SELECT status,COUNT(id) AS 'count' FROM customercase WHERE customercase.process = 'process' GROUP by status"
        mycursor.execute(sql_status_count, )
        status_count = mycursor.fetchall()
        sql_process_count = "SELECT process,COUNT(id) AS 'count' FROM customercase GROUP by process"
        mycursor.execute(sql_process_count, )
        process_count = mycursor.fetchall()

        if sql_where != "":
            sql_status_count_where = "SELECT status,COUNT(id) AS 'count' FROM customercase {} AND customercase.process = 'process' GROUP by status".format(sql_where)
            mycursor.execute(sql_status_count_where, )
            status_count_where = mycursor.fetchall()
            sql_process_count_where = "SELECT process,COUNT(id) AS 'count' FROM customercase {} GROUP by process".format(sql_where)
            mycursor.execute(sql_process_count_where, )
            process_count_where = mycursor.fetchall()
        else:
            sql_status_count_where = "SELECT status,COUNT(id) AS 'count' FROM customercase {} WHERE customercase.process = 'process' GROUP by status".format(
                sql_where)
            mycursor.execute(sql_status_count_where, )
            status_count_where = mycursor.fetchall()
            sql_process_count_where = "SELECT process,COUNT(id) AS 'count' FROM customercase {} GROUP by process".format(
                sql_where)
            mycursor.execute(sql_process_count_where, )
            process_count_where = mycursor.fetchall()

        mydb.close()


        list_case = ["receive","contact_customer","account_closing","transfer_doc_received","transfer_doc_submitted",
                     "book_received","submit_book_transfer","car_check_up","book_transfer","book_copy_received",
                     "deposit_doc_to_new_bank","submit_book_deposit_return","book_received_back","cash_received",
                     "book_deposit_received"]

        list_process = ["process", "cancel", "complete"]

        dict_case_text = {  "receive" : "รับเคสแล้ว",
                            "contact_customer" : "ติดต่อลูกค้าแล้ว",
                            "account_closing" : "ปิดเล่มแล้ว",
                            "transfer_doc_received" : "รับชุดโอนแล้ว",
                            "transfer_doc_submitted" : "ยื่นชุดโอนแล้ว",
                            "book_received" : "ได้รับเล่มแล้ว",
                            "submit_book_transfer" : "ส่งงานโอนทะเบียนแล้ว",
                            "car_check_up" : "ตรวจสภาพรถแล้ว",
                            "book_transfer" : "โอนเล่มทะเบียนแล้ว",
                            "book_copy_received" : "รับสำเนาเล่มแล้ว",
                            "deposit_doc_to_new_bank" : "ส่งเอกสารเบิกเงินธนาคารใหม่แล้ว",
                            "submit_book_deposit_return" : "ทำเรื่องเบิกมัดจำคืนแล้ว",
                            "book_received_back" : "รับเล่มคืนแล้ว",
                            "cash_received" : "เงินเข้าบัญชีคาร์ทรัสแล้ว",
                            "book_deposit_received" : "เงินมัดจำคืนเข้าบัญชีแล้ว"}

########################################################################################################################------prepare_data_1

        real_data = []
        for i in range(len(case)):
            data_list = []
            for key, val in case[i].items():
                data_list.append(val)
            t = tuple(data_list)
            real_data.append(t)

        data = editExcelDate(real_data)

        # print(data)

        red = {}
        orange = {}

        for i in range(len(condition_date)):
            red[condition_date[i]['case_type']] = condition_date[i]['red']
            orange[condition_date[i]['case_type']] = condition_date[i]['orange']

        len_data = len(data)
        columns = []
        for key,val in case[0].items():
            columns.append(key)

########################################################################################################################------prepare_data_2

        # print("all_case :", all_case)
        # print("status_count :", status_count)
        # print("process_count :", process_count)
        # print("status_count_where :", status_count_where)
        # print("process_count_where :", process_count_where)

        dict_status_count = {}
        dict_status_count_where = {}
        dict_process_count = {}
        dict_process_count_where = {}

        for i in range(len(status_count)):
            dict_status_count[status_count[i]['status']] = status_count[i]['count']
        for j in range(len(list_case)) :
            if list_case[j] not in dict_status_count:
                dict_status_count[list_case[j]] = ""

        for i in range(len(status_count_where)):
            dict_status_count_where[status_count_where[i]['status']] = status_count_where[i]['count']
        for j in range(len(list_case)):
            if list_case[j] not in dict_status_count_where:
                dict_status_count_where[list_case[j]] = ""

        for i in range(len(process_count)):
            dict_process_count[process_count[i]['process']] = process_count[i]['count']
        for j in range(len(list_process)) :
            if list_process[j] not in dict_process_count:
                dict_process_count[list_process[j]] = ""

        for i in range(len(process_count_where)):
            dict_process_count_where[process_count_where[i]['process']] = process_count_where[i]['count']
        for j in range(len(list_process)) :
            if list_process[j] not in dict_process_count_where:
                dict_process_count_where[list_process[j]] = ""

        # print(len(dict_status_count),dict_status_count)
        # print(len(dict_status_count_where),dict_status_count_where)
        # print(len(dict_process_count), dict_process_count)
        # print(len(dict_process_count_where), dict_process_count_where)

        list_data_case = []
        for k in range(len(list_case)) :
            list_data = []
            list_data.append(k+1)
            list_data.append(dict_case_text[list_case[k]])
            list_data.append(dict_status_count_where[list_case[k]])
            list_data.append(dict_status_count[list_case[k]])
            if list_data[2] == "" and list_data[3] == "":
                list_data.append("")
                list_data.append("")
                list_data.append("")
            elif list_data[2] == "" and list_data[3] != "":
                list_data.insert(3,"")
                list_data.append(str(format(list_data[4]*100/dict_process_count['process'],'.1f'))+"%")
                list_data.append("")
            elif list_data[2] != "" and list_data[3] != "":
                list_data.insert(3, str(format(list_data[2] * 100 / dict_process_count_where['process'],'.1f'))+"%")
                list_data.append(str(format(list_data[4] * 100 / dict_process_count['process'],'.1f'))+"%")
                list_data.append(str(format(list_data[2]*100/list_data[4],'.1f'))+"%")
            list_data_case.append(tuple(list_data))

        dict_process_percent = {}
        for process in list_process:
            if dict_process_count[process] != "" and dict_process_count_where[process] != "":
                dict_process_percent[process] = str(format(dict_process_count_where[process] * 100 / dict_process_count[process],'.1f'))+"%"
            else:
                dict_process_percent[process] = ""

        dict_process_percent_con = {}
        for process in list_process:
            if dict_process_count_where[process] != "" :
                dict_process_percent_con[process] = str(format(dict_process_count_where[process] * 100 / len(case),'.1f'))+"%"
            else:
                dict_process_percent_con[process] = ""

        dict_process_percent_all = {}
        for process in list_process:
            if dict_process_count[process] != "":
                dict_process_percent_all[process] = str(
                    format(dict_process_count[process] * 100 / len(case), '.1f')) + "%"
            else:
                dict_process_percent_all[process] = ""

        list_data_case.append(tuple(["เคสที่กำลังดำเนินการ","",dict_process_count_where['process'],dict_process_percent_con['process'],dict_process_count['process'],dict_process_percent_all['process'],dict_process_percent['process']]))
        list_data_case.append(tuple(["เคสที่ยกเลิก", "", dict_process_count_where['cancel'], dict_process_percent_con['cancel'], dict_process_count['cancel'], dict_process_percent_all['cancel'],dict_process_percent['cancel']]))
        list_data_case.append(tuple(["เคสที่สำเร็จ", "", dict_process_count_where['complete'], dict_process_percent_con['complete'], dict_process_count['complete'], dict_process_percent_all['complete'],dict_process_percent['complete']]))
        list_data_case.append(tuple(["จำนวนเคสทั้งหมด", "", len(case), "", all_case[0]['all'], "",str(format(len(case)*100/all_case[0]['all'],'.1f'))+"%"]))
        # print(list_data_case)

        columns2 = ['No.','ขั้นตอน',condition,"%","เคสทั้งหมด","%","%ALL"]


########################################################################################################################------add_data

        df = pd.DataFrame([tuple(t) for t in data], columns=columns)
        df2 = pd.DataFrame([tuple(t) for t in list_data_case], columns=columns2)

#-----------------------------------------------------------------------------------------------------------------------rename_column

        df.rename(columns={'case_id': 'No.'}, inplace=True)
        df.rename(columns={'team': 'Team'}, inplace=True)
        df.rename(columns={'document_id': 'Document No.\n/ เลขที่ใบคำขอ'}, inplace=True)
        df.rename(columns={'contract_officer': 'Contract Officer \n/ เจ้าหน้าที่ทำสัญญา'}, inplace=True)
        df.rename(columns={'job_id': 'JOB No.'}, inplace=True)
        df.rename(columns={'name': 'Customer Name \n/ ชื่อลูกค้า'}, inplace=True)
        df.rename(columns={'car_license': 'Licence Plate No. \n/ หมายเลขป้ายทะเบียน'}, inplace=True)
        df.rename(columns={'car_province': 'Province \n/ จังหวัดป้ายทะเบียน'}, inplace=True)
        df.rename(columns={'case_status': 'Case Status \n/ สถานะเคส'}, inplace=True)
        df.rename(columns={'status': 'Case Status \n/ สถานะเคส'}, inplace=True)
        df.rename(columns={'receive_date': 'Receive Date \n/ วันที่รับเคส'}, inplace=True)
        df.rename(columns={'contact_customer_date': 'Contact Customer Date \n/ วันที่ติดต่อลูกค้า'}, inplace=True)
        df.rename(columns={'account_closing_date': 'Account Closing Date \n/ วันที่ปิดเล่ม'}, inplace=True)
        df.rename(columns={'transfer_doc_received_date': 'Transfer Doc. Received \n/ วันรับชุดโอน'}, inplace=True)
        df.rename(columns={'transfer_doc_submitted_date': 'Transfer Doc. Submitted \n/ วันยื่นชุดโอน'}, inplace=True)
        df.rename(columns={'book_received_date': 'Book Received Date \n/ วันที่ได้รับเล่ม'}, inplace=True)
        df.rename(columns={'submit_book_transfer_date': 'Submit Book Transfer Date \n/ วันที่ส่งงานโอนทะเบียน'}, inplace=True)
        df.rename(columns={'car_check_up_date': 'Car Check-Up \n/ วันตรวจสภาพรถ'}, inplace=True)
        df.rename(columns={'book_transfer_date': 'Book Transfer Date \n/ โอนเล่มทะเบียน'}, inplace=True)
        df.rename(columns={'book_copy_received_date': 'Book Copy Received \n/ รับสำเนาเล่ม'}, inplace=True)
        df.rename(columns={'deposit_doc_to_new_bank_date': 'ส่งเอกสารเบิกเงินธนาคารใหม่'}, inplace=True)
        df.rename(columns={'submit_book_deposit_return_date': 'Submit Book Deposit Return \n/ ทำเรื่องเบิกมัดจำคืน'}, inplace=True)
        df.rename(columns={'book_received_back_date': 'Book Received \n/ รับเล่มคืน'}, inplace=True)
        df.rename(columns={'cash_received_date': 'Cash Received \n/ เงินเข้าบัญชีคาร์ทรัส'}, inplace=True)
        df.rename(columns={'book_deposit_received_date': 'Book Deposit Received \n/ เงินมัดจำคืนเข้าบัญชี'}, inplace=True)
        df.rename(columns={'submit_book_to_new_finance_date': 'Submit Book to New Finance \n/ ส่งเล่มให้ไฟแนนซ์ใหม่'}, inplace=True)
        df.rename(columns={'old_bank': 'ไฟแนนซ์เดิม'}, inplace=True)
        df.rename(columns={'finance_staff': 'InputStaff'}, inplace=True)
        df.rename(columns={'approve_amount': 'Approved Amount \n/ ยอดจัด'}, inplace=True)
        df.rename(columns={'note_status': 'Update Note'}, inplace=True)
        df.rename(columns={'case_type': 'Case Type \n/ ประเภทเคส'}, inplace=True)
        df.rename(columns={'case_receiver': 'Case Receiver \n/ ผู้ลงข้อมูล'}, inplace=True)
        df.rename(columns={'case_source': 'Case Source / รับเคสจาก'}, inplace=True)
        df.rename(columns={'car_brand': 'Brand / ยี่ห้อ'}, inplace=True)
        df.rename(columns={'car_model': 'Model / รุ่นรถ'}, inplace=True)
        df.rename(columns={'date_create': 'Date Create'}, inplace=True)
        df.rename(columns={'date_update': 'Date Update'}, inplace=True)

        now = datetime.now().strftime("%d%m%y")
        filename = "case_{}_{}.xlsx".format(condition,now)
        excel_writer = pd.ExcelWriter(r'./controller/excel_file/{}'.format(filename), engine="openpyxl")
        df.to_excel(excel_writer, sheet_name=condition)
        df2.to_excel(excel_writer, sheet_name='SUMMARY')

        ws = excel_writer.sheets[condition]
        ws2 = excel_writer.sheets['SUMMARY']

########################################################################################################################------color_fill

        blueFill = PatternFill(start_color='CCFFFF', end_color='CCFFFF', fill_type='solid')
        blue2Fill = PatternFill(start_color='33CCCC', end_color='33CCCC', fill_type='solid')
        greenFill = PatternFill(start_color='CCFFCC', end_color='CCFFCC', fill_type='solid')
        green2Fill = PatternFill(start_color='00FF00', end_color='00FF00', fill_type='solid')
        green3Fill = PatternFill(start_color='339966', end_color='339966', fill_type='solid')
        yellowFill = PatternFill(start_color='FFFF00', end_color='FFFF00', fill_type='solid')
        redFill = PatternFill(start_color='FFFF0000', end_color='FFFF0000', fill_type='solid')
        orangeFill = PatternFill(start_color='FFFFCC00', end_color='FFFFCC00', fill_type='solid')
        brownFill = PatternFill(start_color='993300', end_color='993300', fill_type='solid')
        greyFill = PatternFill(start_color='969696', end_color='969696', fill_type='solid')

        cgreyFill = PatternFill(start_color='575D62', end_color='575D62', fill_type='solid')
        cblueFill = PatternFill(start_color='A7E8FA', end_color='A7E8FA', fill_type='solid')
        cblue2Fill = PatternFill(start_color='58B8FC', end_color='58B8FC', fill_type='solid')
        profill = PatternFill(start_color='99CCFF', end_color='99CCFF', fill_type='solid')
        cancelfill = PatternFill(start_color='F99C95', end_color='F99C95', fill_type='solid')
        comfill = PatternFill(start_color='CCFFCC', end_color='CCFFCC', fill_type='solid')
        allfill = PatternFill(start_color='FFFF99', end_color='FFFF99', fill_type='solid')
        sumfill = PatternFill(start_color='F9CB3C', end_color='F9CB3C', fill_type='solid')

########################################################################################################################------sheet2

# -----------------------------------------------------------------------------------------------------------------------set_color

        for r in range(1,21):
            if ws2['D{}'.format(str(r))].value == "" and ws2['F{}'.format(str(r))].value == "":
                for c1 in ["B", "C", "D", "F", "E", "G", "H"]:
                    cell = c1 + str(r)
                    ws2[cell].fill = cgreyFill
            elif ws2['D{}'.format(str(r))].value != "" and ws2['F{}'.format(str(r))].value != "":
                for c1 in ["B", "C", "D", "F", "E", "G", "H"]:
                    cell = c1 + str(r)
                    ws2[cell].fill = cblueFill
        for c2 in ["B", "C", "D", "F", "E", "G", "H"]:
            ws2['{}1'.format(c2)].fill = cblue2Fill
            ws2['{}17'.format(c2)].fill = profill
            ws2['{}18'.format(c2)].fill = cancelfill
            ws2['{}19'.format(c2)].fill = comfill
            ws2['{}20'.format(c2)].fill = allfill

# -----------------------------------------------------------------------------------------------------------------------set_font

        for r in range(1,21):
            for c in ["A", "B", "C", "D", "F", "E", "G", "H"]:
                cell = c+str(r)
                ws2['{}'.format(cell)].font = Font(name='Calibri', size=10)

        for c5 in ["A", "B", "C", "D", "F", "E", "G", "H"]:
            ws2['{}1'.format(c5)].font = Font(name='Calibri', size=10, bold=True)

        for r2 in range(2, 17):
            ws2['D{}'.format(r2)].font = Font(name='Calibri', size=10, bold=True)
            ws2['E{}'.format(r2)].font = Font(name='Calibri', size=8)
            ws2['F{}'.format(r2)].font = Font(name='Calibri', size=10, bold=True)
            ws2['G{}'.format(r2)].font = Font(name='Calibri', size=8)
            ws2['H{}'.format(r2)].font = Font(name='Calibri', size=10, bold=True)

        ws2['B17'].font = Font(name='Calibri', size=14)
        ws2['B18'].font = Font(name='Calibri', size=14)
        ws2['B19'].font = Font(name='Calibri', size=14)

        ws2['D17'].font = Font(name='Calibri', size=12, bold=True)
        ws2['D18'].font = Font(name='Calibri', size=12, bold=True)
        ws2['D19'].font = Font(name='Calibri', size=12, bold=True)

        ws2['F17'].font = Font(name='Calibri', size=12, bold=True)
        ws2['F18'].font = Font(name='Calibri', size=12, bold=True)
        ws2['F19'].font = Font(name='Calibri', size=12, bold=True)

        ws2['H17'].font = Font(name='Calibri', size=12, bold=True)
        ws2['H18'].font = Font(name='Calibri', size=12, bold=True)
        ws2['H19'].font = Font(name='Calibri', size=12, bold=True)

        ws2['B20'].font = Font(name='Calibri', size=20, bold=True)
        ws2['D20'].font = Font(name='Calibri', size=20, bold=True)
        ws2['F20'].font = Font(name='Calibri', size=20, bold=True)
        ws2['H20'].font = Font(name='Calibri', size=16, bold=True)
# -----------------------------------------------------------------------------------------------------------------------merge_cell_center

        ws2['A2'] = "CASE"
        ws2['A17'] = "STATUS"
        ws2['A20'] = "SUMMARY"

        ws2.merge_cells('A2:A16')
        ws2.merge_cells('A17:A19')
        ws2.merge_cells('B17:C17')
        ws2.merge_cells('B18:C18')
        ws2.merge_cells('B19:C19')
        ws2.merge_cells('B20:C20')

        ws2['A2'].alignment = Alignment(horizontal='center',vertical='center')
        ws2['A17'].alignment = Alignment(horizontal='center',vertical='center')
        ws2['A20'].alignment = Alignment(horizontal='center',vertical='center')

        ws2['A2'].fill = cblue2Fill
        ws2['A17'].fill = greyFill
        ws2['A20'].fill = sumfill

        ws2['B17'].alignment = Alignment(horizontal='center', vertical='center')
        ws2['B18'].alignment = Alignment(horizontal='center', vertical='center')
        ws2['B19'].alignment = Alignment(horizontal='center', vertical='center')
        ws2['B20'].alignment = Alignment(horizontal='center', vertical='center')

        for r in range(1, 21):
            for c3 in ["D", "F", "E", "G", "H"]:
                cell = c3 + str(r)
                ws2[cell].alignment = Alignment(horizontal='center', vertical='center')
        for r in range(2, 17):
           ws2['B{}'.format(str(r))].alignment = Alignment(horizontal='center', vertical='center')
        for c6 in ["A", "B", "C", "D", "F", "E", "G", "H"]:
            cell = c6 + str(1)
            ws2[cell].alignment = Alignment(horizontal='center', vertical='center')

#-----------------------------------------------------------------------------------------------------------------------set_width_height

        ws2.row_dimensions[1].height = 22

        ws2.column_dimensions['A'].width = 9
        ws2.column_dimensions['B'].width = 5
        ws2.column_dimensions['C'].width = 28
        ws2.column_dimensions['D'].width = 9
        ws2.column_dimensions['E'].width = 5
        ws2.column_dimensions['F'].width = 9
        ws2.column_dimensions['G'].width = 5
        ws2.column_dimensions['H'].width = 9

#-----------------------------------------------------------------------------------------------------------------------set_comment2

        if ws2['E17'].value != "":
            ws2['E17'].comment = Comment("({} / {})*100".format(ws2['D17'].value, ws2['D20'].value), "Author")
            ws2['E17'] = str( format( int(ws2['D17'].value) * 100 / int(ws2['D20'].value), '.1f') + "%")
        if ws2['E18'].value != "":
            ws2['E18'].comment = Comment("({} / {})*100".format(ws2['D18'].value, ws2['D20'].value), "Author")
            ws2['E18'] = str(format(int(ws2['D18'].value) * 100 / int(ws2['D20'].value), '.1f') + "%")
        if ws2['E19'].value != "":
            ws2['E19'].comment = Comment("({} / {})*100".format(ws2['D19'].value, ws2['D20'].value), "Author")
            ws2['E19'] = str(format(int(ws2['D19'].value) * 100 / int(ws2['D20'].value), '.1f') + "%")

        if ws2['G17'].value != "":
            ws2['G17'].comment = Comment("({} / {})*100".format(ws2['F17'].value, ws2['F20'].value), "Author")
            ws2['G17'] = str(format(int(ws2['F17'].value) * 100 / int(ws2['F20'].value), '.1f') + "%")
        if ws2['G18'].value != "":
            ws2['G18'].comment = Comment("({} / {})*100".format(ws2['F18'].value, ws2['F20'].value), "Author")
            ws2['G18'] = str(format(int(ws2['F18'].value) * 100 / int(ws2['F20'].value), '.1f') + "%")
        if ws2['G19'].value != "":
            ws2['G19'].comment = Comment("({} / {})*100".format(ws2['F19'].value, ws2['F20'].value), "Author")
            ws2['G19'] = str(format(int(ws2['F19'].value) * 100 / int(ws2['F20'].value), '.1f') + "%")

#-----------------------------------------------------------------------------------------------------------------------edit_sheet2


########################################################################################################################------sheet1

#-----------------------------------------------------------------------------------------------------------------------set_width_height

        ws.row_dimensions[1].height = 57

        for i in range(2,len(data)+2):
            ws.row_dimensions[i].height = 21

        ws.column_dimensions['A'].width = 9
        ws.column_dimensions['B'].width = 9
        ws.column_dimensions['C'].width = 9
        ws.column_dimensions['D'].width = 9
        ws.column_dimensions['E'].width = 23
        ws.column_dimensions['F'].width = 14
        ws.column_dimensions['G'].width = 27
        ws.column_dimensions['H'].width = 20
        ws.column_dimensions['I'].width = 20
        ws.column_dimensions['J'].width = 24

#-----------------------------------------------------------------------------------------------------------------------column_fill

        for i in "ABCDEFGHI":
            ws['{}'.format(i + "1")].fill = blueFill
        ws['J1'].fill = greenFill
        ws['K1'].fill = greenFill
        for i in 'LMNOPQRSTUVWSYXZ':
            ws['{}'.format(i + "1")].fill = yellowFill


        ws['AA1'].fill = yellowFill
        ws['AB1'].fill = orangeFill
        ws['AC1'].fill = orangeFill
        ws['AD1'].fill = green2Fill
        ws['AE1'].fill = brownFill
        ws['AF1'].fill = green3Fill
        ws['AG1'].fill = green3Fill
        ws['AH1'].fill = green3Fill
        ws['AI1'].fill = green3Fill
        ws['AJ1'].fill = green3Fill
        ws['AK1'].fill = blue2Fill
        ws['AL1'].fill = blue2Fill

        data_colunm = {"L": "receive",
                       "M": "contact_customer",
                       "N": "account_closing",
                       "O": "transfer_doc_received",
                       "P": "transfer_doc_submitted",
                       "Q": "book_received",
                       "R": "submit_book_transfer",
                       "S": "car_check_up",
                       "T": "book_transfer",
                       "U": "book_copy_received",
                       "V": "deposit_doc_to_new_bank",
                       "W": "submit_book_deposit_return",
                       "X": "book_received_back",
                       "Y": "cash_received",
                       "Z": "book_deposit_received",
                       "AA": "submit_book_to_new_finance"}

#-----------------------------------------------------------------------------------------------------------------------blank_to_grey_and_font

        row = len(data) + 1
        column = len(data[0])

        all_cell = allCell(row, column)
        for cell in all_cell:
            ws['{}'.format(cell)].font = Font(name='Calibri', size=10)
            if ws['{}'.format(cell)].value == "":
                ws[cell].fill = greyFill

#-----------------------------------------------------------------------------------------------------------------------condition_date

        def char_range(c1, c2):
            for c in range(ord(c1), ord(c2) + 1):
                yield chr(c)
        for c in char_range('L', 'Z'):
            for i in range(2,len_data+2):
                c1 = c
                c2 = chr(ord(c) + 1)
                if c2 == '[':
                    c2 = "AA"
                column_1 = c1 + str(i)
                column_2 = c2 + str(i)
                if ws['{}'.format(column_1)].value == "" :
                    pass
                elif ws['{}'.format(column_2)].value == "" and ws['K{}'.format(i)].value == data_colunm[c1]:
                    if (datetime.now()-datetime.strptime(ws['{}'.format(column_1)].value, "%d/%m/%Y")).days >= red[data_colunm[c2]] :
                        ws['{}'.format(column_2)].fill = redFill
                        if note[i-2][data_colunm[c2]] != None:
                            ws['{}'.format(column_2)].comment = Comment("{}".format(note[i-2][data_colunm[c2]]), "Author")
                    elif (datetime.now()-datetime.strptime(ws['{}'.format(column_1)].value, "%d/%m/%Y")).days >= orange[data_colunm[c2]] :
                        ws['{}'.format(column_2)].fill = orangeFill
                        if note[i-2][data_colunm[c2]] != None:
                            ws['{}'.format(column_2)].comment = Comment("{}".format(note[i-2][data_colunm[c2]]), "Author")
                elif ws['{}'.format(column_2)].value == "" and ws['K{}'.format(i)].value != data_colunm[c1]:
                    pass
                else:
                    date_1 = datetime.strptime(ws['{}'.format(column_1)].value, "%d/%m/%Y")
                    date_2 = datetime.strptime(ws['{}'.format(column_2)].value, "%d/%m/%Y")
                    day_delta = (date_2-date_1).days
                    if day_delta >= red[data_colunm[c2]]:
                        ws['{}'.format(column_2)].fill = redFill
                        if note[i-2][data_colunm[c2]] != None:
                            ws['{}'.format(column_2)].comment = Comment("{}".format(note[i-2][data_colunm[c2]]), "Author")
                    elif day_delta >= orange[data_colunm[c2]]:
                        ws['{}'.format(column_2)].fill = orangeFill
                        if note[i - 2][data_colunm[c2]] != None:
                            ws['{}'.format(column_2)].comment = Comment("{}".format(note[i-2][data_colunm[c2]]), "Author")

#-----------------------------------------------------------------------------------------------------------------------set_bold_font

        row = len(data) + 1
        column = len(data[0])

        all_column = allColumn(row,column)

        for i in all_column:
            ws['{}'.format(i+"1")].font = Font(name='Calibri', size=10, bold=True)

        # case_status

        for i in range(1, row + 1):
            cell = "K" + str(i)
            case_status = ws[cell].value
            if case_status == "submit_book_to_new_finance":
                ws[cell] = "[16] ส่งเล่มให้ไฟแนนซ์ใหม่แล้ว"
            else :
                ws[cell].fill = yellowFill
                if case_status == "book_deposit_received":
                    ws[cell] = "[15] เงินมัดจำคืนเข้าบัญชีแล้ว"
                elif case_status == "cash_received":
                    ws[cell] = "[14] เงินเข้าบัญชีคาร์ทรัสแล้ว"
                elif case_status == "book_received_back":
                    ws[cell] = "[13] รับเล่มคืนแล้ว"
                elif case_status == "submit_book_deposit_return":
                    ws[cell] = "[12] ทำเรื่องเบิกมัดจำคืนแล้ว"
                elif case_status == "deposit_doc_to_new_bank":
                    ws[cell] = "[11] ส่งเอกสารเบิกเงินธนาคารใหม่แล้ว"
                elif case_status == "book_copy_received":
                    ws[cell] = "[10] รับสำเนาเล่มแล้ว"
                elif case_status == "book_transfer":
                    ws[cell] = "[9] โอนเล่มทะเบียนแล้ว"
                elif case_status == "car_check_up":
                    ws[cell] = "[8] ตรวจสภาพรถแล้ว"
                elif case_status == "submit_book_transfer":
                    ws[cell] = "[7] ส่งงานโอนทะเบียนแล้ว"
                elif case_status == "book_received":
                    ws[cell] = "[6] ได้รับเล่มแล้ว"
                elif case_status == "transfer_doc_submitted":
                    ws[cell] = "[5] ยื่นชุดโอนแล้ว"
                elif case_status == "transfer_doc_received":
                    ws[cell] = "[4] รับชุดโอนแล้ว"
                elif case_status == "account_closing":
                    ws[cell] = "[3] ปิดเล่มแล้ว"
                elif case_status == "contact_customer":
                    ws[cell] = "[2] ติดต่อลูกค้าแล้ว"
                elif case_status == "receive":
                    ws[cell] = "[1] รับเคสแล้ว"

        ws.delete_cols(10)
        ws.insert_cols(27, 3)

        def compareDate(date_1, date_2):
            if date_1 == "" or date_2 == "":
                return ""
            else:
                date1 = datetime.strptime(date_1, "%d/%m/%Y")
                date2 = datetime.strptime(date_2, "%d/%m/%Y")
                return str((date1 - date2).days) + " วัน"

        for i_row in range(2, len_data + 2):
            ws['AA{}'.format(i_row)] = compareDate(ws['X{}'.format(i_row)].value, ws['M{}'.format(i_row)].value)
            ws['AA{}'.format(i_row)].font = Font(name='Calibri', size=10)
            ws['AB{}'.format(i_row)] = compareDate(ws['X{}'.format(i_row)].value, ws['L{}'.format(i_row)].value)
            ws['AB{}'.format(i_row)].font = Font(name='Calibri', size=10)
            ws['AC{}'.format(i_row)] = compareDate(ws['Z{}'.format(i_row)].value, ws['P{}'.format(i_row)].value)
            ws['AC{}'.format(i_row)].font = Font(name='Calibri', size=10)

            ws['AF{}'.format(i_row)].alignment = Alignment(horizontal='right', vertical='center')

        ws['AA1'] = "KPI X-M"
        ws['AB1'] = "KPI X-L"
        ws['AC1'] = "KPI Z-P"
        ws['AA1'].fill = PatternFill(start_color='F18560', end_color='F18560', fill_type='solid')
        ws['AB1'].fill = PatternFill(start_color='F18560', end_color='F18560', fill_type='solid')
        ws['AC1'].fill = PatternFill(start_color='F18560', end_color='F18560', fill_type='solid')
        ws['AA1'].font = Font(name='Calibri', size=10, bold=True)
        ws['AB1'].font = Font(name='Calibri', size=10, bold=True)
        ws['AC1'].font = Font(name='Calibri', size=10, bold=True)
        ws.column_dimensions['AA'].width = 9
        ws.column_dimensions['AB'].width = 9
        ws.column_dimensions['AC'].width = 9
        ws['AA1'].alignment = Alignment(horizontal='center', vertical='center')
        ws['AB1'].alignment = Alignment(horizontal='center', vertical='center')
        ws['AC1'].alignment = Alignment(horizontal='center', vertical='center')

        for column_3 in 'ABCDEFGHIJKLMNOPQRSTUVWXYXZ' :
            ws['{}1'.format(column_3)].alignment = Alignment(horizontal='center', vertical='center',wrapText=True)
        for column_5 in 'ABCDEFGHIJKLMN' :
            ws['A{}1'.format(column_5)].alignment = Alignment(horizontal='center', vertical='center', wrapText=True)

        ws.column_dimensions['K'].width = 14

        for i in 'LMNOPQRSTUVWSYXZ':
            ws.column_dimensions[i].width = 14

        ws.column_dimensions['AD'].width = 15
        ws.column_dimensions['AE'].width = 10
        ws.column_dimensions['AF'].width = 15
        ws.column_dimensions['AG'].width = 82
        ws.column_dimensions['AH'].width = 15
        ws.column_dimensions['AI'].width = 25
        ws.column_dimensions['AJ'].width = 15
        ws.column_dimensions['AK'].width = 15
        ws.column_dimensions['AL'].width = 15
        ws.column_dimensions['AM'].width = 15
        ws.column_dimensions['AN'].width = 15

#-----------------------------------------------------------------------------------------------------------------------save_and_send

        excel_writer.save()

        mail_address = {"yahoo": 'smtp.mail.yahoo.com',
                         "hotmail": 'smtp-mail.outlook.com',
                        "outlook": 'smtp-mail.outlook.com',
                        "gmail": 'smtp.gmail.com'}
        host_address = ""

        from_email = data_configs["email_system"]["address"]
        password = data_configs["email_system"]["password"]
        path = r'./controller/excel_file'

        for key, val in mail_address.items():
            if from_email.find(key):
                host_address = val
        if host_address != "":
            s = smtplib.SMTP(host=host_address, port=587)
            s.starttls()
            s.login(from_email, password)
            for to_email in data_email:
                msg = MIMEMultipart()
                msg['From'] = from_email
                msg['To'] = to_email
                msg['Subject'] = "[ CARTRUST ] - Excel Case"
                body = """เรียน ผู้เกี่ยวข้องทุกท่าน
                    เอกสารฉบับนี้เป็นข้อมูลเกี่ยวกับ{}""".format(case_str)
                msg.attach(MIMEText(body, 'plain'))
                attachment = open(os.path.join(path, filename), "rb")
                p = MIMEBase('application', 'octet-stream')
                p.set_payload((attachment).read())
                encoders.encode_base64(p)
                p.add_header('Content-Disposition', "attachment; filename= %s" % filename)
                msg.attach(p)
                text = msg.as_string()
                s.sendmail(from_email, to_email, text)
            s.quit()
            os.remove(os.path.join(path, filename))
            return "success {}".format(filename)
        else:
            return jsonify({ "message" : 'Please use Gmail,Outlook,Hotmail,Yahoo'})


    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR