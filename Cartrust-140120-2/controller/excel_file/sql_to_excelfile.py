
import io

import pandas as pd
import os
import wget
from flask_api import status
from openpyxl.comments import Comment
from openpyxl.formatting.rule import ColorScaleRule, CellIsRule
from openpyxl.styles import PatternFill, Font, Alignment, Border, Side
from tkinter import *
from flask import Flask, Response, request, send_file, jsonify
from datetime import datetime,timedelta
from controller.all_function import *
from controller.excel_file.summary import summary
import calendar

def editExcelDate(real_data):
    data = []
    for item in real_data:
        lst = list(item)
        for i in range(len(lst)):
            if type(lst[i]) == None:
                pass
            elif type(lst[i]) == datetime:
                str_date = lst[i].strftime("%d/%m/%Y")
                lst[i] = str_date
            elif type(lst[i]) == str:
                if lst[i].endswith(".0"):
                    value = lst[i].replace(".0", "")
                    lst[i] = int(value)
        t = tuple(lst)
        data.append(t)
    return data
def allColumn(row, column):
    list_column = []
    for c in range(ord("A"), ord("A") + column + 1):
        if c > 90:
            a = (c - 90) // 26
            b = (c - 90) % 26
            if b == 0:
                column_name = chr(64 + a) + chr(90)
            else:
                column_name = chr(65 + a) + chr(64 + b)
        else:
            column_name = chr(c)
        list_column.append(column_name)
    return list_column
def allCell(row, column):
    all_cell = []
    for ch in allColumn(row, column):
        for i in range(1, row + 1):
            cell = ch + str(i)
            all_cell.append(cell)
    return all_cell
def allColumn2(column):
    list_column = []
    for c in range(ord("A"), ord("A") + column + 1):
        if c > 90:
            a = (c - 90) // 26
            b = (c - 90) % 26
            if b == 0:
                column_name = chr(64 + a) + chr(90)
            else:
                column_name = chr(65 + a) + chr(64 + b)
        else:
            column_name = chr(c)
        list_column.append(column_name)
    return list_column
def allCell2(row, column):
    all_cell = []
    for ch in allColumn2(column):
        for i in range(1, row + 1):
            cell = ch + str(i)
            all_cell.append(cell)
    return all_cell

# start

def sqlToExcel():
    try:
        parameter = request.args['parameter']
        value = request.args['value']
        date = request.args['date']

########################################################################################################################
# excel_TB -------------------------------------------------------------------------------------------------------------

        if parameter=="case_source" and value=="Thanachart":
            condition = ("case_source").upper() + "-" + "Thanachart"

            sql_where = "WHERE customercase.case_source = 'Thanachart'"

            if date != "":
                date = date.split("-")
                date[0] = datetime.strptime(date[0], "%d/%m/%Y")
                date[1] = datetime.strptime(str(date[1] + " 23:59:59"), "%d/%m/%Y %H:%M:%S")
                sql_where += " and tracking.receive_date BETWEEN '{}' AND '{}'".format(date[0], date[1])

            sql = """SELECT
                            customercase.id as case_id,
                            customercase.document_id as document_id,
                            CONCAT (customer.firstname," ",customer.lastname) as name,
                            customer.tel as tel,
                            customer.tel2 as tel2,
                            car.car_license as car_license,
                            car.car_province as car_province,
                            customercase.contract_officer as contract_officer,
                            customercase.hub as hub,
                            customercase.approve_amount as approve_amount,
                            customercase.close_amount as close_amount,
                            customercase.old_bank as old_bank,
                            customercase.job_id as job_id,
                            tracking.receive_date as receive_date,
                            customercase.process as process,
                            customercase.difference as difference,
                            customercase.difference_fee as difference_fee,
                            tracking.contact_customer_date as contact_customer_date,
                            tracking.transfer_doc_received_date as transfer_doc_received_date,
                            tracking.account_closing_date as account_closing_date,
                            customercase.difference_date as difference_date,
                            tracking.transfer_doc_submitted_date as transfer_doc_submitted_date,
                            tracking.book_received_date as book_received_date,
                            tracking.car_check_up_date as car_check_up_date,
                            tracking.book_transfer_date as book_transfer_date,
                            tracking.book_copy_received_date as book_copy_received_date,
                            tracking.deposit_doc_to_new_bank_date as deposit_doc_to_new_bank_date,
                            tracking.submit_book_to_new_finance_date as submit_book_to_new_finance_date,
                            tracking.cash_received_date as cash_received_date,
                            customercase.finance_staff as finance_staff,
                            customercase.note_status as note_status,
                            customercase.status as status,
                            customercase.car_check_con as car_check_con

                                 FROM customercase
                                 LEFT JOIN tracking ON customercase.case_id = tracking.case_id
                                 LEFT JOIN car ON customercase.car_id = car.car_id
                                 LEFT JOIN customer ON customercase.customer_id = customer.customer_id 
                                 {};""".format(sql_where)

            sql_text2 = """SELECT
                        customercase.id,
                         tracking.contact_customer_yn as contact_customer_date,
                         tracking.account_closing_yn as account_closing_date,
                         tracking.transfer_doc_received_yn as transfer_doc_received_date,
                         tracking.transfer_doc_submitted_yn as transfer_doc_submitted_date,
                         tracking.book_received_yn as book_received_date,
                         tracking.submit_book_transfer_yn as submit_book_transfer_date,
                         tracking.car_check_up_yn as car_check_up_date,
                         tracking.book_transfer_yn as book_transfer_date,
                         tracking.book_copy_received_yn as book_copy_received_date,
                         tracking.deposit_doc_to_new_bank_yn as deposit_doc_to_new_bank_date,
                         tracking.submit_book_deposit_return_yn as submit_book_deposit_return_date,
                         tracking.book_received_back_yn as book_received_back_date,
                         tracking.cash_received_yn as cash_received_date,
                         tracking.book_deposit_received_yn as book_deposit_received_date,
                         tracking.submit_book_to_new_finance_yn as submit_book_to_new_finance_date
                         FROM customercase
                                 LEFT JOIN tracking ON customercase.case_id = tracking.case_id
                                 LEFT JOIN car ON customercase.car_id = car.car_id
                                 LEFT JOIN customer ON customercase.customer_id = customer.customer_id 
                                 {};""".format(sql_where)

            sql_tracking = """SELECT 
                                        customercase.id as id,
                                        customercase.case_id as case_id,
                                        tracking.contact_customer_note as contact_customer,
                                        tracking.account_closing_note as account_closing,
                                        tracking.transfer_doc_received_note as transfer_doc_received,
                                        tracking.transfer_doc_submitted_note as transfer_doc_submitted,
                                        tracking.book_received_note as book_received,
                                        tracking.submit_book_transfer_note as submit_book_transfer,
                                        tracking.car_check_up_note as car_check_up,
                                        tracking.book_transfer_note as book_transfer,
                                        tracking.book_copy_received_note as book_copy_received,
                                        tracking.deposit_doc_to_new_bank_note as deposit_doc_to_new_bank,
                                        tracking.submit_book_deposit_return_note as submit_book_deposit_return,
                                        tracking.book_received_back_note as book_received_back,
                                        tracking.cash_received_note as cash_received,
                                        tracking.book_deposit_received_note as book_deposit_received,
                                        tracking.submit_book_to_new_finance_note as submit_book_to_new_finance
                                            FROM customercase 
                                         LEFT JOIN tracking ON customercase.case_id = tracking.case_id
                                         {};""".format(sql_where)

            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            mycursor.execute(sql, )
            case = mycursor.fetchall()
            mycursor.execute(sql_text2, )
            case2 = mycursor.fetchall()
            mycursor.execute(sql_tracking, )
            note = mycursor.fetchall()
            mydb.close()

            if case == []:
                return "no data"

# prepare_date ---------------------------------------------------------------------------------------------------------

            dict_colunm = {1: "receive",
                           2: "contact_customer",
                           3: "account_closing",
                           5: "transfer_doc_received",
                           4: "transfer_doc_submitted",
                           6: "book_received",
                           7: "submit_book_transfer",
                           8: "car_check_up",
                           9: "book_transfer",
                           10: "book_copy_received",
                           11: "deposit_doc_to_new_bank",
                           12: "submit_book_deposit_return",
                           13: "book_received_back",
                           14: "cash_received",
                           15: "book_deposit_received",
                           16: "submit_book_to_new_finance"}

            sql_date_fill = """SELECT
                         customercase.id,
                         tracking.receive_date as receive,
                         tracking.contact_customer_date as contact_customer,
                         tracking.account_closing_date as account_closing,
                         tracking.transfer_doc_received_date as transfer_doc_received,
                         tracking.transfer_doc_submitted_date as transfer_doc_submitted,
                         tracking.book_received_date as book_received,
                         tracking.submit_book_transfer_date as submit_book_transfer,
                         tracking.car_check_up_date as car_check_up,
                         tracking.book_transfer_date as book_transfer,
                         tracking.book_copy_received_date as book_copy_received,
                         tracking.deposit_doc_to_new_bank_date as deposit_doc_to_new_bank,
                         tracking.submit_book_deposit_return_date as submit_book_deposit_return,
                         tracking.book_received_back_date as book_received_back,
                         tracking.cash_received_date as cash_received,
                         tracking.book_deposit_received_date as book_deposit_received,
                         tracking.submit_book_to_new_finance_date as submit_book_to_new_finance,
                         customercase.deposit_12 AS deposit_12,
                         customercase.status AS status
                         FROM customercase
                                 LEFT JOIN tracking ON customercase.case_id = tracking.case_id
                                 LEFT JOIN car ON customercase.car_id = car.car_id
                                 LEFT JOIN customer ON customercase.customer_id = customer.customer_id 
                                 {};""".format(sql_where)

            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql = "{}".format(sql_date_fill)
            mycursor.execute(sql, )
            data = mycursor.fetchall()
            sql2 = "SELECT * FROM conditiondate"
            mycursor.execute(sql2, )
            condition_date = mycursor.fetchall()
            mydb.close()

            red = {}
            orange = {}
            for i in range(len(condition_date)):
                red[condition_date[i]['case_type']] = condition_date[i]['red']
                orange[condition_date[i]['case_type']] = condition_date[i]['orange']

            def compareDate(date_1, date_2):
                if date_1 == None or date_2 == None:
                    return None
                else:
                    return (date_1 - date_2).days

            orange_red = {}
            for key,val in orange.items():
                if val == 0:
                    orange_red[key] = "0.5 days"
                elif val == 1:
                    orange_red[key] = "1 days"
                else:
                    orange_red[key] = "1-{} days".format(orange[key])

            red_fill = {2: [], 3: [], 4: [], 5: [], 6.1: [], 6.2: [], 7: [], 8: [], 9: [], 10: [], 11: [], 12: [],
                        13: [], 14: [], 15: [], 16: []}
            orange_fill = {2: [], 3: [], 4: [], 5: [], 6.1: [], 6.2: [], 7: [], 8: [], 9: [], 10: [], 11: [], 12: [],
                           13: [], 14: [], 15: [], 16: []}

            for i in range(len(data)):
                dict_date = {
                    2: compareDate(data[i][dict_colunm[2]], data[i][dict_colunm[1]]),
                    3: compareDate(data[i][dict_colunm[3]], data[i][dict_colunm[2]]),
                    4: compareDate(data[i][dict_colunm[4]], data[i][dict_colunm[3]]),
                    5: compareDate(data[i][dict_colunm[5]], data[i][dict_colunm[2]]),
                    6.1: compareDate(data[i][dict_colunm[6]], data[i][dict_colunm[3]]),
                    6.2: compareDate(data[i][dict_colunm[6]], data[i][dict_colunm[4]]),
                    7: compareDate(data[i][dict_colunm[7]], data[i][dict_colunm[6]]),
                    8: compareDate(data[i][dict_colunm[8]], data[i][dict_colunm[7]]),
                    9: compareDate(data[i][dict_colunm[9]], data[i][dict_colunm[8]]),
                    10: compareDate(data[i][dict_colunm[10]], data[i][dict_colunm[9]]),
                    11: compareDate(data[i][dict_colunm[11]], data[i][dict_colunm[10]]),
                    12: compareDate(data[i][dict_colunm[12]], data[i][dict_colunm[10]]),
                    13: compareDate(data[i][dict_colunm[13]], data[i][dict_colunm[10]]),
                    14: compareDate(data[i][dict_colunm[14]], data[i][dict_colunm[11]]),
                    15: compareDate(data[i][dict_colunm[15]], data[i][dict_colunm[12]]),
                    16: compareDate(data[i][dict_colunm[16]], data[i][dict_colunm[13]])
                }
                for j in [2, 3, 4, 5, 6.1, 6.2, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]:
                    if int(j) == 6:
                        if dict_date[j] != None:
                            if dict_date[j] > red[dict_colunm[6]]:
                                red_fill[j].append(i + 1)
                            elif dict_date[j] > orange[dict_colunm[6]]:
                                orange_fill[j].append(i + 1)
                    else:
                        if dict_date[j] != None:
                            if dict_date[j] > red[dict_colunm[j]]:
                                red_fill[j].append(i + 1)
                            elif dict_date[j] > orange[dict_colunm[j]]:
                                orange_fill[j].append(i + 1)

            now = datetime.now()

            def get_key(val):
                for key, value in dict_colunm.items():
                    if val == value:
                        return key
                return 0

            for i in range(len(data)):
                if data[i]['status'] != "submit_book_to_new_finance":
                    key_data = get_key(data[i]['status']) + 1
                    if key_data != 1:
                        dict_date_now = {
                            2: compareDate(now, data[i][dict_colunm[1]]),
                            3: compareDate(now, data[i][dict_colunm[2]]),
                            4: compareDate(now, data[i][dict_colunm[3]]),
                            5: compareDate(now, data[i][dict_colunm[2]]),
                            6.1: compareDate(now, data[i][dict_colunm[3]]),
                            6.2: compareDate(now, data[i][dict_colunm[4]]),
                            7: compareDate(now, data[i][dict_colunm[6]]),
                            8: compareDate(now, data[i][dict_colunm[7]]),
                            9: compareDate(now, data[i][dict_colunm[8]]),
                            10: compareDate(now, data[i][dict_colunm[9]]),
                            11: compareDate(now, data[i][dict_colunm[10]]),
                            12: compareDate(now, data[i][dict_colunm[10]]),
                            13: compareDate(now, data[i][dict_colunm[10]]),
                            14: compareDate(now, data[i][dict_colunm[11]]),
                            15: compareDate(now, data[i][dict_colunm[12]]),
                            16: compareDate(now, data[i][dict_colunm[13]])
                        }
                        if key_data == 6:
                            if dict_date_now[6.1] != None:
                                if dict_date_now[6.1] > red[dict_colunm[6]]:
                                    red_fill[6.1].append(i + 1)
                                elif dict_date_now[6.1] > orange[dict_colunm[6]]:
                                    orange_fill[6.1].append(i + 1)
                            if dict_date_now[6.2] != None:
                                if dict_date_now[6.2] > red[dict_colunm[6]]:
                                    red_fill[6.2].append(i + 1)
                                elif dict_date_now[6.2] > orange[dict_colunm[6]]:
                                    orange_fill[6.2].append(i + 1)
                        else:
                            if dict_date_now[key_data] != None:
                                if dict_date_now[key_data] > red[dict_colunm[key_data]]:
                                    red_fill[key_data].append(i + 1)
                                elif dict_date_now[key_data] > orange[dict_colunm[key_data]]:
                                    orange_fill[key_data].append(i + 1)

            list_deposit = []
            list_deposit_no = []
            for i in range(len(data)):
                if data[i]['deposit_12'] == 0 or data[i]['deposit_12'] == None:
                    list_deposit_no.append(i + 1)
                else:
                    list_deposit.append(i + 1)

            for i in list_deposit_no:
                for j in red_fill[6.1]:
                    if i == j:
                        red_fill[6.1].remove(i)

            for i in list_deposit:
                for j in red_fill[6.2]:
                    if i == j:
                        red_fill[6.2].remove(i)

            for i in list_deposit_no:
                for j in orange_fill[6.1]:
                    if i == j:
                        orange_fill[6.1].remove(i)

            for i in list_deposit:
                for j in orange_fill[6.2]:
                    if i == j:
                        orange_fill[6.2].remove(i)

            def unionfn(lst1, lst2):
                final_list = list(set(lst1) | set(lst2))
                return final_list

            red_fill[6] = unionfn(red_fill[6.1], red_fill[6.2])
            orange_fill[6] = unionfn(orange_fill[6.1], orange_fill[6.2])

            del red_fill[6.1]
            del red_fill[6.2]
            del orange_fill[6.1]
            del orange_fill[6.2]

# prepare_data ---------------------------------------------------------------------------------------------------------

            for i in range(len(case2)):
                for key, val in case2[i].items():
                    if val == "no":
                        if key in case[i].keys():
                            case[i][key] = None

            real_data = []
            for i in range(len(case)):
                data_list = []
                for key, val in case[i].items():
                    if val == None:
                        data_list.append("-")
                    elif key == "difference":
                        if val.lower() == "true":
                            data_list.append("2 รับเงินก่อนโอนรถ")
                        elif val.lower() == "false":
                            data_list.append("1 รับเงินหลังโอนรถ")
                        else:
                            data_list.append("-")

                    else:
                        data_list.append(val)
                if data_list[len(data_list) - 1] == "นัดตรวจรถ(บ.)":
                    data_list.insert(22, "-")
                else:
                    data_list.insert(23, "-")

                data_list.insert(30, "1")
                data_list.insert(31, "2")
                data_list.insert(32, "3")
                data_list.insert(33, "4")
                data_list.pop(len(data_list) - 1)
                t = tuple(data_list)
                real_data.append(t)

            data2 = editExcelDate(real_data)

            len_data = len(data2)
            columns = ["ลำดับที่", "เลขที่ใบคำขอ", "ชื่อ-นามสกุล ลูกค้า", "หมายเลขโทร.ติดต่อ (1)",
                       "หมายเลขโทร.ติดต่อ (2)", "ทะเบียนรถ", "ป้ายทะเบียนจังหวัด", "เจ้าหน้าที่ทำสัญญา", "Hub",
                       "ยอดอนุมัติสุทธิ หลังหักค่าใช้จ่ายต่างๆ", "ประมาณการยอดปิดไฟแนนซ์เดิม", "ชื่อไฟแนนซ์เดิม",
                       "Job No.", "วันที่รับงาน", "สถานะล่าสุด", "Option ที่ลูกค้าเลือก",
                       "กรณีเลือก Option 2 ระบุเงินส่วนต่างก้อนแรก", "ติดต่อลูกค้า", "รับชุดโอนจัดเข้าTBANK",
                       "ปิดไฟแนนซ์เดิม", "รับเงินส่วนต่างก้อนแรก", "ยื่นชุดโอน",
                       "รับชุดโอนเอง(กรณีมัดจำเล่ม)", "รับเล่ม(กรณีไฟแนนซ์โอนเอง)", "นัดหมายตรวจสภาพรถ",
                       "โอนเล่มทะเบียนสำเร็จ", "รูปถ่ายสำเนาจากขนส่ง", "ส่งสำเนาเบิกและขอเบิกเงินจากTBANK",
                       "ส่งเล่มทะเบียนตัวจริง", "ลูกค้ารับเงินส่วนที่เหลือ", "Agingจำนวนวันตั้งแต่ส่งงานถึงรับเงิน",
                       "Agingจำนวนวันตั้งแต่ส่งงานถึงรับเงินก้อนแรก", "Agingจำนวนวันตั้งแต่ส่งงานถึงรับเงินก้อนสุดท้าย",
                       "Agingจำนวนวันตั้งแต่ปิดเล่มถึงรับเงินเข้า", "เจ้าหน้าที่ติดต่อลูกค้า", "หมายเหตุ", "status"]

            dict_summary = summary(sql_where)

# import_to_excel ------------------------------------------------------------------------------------------------------

            df = pd.DataFrame([tuple(t) for t in data2], columns=columns)
            df2 = pd.DataFrame([tuple(t) for t in dict_summary['summary']])

            strIO = io.BytesIO()
            excel_writer = pd.ExcelWriter(strIO, engine="openpyxl")
            df.to_excel(excel_writer, sheet_name="Daily Report")
            df2.to_excel(excel_writer, sheet_name='Average')
            ws = excel_writer.sheets["Daily Report"]
            ws2 = excel_writer.sheets['Average']

# manage_excel 2--------------------------------------------------------------------------------------------------------

            ws2.delete_cols(1)
            ws2.delete_rows(1)
            ws2.insert_cols(1,1)
            ws2.insert_rows(1,1)

            thin_border = Border(left=Side(style='thin'),
                                 right=Side(style='thin'),
                                 top=Side(style='thin'),
                                 bottom=Side(style='thin'))

            for i in range(2,dict_summary['len_data_1']+1):
                ws2['B{}'.format(str(i))].fill = PatternFill(start_color='C6BF98', end_color='C6BF98', fill_type='solid')
                ws2['C{}'.format(str(i))].fill = PatternFill(start_color='EEECE1', end_color='EEECE1', fill_type='solid')
                ws2['D{}'.format(str(i))].fill = PatternFill(start_color='F6DAD9', end_color='F6DAD9', fill_type='solid')
                ws2['E{}'.format(str(i))].fill = PatternFill(start_color='FFFFCC', end_color='FFFFCC', fill_type='solid')
                ws2['F{}'.format(str(i))].fill = PatternFill(start_color='DAE7B8', end_color='DAE7B8', fill_type='solid')
                ws2['G{}'.format(str(i))].fill = PatternFill(start_color='DCE6F1', end_color='DCE6F1', fill_type='solid')
                ws2['H{}'.format(str(i))].fill = PatternFill(start_color='DAEEF3', end_color='DAEEF3', fill_type='solid')
                ws2['I{}'.format(str(i))].fill = PatternFill(start_color='B7DEE8', end_color='B7DEE8',fill_type='solid')
                if i != 2:
                    ws2['G{}'.format(str(i))].number_format = '#,##0.00'
                    ws2['H{}'.format(str(i))].number_format = '#,##0.00'
                    ws2['I{}'.format(str(i))].number_format = '#,##0.00'

            for i in dict_summary['list_col_fill']:
                for j in "BCDEFGH":
                    ws2['{}{}'.format(j,str(i))].fill = PatternFill(start_color='DEDAC8', end_color='DEDAC8',
                                                                fill_type='solid')
                    ws2['{}{}'.format(j,str(i))].border = thin_border
                    ws2['{}2'.format(j)].border = thin_border

            for i in range(2,dict_summary['len']+1):
                for j in "BCDEFGHI":
                    ws2['{}{}'.format(j,str(i))].font = Font(name='Calibri', size=10)
                    ws2['{}{}'.format(j,str(i))].alignment = Alignment(horizontal='center')

            for j in "BCDEFGHI":
                ws2.column_dimensions[j].width = 12

            ws2['I2'].border = thin_border

# manage_excel 1--------------------------------------------------------------------------------------------------------

            ws.delete_cols(1)
            ws.insert_cols(1, 1)
            ws.insert_rows(1, 8)

            ws.merge_cells('B1:M1')
            ws.merge_cells('B2:M2')
            ws.merge_cells('B3:M3')
            ws.merge_cells('B5:M5')
            ws.merge_cells('B6:M6')

            ws.merge_cells('Q7:Q9')
            ws.merge_cells('R7:R9')
            ws.merge_cells('S7:AE7')
            ws.merge_cells('AG7:AH7')
            ws.merge_cells('AG8:AH8')

            ws.merge_cells('N5:AK5')
            ws.merge_cells('N6:AK6')
            ws.merge_cells('N5:N6')

            ws['B1'] = "Balance Transfer Report"
            ws['B2'] = "ชื่อตัวแทน Cartrust"
            ws['B3'] = "วันที่ {}".format(datetime.now().strftime("%d/%m/%Y"))
            ws['B6'] = "ข้อมูลจากธนชาต"

            ws['Q7'] = "Option ที่ลูกค้าเลือก รับส่วนต่าง=ก่อน"
            ws['R7'] = "กรณีเลือก Option 2 ระบุเงินส่วนต่างก้อนแรก"
            ws['S7'] = "ระบุวันที่"
            ws['AF7'] = "Option 1"
            ws['AG7'] = "Option 2"

            ws['S8'] = "{}".format(orange_red['contact_customer'])
            ws['T8'] = "-"
            ws['U8'] = "{}".format(orange_red['account_closing'])
            ws['V8'] = "1 days"
            ws['W8'] = "{}".format(orange_red['transfer_doc_submitted'])
            ws['X8'] = "{}".format(orange_red['book_received'])
            ws['Y8'] = "{}".format(orange_red['book_received'])
            ws['Z8'] = "{}".format(orange_red['car_check_up'])
            ws['AA8'] = "{}".format(orange_red['book_transfer'])
            ws['AB8'] = "{}".format(orange_red['book_copy_received'])
            ws['AC8'] = "{}".format(orange_red['deposit_doc_to_new_bank'])
            ws['AD8'] = "{}".format(orange_red['submit_book_to_new_finance'])
            ws['AE8'] = "{}".format(orange_red['cash_received'])

            thin_border = Border(left=Side(style='thin'),
                                 right=Side(style='thin'),
                                 top=Side(style='thin'),
                                 bottom=Side(style='thin'))

# height ---------------------------------------------------------------------------------------------------------------

            ws.row_dimensions[5].height = 5
            ws.row_dimensions[6].height = 30
            ws.row_dimensions[7].height = 37.2
            ws.row_dimensions[9].height = 43.5

            ws.column_dimensions['A'].width = 1.25
            ws.column_dimensions['B'].width = 7.5
            ws.column_dimensions['C'].width = 13.5
            ws.column_dimensions['D'].width = 22.5
            ws.column_dimensions['E'].width = 12
            ws.column_dimensions['F'].width = 12
            ws.column_dimensions['G'].width = 12
            ws.column_dimensions['H'].width = 12
            ws.column_dimensions['I'].width = 17.5
            ws.column_dimensions['J'].width = 10
            ws.column_dimensions['K'].width = 10
            ws.column_dimensions['L'].width = 10
            ws.column_dimensions['M'].width = 18
            ws.column_dimensions['N'].width = 9
            ws.column_dimensions['O'].width = 9.5
            ws.column_dimensions['P'].width = 17
            ws.column_dimensions['Q'].width = 15.5
            ws.column_dimensions['R'].width = 12
            ws.column_dimensions['S'].width = 10
            ws.column_dimensions['T'].width = 10
            ws.column_dimensions['U'].width = 10
            ws.column_dimensions['V'].width = 10
            ws.column_dimensions['W'].width = 10
            ws.column_dimensions['X'].width = 10
            ws.column_dimensions['Y'].width = 10
            ws.column_dimensions['X'].width = 10
            ws.column_dimensions['Z'].width = 10
            ws.column_dimensions['AA'].width = 10
            ws.column_dimensions['AB'].width = 10
            ws.column_dimensions['AC'].width = 10
            ws.column_dimensions['AD'].width = 10
            ws.column_dimensions['AE'].width = 10
            ws.column_dimensions['AF'].width = 10
            ws.column_dimensions['AG'].width = 10
            ws.column_dimensions['AH'].width = 10
            ws.column_dimensions['AI'].width = 10
            ws.column_dimensions['AJ'].width = 7
            ws.column_dimensions['AK'].width = 30

# ----------------------------------------------------------------------------------------------------------------------

            column_border = allColumn2(36)
            column_border.pop(0)

            for i in range(6, len_data + 10):
                for j in column_border:
                    cell = j + str(i)
                    ws[cell].border = thin_border

            for i in "BCDEFGHIJKLMNOP":
                ws['{}7'.format(i)] = ws['{}9'.format(i)].value
                ws['{}9'.format(i)] = ""

            ws['AK7'.format(i)] = ws['AK9'.format(i)].value
            ws['AK9'.format(i)] = ""

# center ---------------------------------------------------------------------------------------------------------------

            for i in range(1, 10):
                for j1 in "BCDEFGHIJKLMNOPQRSTUVWXYZ":
                    ws['{}{}'.format(j1, str(i))].alignment = Alignment(horizontal='center', vertical='center',
                                                                        wrapText=True)
                for j2 in "ABCDEFGHIJK":
                    ws['A{}{}'.format(j2, str(i))].alignment = Alignment(horizontal='center', vertical='center',
                                                                         wrapText=True)

#-----------------------------------------------------------------------------------------------------------------------

            ws['K10'].number_format = '#,##0'

            for i in range(10, len_data + 10):
                ws['K{}'.format(str(i))].number_format = '#,##0'
                ws['L{}'.format(str(i))].number_format = '#,##0'
                ws['R{}'.format(str(i))].number_format = '#,##0'

                if ws['P{}'.format(str(i))].value == 'cancel':
                    ws['P{}'.format(str(i))] = 'ยกเลิก'
                else:
                    case_dict = {
                        "receive": "ติดต่อลูกค้า",
                        "contact_customer": "ปิดเล่ม",
                        "account_closing": "รับชุดโอน",
                        "transfer_doc_submitted": "ยื่นชุดโอน",
                        "transfer_doc_received": "ได้รับเล่ม",
                        "book_received": "ส่งงานโอนทะเบียน",
                        "submit_book_transfer": "ตรวจสภาพรถ",
                        "car_check_up": "โอนเล่มทะเบียน",
                        "book_transfer": "รับสำเนาเล่ม",
                        "book_copy_received": "ส่งเอกสารเบิกเงินธนาคารใหม่",
                        "deposit_doc_to_new_bank": "ทำเรื่องเบิกมัดจำคืน",
                        "submit_book_deposit_return": "รับเล่มคืน",
                        "book_received_back": "เงินเข้าบัญชีคาร์ทรัส",
                        "cash_received": "a",
                        "book_deposit_received": "a",
                        "submit_book_to_new_finance": "a"
                    }
                    if case_dict[ws['AL{}'.format(str(i))].value] == "a":
                        ws['P{}'.format(str(i))] = ws['AE{}'.format(str(i))].value
                    else:
                        ws['P{}'.format(str(i))] = case_dict[ws['AL{}'.format(str(i))].value]


# font -----------------------------------------------------------------------------------------------------------------

            for i in allCell2(len_data + 9, 36):
                ws[i].font = Font(name='Calibri', size=11)

            ws['B1'].font = Font(name='Calibri', size=11, bold=True)
            ws['B2'].font = Font(name='Calibri', size=11, bold=True)
            ws['B3'].font = Font(name='Calibri', size=11, bold=True)
            ws['B5'].font = Font(name='Calibri', size=11, bold=True)

            ws['K7'].font = Font(name='Calibri', size=9)
            ws['L7'].font = Font(name='Calibri', size=9)

            ws['T9'].font = Font(name='Calibri', size=10)
            ws['U9'].font = Font(name='Calibri', size=10)
            ws['V9'].font = Font(name='Calibri', size=10)
            ws['W9'].font = Font(name='Calibri', size=10)

            ws['X9'].font = Font(name='Calibri', size=9)
            ws['Y9'].font = Font(name='Calibri', size=9)

            ws['AA9'].font = Font(name='Calibri', size=10)
            ws['AB9'].font = Font(name='Calibri', size=10)
            ws['AC9'].font = Font(name='Calibri', size=8)
            ws['AD9'].font = Font(name='Calibri', size=10)
            ws['AE9'].font = Font(name='Calibri', size=10)

            ws['AF9'].font = Font(name='Calibri', size=8)
            ws['AG9'].font = Font(name='Calibri', size=8)
            ws['AH9'].font = Font(name='Calibri', size=8)
            ws['AI9'].font = Font(name='Calibri', size=8)
            ws['AJ9'].font = Font(name='Calibri', size=8)

# fill -----------------------------------------------------------------------------------------------------------------

            for i in range(6, 10):
                # orange_fill
                for j in "BCDEFGHIJKLM":
                    ws['{}{}'.format(j, str(i))].fill = PatternFill(start_color='FABF8F', end_color='FABF8F',
                                                                    fill_type='solid')
                # green_fill
                for j2 in "NOPQRSTUVWXYZ":
                    ws['{}{}'.format(j2, str(i))].fill = PatternFill(start_color='C4D79B', end_color='C4D79B',
                                                                     fill_type='solid')
                for j3 in "ABCDEFGHIJK":
                    ws['A{}{}'.format(j3, str(i))].fill = PatternFill(start_color='C4D79B', end_color='C4D79B',
                                                                      fill_type='solid')

            for i in range(10, len_data + 10):
                if ws['P{}'.format(str(i))].value == "ยกเลิก":
                    for j2 in "BCDEFGHIJKLMNOPQRSTUVWXYZ":
                        ws['{}{}'.format(j2, str(i))].fill = PatternFill(start_color='5E5F5C', end_color='5E5F5C',
                                                                         fill_type='solid')
                    for j3 in "ABCDEFGHIJK":
                        ws['A{}{}'.format(j3, str(i))].fill = PatternFill(start_color='5E5F5C', end_color='5E5F5C',
                                                                          fill_type='solid')
                for j5 in "BCHJKLNOPRSTUVWXYZ":
                    ws['{}{}'.format(j5, i)].alignment = Alignment(horizontal='center')
                for j6 in "ABCDE":
                    ws['A{}{}'.format(j6, i)].alignment = Alignment(horizontal='center')

            # img = Image.open(r'./controller/excel_file/TB_icon.png')
            # ws.add_image(img, 'A1')

            ws.delete_cols(38)

            for i in range(10, len_data + 10):
                if ws['R{}'.format(i)].value == "0" or ws['R{}'.format(i)].value == 0:
                    ws['R{}'.format(i)] = "-"
                for j2 in "STUWZ":
                    if ws['{}{}'.format(j2, str(i))].value == "-":
                        ws['{}{}'.format(j2, str(i))] = ""
                for j3 in "ABCDE":
                    if ws['A{}{}'.format(j3, str(i))].value == "-":
                        ws['A{}{}'.format(j3, str(i))] = ""

# cal_date -------------------------------------------------------------------------------------------------------------

            for i in range(10, len_data + 10):
                ws['AF{}'.format(str(i))] = "=IF(AE{}<>\"\",AE{}-O{},0)".format(i, i, i)
                ws['AG{}'.format(str(i))] = "=IF(Q{}=\"2 รับเงินก่อนโอนรถ\",IF(V{}<>\"-\",(V{}-O{}),(TODAY()-O{})),0)".format(
                    i,i,i,i,i)
                ws['AH{}'.format(str(i))] = "=IF(Q{}=\"2 รับเงินก่อนโอนรถ\",IF(AE{}<>\"\",(AE{}-O{}),(TODAY()-O{})),0)".format(
                    i,i,i,i,i)
                ws['AI{}'.format(str(i))] = "=IF(AE{}<>\"\",AE{}-U{},0)".format(i,i,i)

            redFill = PatternFill(start_color='FFFF0000', end_color='FFFF0000', fill_type='solid')
            orangeFill = PatternFill(start_color='FFFFCC00', end_color='FFFFCC00', fill_type='solid')



            for i in red_fill[2]:
                ws['S{}'.format(i+9)].fill = redFill
                ws['S{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[2]], "Author")
            for i in orange_fill[2]:
                ws['S{}'.format(i+9)].fill = orangeFill
                ws['S{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[2]], "Author")

            for i in red_fill[3]:
                ws['U{}'.format(i+9)].fill = redFill
                ws['U{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[3]], "Author")
            for i in orange_fill[3]:
                ws['U{}'.format(i+9)].fill = orangeFill
                ws['U{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[3]], "Author")

            for i in red_fill[4]:
                ws['W{}'.format(i+9)].fill = redFill
                ws['W{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[4]], "Author")
            for i in orange_fill[4]:
                ws['W{}'.format(i+9)].fill = orangeFill
                ws['W{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[4]], "Author")

            for i in red_fill[5]:
                ws['T{}'.format(i+9)].fill = redFill
                ws['T{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[5]], "Author")
            for i in orange_fill[5]:
                ws['T{}'.format(i+9)].fill = orangeFill
                ws['T{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[5]], "Author")

            for i in red_fill[6]:
                ws['X{}'.format(i+9)].fill = redFill
                ws['Y{}'.format(i+9)].fill = redFill
                ws['X{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[6]], "Author")
                ws['Y{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[6]], "Author")
            for i in orange_fill[6]:
                ws['X{}'.format(i+9)].fill = orangeFill
                ws['Y{}'.format(i+9)].fill = orangeFill
                ws['X{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[6]], "Author")
                ws['Y{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[6]], "Author")

            for i in red_fill[8]:
                ws['Z{}'.format(i+9)].fill = redFill
                ws['Z{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[8]], "Author")
            for i in orange_fill[5]:
                ws['Z{}'.format(i+9)].fill = orangeFill
                ws['Z{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[8]], "Author")

            for i in red_fill[9]:
                ws['AA{}'.format(i+9)].fill = redFill
                ws['AA{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[9]], "Author")
            for i in orange_fill[9]:
                ws['AA{}'.format(i+9)].fill = orangeFill
                ws['AA{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[9]], "Author")

            for i in red_fill[10]:
                ws['AB{}'.format(i+9)].fill = redFill
                ws['AB{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[10]], "Author")
            for i in orange_fill[10]:
                ws['AB{}'.format(i+9)].fill = orangeFill
                ws['AB{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[10]], "Author")

            for i in red_fill[11]:
                ws['AC{}'.format(i+9)].fill = redFill
                ws['AC{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[11]], "Author")
            for i in orange_fill[11]:
                ws['AC{}'.format(i+9)].fill = orangeFill
                ws['AC{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[11]], "Author")

            for i in red_fill[14]:
                ws['AE{}'.format(i+9)].fill = redFill
                ws['AE{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[14]], "Author")
            for i in orange_fill[14]:
                ws['AE{}'.format(i+9)].fill = orangeFill
                ws['AE{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[14]], "Author")

            for i in red_fill[16]:
                ws['AD{}'.format(i+9)].fill = redFill
                ws['AD{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[16]], "Author")
            for i in orange_fill[16]:
                ws['AD{}'.format(i+9)].fill = orangeFill
                ws['AD{}'.format(i + 9)].comment = Comment(note[i - 1][dict_colunm[16]], "Author")

# average

            b = []
            e = []
            for i in range(10,len_data+10):
                month_year = str(ws['O{}'.format(i)].value).split("/")
                if len(month_year) == 3:
                    if month_year[1:3] not in e:
                        e.append(month_year[1:3])
                        b.append([int(month_year[2]),int(month_year[1]),i])

            b = sorted(b)

            for i in b:
                i.insert(0, i[1])
                i.pop(2)

            c = b

            for i in range(len(c)):
                if i != len(c) - 1:
                    c[i].append(c[i + 1][2] - 1)
                else:
                    c[i].append(len_data + 2)

            ws.insert_cols(36, 1)

            for i in range(10, len_data + 10):
                ws['AJ{}'.format(str(i))] = "=IF(AD{}<>\"\",AD{}-O{},0)".format(i, i, i)


            for i in range(len(c)):
                ws2['G{}'.format(i + 3)] = "=AVERAGE('Daily Report'!AF{}:'Daily Report'!AF{})".format(c[i][2], c[i][3])
                ws2['H{}'.format(i + 3)] = "=AVERAGE('Daily Report'!AI{}:'Daily Report'!AI{})".format(c[i][2], c[i][3])
                ws2['I{}'.format(i + 3)] = "=AVERAGE('Daily Report'!AJ{}:'Daily Report'!AJ{})".format(c[i][2], c[i][3])

            redFill = PatternFill(start_color='FFC7CE', end_color='FFC7CE', fill_type='solid')
            ws.conditional_formatting.add('AF10:AJ{}'.format(str(len_data + 9)), CellIsRule(operator='greaterThan',
                                                                                            formula=['28'],
                                                                                            stopIfTrue=True,
                                                                                            fill=redFill,
                                                                                            font=Font(color='9C0006')))

            ws['AJ9'] = "Agingจำนวนวันตั้งแต่ส่งงานถึงส่งเล่ม"
            for i in range(6, len_data + 10):
                ws['AJ{}'.format(i)].border = thin_border
                ws['AJ{}'.format(i)].font = Font(name='Calibri', size=11)
            for i in range(6, 10):
                ws['AJ{}'.format(i)].fill = PatternFill(start_color='C4D79B', end_color='C4D79B', fill_type='solid')

            ws['AJ9'].font = Font(name='Calibri', size=8)
            ws['AJ9'].alignment = Alignment(horizontal='center', vertical='center', wrapText=True)
            ws.column_dimensions['AJ'].width = 12


            ws.freeze_panes = 'H10'

            ws.sheet_view.showGridLines = False

            excel_writer.save()
            excel_data = strIO.getvalue()
            strIO.seek(0)
            now = datetime.now().strftime("%d-%m-%Y")
            return send_file(strIO, attachment_filename="case {} {}.xlsx".format(condition, now), as_attachment=True)

########################################################################################################################
# excel_KK -------------------------------------------------------------------------------------------------------------

        elif parameter == "all":
            sql_where = ""
        elif parameter in ["cqc_team","team","contract_officer","process","status","old_bank","case_type","case_receiver","case_source"]:
            sql_where = "WHERE customercase.{} = '{}' ".format(parameter, value)
        else:
            return "parameter not found"

        if date != "":
            date = date.split("-")
            date[0] = datetime.strptime(date[0], "%d/%m/%Y")
            date[1] = datetime.strptime(str(date[1] + " 23:59:59"), "%d/%m/%Y %H:%M:%S")
            if sql_where != "":
                sql_where += " and tracking.receive_date BETWEEN '{}' AND '{}'".format(date[0], date[1])
            else:
                sql_where = "WHERE tracking.receive_date BETWEEN '{}' AND '{}'".format(date[0], date[1])

        condition = parameter.upper()+"-"+value

        sql = """SELECT 
                customercase.id as case_id,
                customercase.team as team,
                customercase.document_id as document_id,
                customercase.contract_officer as contract_officer,
                customercase.job_id as job_id,
                CONCAT (customer.firstname," ",customer.lastname) as name,
                car.car_license as car_license,
                car.car_province as car_province,
                customercase.process as process,
                customercase.status as status,
                tracking.receive_date as receive_date,
                 tracking.contact_customer_date as contact_customer_date,
                 tracking.account_closing_date as account_closing_date,
                 tracking.transfer_doc_received_date as transfer_doc_received_date,
                 tracking.transfer_doc_submitted_date as transfer_doc_submitted_date,
                 tracking.book_received_date as book_received_date,
                 tracking.submit_book_transfer_date as submit_book_transfer_date,
                 tracking.car_check_up_date as car_check_up_date,
                 tracking.book_transfer_date as book_transfer_date,
                 tracking.book_copy_received_date as book_copy_received_date,
                 tracking.deposit_doc_to_new_bank_date as deposit_doc_to_new_bank_date,
                 tracking.cash_received_date as cash_received_date,
                 tracking.book_received_back_date as book_received_back_date,
                 tracking.submit_book_to_new_finance_date as submit_book_to_new_finance_date,
                 tracking.submit_book_deposit_return_date as submit_book_deposit_return_date,
                 tracking.book_deposit_received_date as book_deposit_received_date,
                 customercase.old_bank as old_bank,
                 customercase.finance_staff as finance_staff,
                 customercase.approve_amount as approve_amount,
                  customercase.note_status as note_status,
                 customercase.case_type as case_type,
                 customercase.case_receiver as case_receiver,
                 customercase.case_source as case_source,
                 car.car_brand as car_brand,
                 car.car_model as car_model,
                 customercase.date_create as date_create,
                 customercase.date_update as date_update

                     FROM customercase 
                     LEFT JOIN tracking ON customercase.case_id = tracking.case_id 
                     LEFT JOIN car ON customercase.car_id = car.car_id 
                     LEFT JOIN customer ON customercase.customer_id = customer.customer_id {} ;""".format(sql_where)

        sql_text2 = """SELECT
                                customercase.id,
                                 tracking.contact_customer_yn as contact_customer_date,
                                 tracking.account_closing_yn as account_closing_date,
                                 tracking.transfer_doc_received_yn as transfer_doc_received_date,
                                 tracking.transfer_doc_submitted_yn as transfer_doc_submitted_date,
                                 tracking.book_received_yn as book_received_date,
                                 tracking.submit_book_transfer_yn as submit_book_transfer_date,
                                 tracking.car_check_up_yn as car_check_up_date,
                                 tracking.book_transfer_yn as book_transfer_date,
                                 tracking.book_copy_received_yn as book_copy_received_date,
                                 tracking.deposit_doc_to_new_bank_yn as deposit_doc_to_new_bank_date,
                                 tracking.submit_book_deposit_return_yn as submit_book_deposit_return_date,
                                 tracking.book_received_back_yn as book_received_back_date,
                                 tracking.cash_received_yn as cash_received_date,
                                 tracking.book_deposit_received_yn as book_deposit_received_date,
                                 tracking.submit_book_to_new_finance_yn as submit_book_to_new_finance_date
                                 FROM customercase 
                     LEFT JOIN tracking ON customercase.case_id = tracking.case_id 
                     LEFT JOIN car ON customercase.car_id = car.car_id 
                     LEFT JOIN customer ON customercase.customer_id = customer.customer_id {} ;""".format(sql_where)

        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        mycursor.execute(sql, )
        case = mycursor.fetchall()
        mycursor.execute(sql_text2, )
        case2 = mycursor.fetchall()
        if case == []:
            return "no data"

        sql_tracking = """SELECT 
                            customercase.id as id,
                            customercase.case_id as case_id,
                            tracking.contact_customer_note as contact_customer,
                            tracking.account_closing_note as account_closing,
                            tracking.transfer_doc_received_note as transfer_doc_received,
                            tracking.transfer_doc_submitted_note as transfer_doc_submitted,
                            tracking.book_received_note as book_received,
                            tracking.submit_book_transfer_note as submit_book_transfer,
                            tracking.car_check_up_note as car_check_up,
                            tracking.book_transfer_note as book_transfer,
                            tracking.book_copy_received_note as book_copy_received,
                            tracking.deposit_doc_to_new_bank_note as deposit_doc_to_new_bank,
                            tracking.submit_book_deposit_return_note as submit_book_deposit_return,
                            tracking.book_received_back_note as book_received_back,
                            tracking.cash_received_note as cash_received,
                            tracking.book_deposit_received_note as book_deposit_received,
                            tracking.submit_book_to_new_finance_note as submit_book_to_new_finance
                                FROM customercase 
                             LEFT JOIN tracking ON customercase.case_id = tracking.case_id {} ;""".format(
            sql_where)

        mycursor.execute(sql_tracking, )
        note = mycursor.fetchall()

        mydb.close()

        list_case = ["receive","contact_customer","account_closing","transfer_doc_received","transfer_doc_submitted",
                     "book_received","submit_book_transfer","car_check_up","book_transfer","book_copy_received",
                     "deposit_doc_to_new_bank","submit_book_deposit_return","book_received_back","cash_received",
                     "book_deposit_received"]

        list_process = ["process", "cancel", "complete"]

        dict_case_text = {  "receive" : "รับเคสแล้ว",
                            "contact_customer" : "ติดต่อลูกค้าแล้ว",
                            "account_closing" : "ปิดเล่มแล้ว",
                            "transfer_doc_received" : "รับชุดโอนแล้ว",
                            "transfer_doc_submitted" : "ยื่นชุดโอนแล้ว",
                            "book_received" : "ได้รับเล่มแล้ว",
                            "submit_book_transfer" : "ส่งงานโอนทะเบียนแล้ว",
                            "car_check_up" : "ตรวจสภาพรถแล้ว",
                            "book_transfer" : "โอนเล่มทะเบียนแล้ว",
                            "book_copy_received" : "รับสำเนาเล่มแล้ว",
                            "deposit_doc_to_new_bank" : "ส่งเอกสารเบิกเงินธนาคารใหม่แล้ว",
                            "submit_book_deposit_return" : "ทำเรื่องเบิกมัดจำคืนแล้ว",
                            "book_received_back" : "รับเล่มคืนแล้ว",
                            "cash_received" : "เงินเข้าบัญชีคาร์ทรัสแล้ว",
                            "book_deposit_received" : "เงินมัดจำคืนเข้าบัญชีแล้ว"}

# prepare_date ---------------------------------------------------------------------------------------------------------

        dict_colunm = {1: "receive",
                       2: "contact_customer",
                       3: "account_closing",
                       5: "transfer_doc_received",
                       4: "transfer_doc_submitted",
                       6: "book_received",
                       7: "submit_book_transfer",
                       8: "car_check_up",
                       9: "book_transfer",
                       10: "book_copy_received",
                       11: "deposit_doc_to_new_bank",
                       12: "submit_book_deposit_return",
                       13: "book_received_back",
                       14: "cash_received",
                       15: "book_deposit_received",
                       16: "submit_book_to_new_finance"}

        sql_date_fill = """SELECT
                                 customercase.id,
                                 tracking.receive_date as receive,
                                 tracking.contact_customer_date as contact_customer,
                                 tracking.account_closing_date as account_closing,
                                 tracking.transfer_doc_received_date as transfer_doc_received,
                                 tracking.transfer_doc_submitted_date as transfer_doc_submitted,
                                 tracking.book_received_date as book_received,
                                 tracking.submit_book_transfer_date as submit_book_transfer,
                                 tracking.car_check_up_date as car_check_up,
                                 tracking.book_transfer_date as book_transfer,
                                 tracking.book_copy_received_date as book_copy_received,
                                 tracking.deposit_doc_to_new_bank_date as deposit_doc_to_new_bank,
                                 tracking.submit_book_deposit_return_date as submit_book_deposit_return,
                                 tracking.book_received_back_date as book_received_back,
                                 tracking.cash_received_date as cash_received,
                                 tracking.book_deposit_received_date as book_deposit_received,
                                 tracking.submit_book_to_new_finance_date as submit_book_to_new_finance,
                                 customercase.deposit_12 AS deposit_12,
                                 customercase.status AS status
                                 FROM customercase 
                     LEFT JOIN tracking ON customercase.case_id = tracking.case_id 
                     LEFT JOIN car ON customercase.car_id = car.car_id 
                     LEFT JOIN customer ON customercase.customer_id = customer.customer_id {} ;""".format(sql_where)

        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql = "{}".format(sql_date_fill)
        mycursor.execute(sql, )
        data = mycursor.fetchall()
        sql2 = "SELECT * FROM conditiondate"
        mycursor.execute(sql2, )
        condition_date = mycursor.fetchall()
        mydb.close()

        red = {}
        orange = {}
        for i in range(len(condition_date)):
            red[condition_date[i]['case_type']] = condition_date[i]['red']
            orange[condition_date[i]['case_type']] = condition_date[i]['orange']

        def compareDate(date_1, date_2):
            if date_1 == None or date_2 == None:
                return None
            else:
                return (date_1 - date_2).days

        red_fill = {2: [], 3: [], 4: [], 5: [], 6.1: [], 6.2: [], 7: [], 8: [], 9: [], 10: [], 11: [], 12: [],
                    13: [], 14: [], 15: [], 16: []}
        orange_fill = {2: [], 3: [], 4: [], 5: [], 6.1: [], 6.2: [], 7: [], 8: [], 9: [], 10: [], 11: [], 12: [],
                       13: [], 14: [], 15: [], 16: []}

        for i in range(len(data)):
            dict_date = {
                2: compareDate(data[i][dict_colunm[2]], data[i][dict_colunm[1]]),
                3: compareDate(data[i][dict_colunm[3]], data[i][dict_colunm[2]]),
                4: compareDate(data[i][dict_colunm[4]], data[i][dict_colunm[3]]),
                5: compareDate(data[i][dict_colunm[5]], data[i][dict_colunm[2]]),
                6.1: compareDate(data[i][dict_colunm[6]], data[i][dict_colunm[3]]),
                6.2: compareDate(data[i][dict_colunm[6]], data[i][dict_colunm[4]]),
                7: compareDate(data[i][dict_colunm[7]], data[i][dict_colunm[6]]),
                8: compareDate(data[i][dict_colunm[8]], data[i][dict_colunm[7]]),
                9: compareDate(data[i][dict_colunm[9]], data[i][dict_colunm[8]]),
                10: compareDate(data[i][dict_colunm[10]], data[i][dict_colunm[9]]),
                11: compareDate(data[i][dict_colunm[11]], data[i][dict_colunm[10]]),
                12: compareDate(data[i][dict_colunm[12]], data[i][dict_colunm[10]]),
                13: compareDate(data[i][dict_colunm[13]], data[i][dict_colunm[10]]),
                14: compareDate(data[i][dict_colunm[14]], data[i][dict_colunm[11]]),
                15: compareDate(data[i][dict_colunm[15]], data[i][dict_colunm[12]]),
                16: compareDate(data[i][dict_colunm[16]], data[i][dict_colunm[13]])
            }
            for j in [2, 3, 4, 5, 6.1, 6.2, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]:
                if int(j) == 6:
                    if dict_date[j] != None:
                        if dict_date[j] > red[dict_colunm[6]]:
                            red_fill[j].append(i + 1)
                        elif dict_date[j] > orange[dict_colunm[6]]:
                            orange_fill[j].append(i + 1)
                else:
                    if dict_date[j] != None:
                        if dict_date[j] > red[dict_colunm[j]]:
                            red_fill[j].append(i + 1)
                        elif dict_date[j] > orange[dict_colunm[j]]:
                            orange_fill[j].append(i + 1)

        now = datetime.now()

        def get_key(val):
            for key, value in dict_colunm.items():
                if val == value:
                    return key
            return 0

        for i in range(len(data)):
            if data[i]['status'] != "submit_book_to_new_finance":
                key_data = get_key(data[i]['status']) + 1
                if key_data != 1:
                    dict_date_now = {
                        2: compareDate(now, data[i][dict_colunm[1]]),
                        3: compareDate(now, data[i][dict_colunm[2]]),
                        4: compareDate(now, data[i][dict_colunm[3]]),
                        5: compareDate(now, data[i][dict_colunm[2]]),
                        6.1: compareDate(now, data[i][dict_colunm[3]]),
                        6.2: compareDate(now, data[i][dict_colunm[4]]),
                        7: compareDate(now, data[i][dict_colunm[6]]),
                        8: compareDate(now, data[i][dict_colunm[7]]),
                        9: compareDate(now, data[i][dict_colunm[8]]),
                        10: compareDate(now, data[i][dict_colunm[9]]),
                        11: compareDate(now, data[i][dict_colunm[10]]),
                        12: compareDate(now, data[i][dict_colunm[10]]),
                        13: compareDate(now, data[i][dict_colunm[10]]),
                        14: compareDate(now, data[i][dict_colunm[11]]),
                        15: compareDate(now, data[i][dict_colunm[12]]),
                        16: compareDate(now, data[i][dict_colunm[13]])
                    }
                    if key_data == 6:
                        if dict_date_now[6.1] != None:
                            if dict_date_now[6.1] > red[dict_colunm[6]]:
                                red_fill[6.1].append(i + 1)
                            elif dict_date_now[6.1] > orange[dict_colunm[6]]:
                                orange_fill[6.1].append(i + 1)
                        if dict_date_now[6.2] != None:
                            if dict_date_now[6.2] > red[dict_colunm[6]]:
                                red_fill[6.2].append(i + 1)
                            elif dict_date_now[6.2] > orange[dict_colunm[6]]:
                                orange_fill[6.2].append(i + 1)
                    else:
                        if dict_date_now[key_data] != None:
                            if dict_date_now[key_data] > red[dict_colunm[key_data]]:
                                red_fill[key_data].append(i + 1)
                            elif dict_date_now[key_data] > orange[dict_colunm[key_data]]:
                                orange_fill[key_data].append(i + 1)

        list_deposit = []
        list_deposit_no = []
        for i in range(len(data)):
            if data[i]['deposit_12'] == 0 or data[i]['deposit_12'] == None:
                list_deposit_no.append(i + 1)
            else:
                list_deposit.append(i + 1)

        for i in list_deposit_no:
            for j in red_fill[6.1]:
                if i == j:
                    red_fill[6.1].remove(i)

        for i in list_deposit:
            for j in red_fill[6.2]:
                if i == j:
                    red_fill[6.2].remove(i)

        for i in list_deposit_no:
            for j in orange_fill[6.1]:
                if i == j:
                    orange_fill[6.1].remove(i)

        for i in list_deposit:
            for j in orange_fill[6.2]:
                if i == j:
                    orange_fill[6.2].remove(i)

        def unionfn(lst1, lst2):
            final_list = list(set(lst1) | set(lst2))
            return final_list

        red_fill[6] = unionfn(red_fill[6.1], red_fill[6.2])
        orange_fill[6] = unionfn(orange_fill[6.1], orange_fill[6.2])

        del red_fill[6.1]
        del red_fill[6.2]
        del orange_fill[6.1]
        del orange_fill[6.2]

# prepare_data ---------------------------------------------------------------------------------------------------------

        for i in range(len(case2)):
            for key, val in case2[i].items():
                if val == "no":
                    if key in case[i].keys():
                        case[i][key] = None

        real_data = []
        for i in range(len(case)):
            data_list = []
            for key, val in case[i].items():
                data_list.append(val)
            t = tuple(data_list)
            real_data.append(t)

        data2 = editExcelDate(real_data)

        red = {}
        orange = {}

        for i in range(len(condition_date)):
            red[condition_date[i]['case_type']] = condition_date[i]['red']
            orange[condition_date[i]['case_type']] = condition_date[i]['orange']

        orange_red = {}
        for key, val in orange.items():
            if val == 0:
                orange_red[key] = "0.5 days"
            elif val == 1:
                orange_red[key] = "1 days"
            else:
                orange_red[key] = "1-{} days".format(orange[key])

        len_data = len(data2)
        columns = []
        for key,val in case[0].items():
            columns.append(key)

# prepare_data_2 -------------------------------------------------------------------------------------------------------

        dict_summary = summary(sql_where)

########################################################################################################################------add_data

        df = pd.DataFrame([tuple(t) for t in data2], columns=columns)
        df2 = pd.DataFrame([tuple(t) for t in dict_summary['summary']])

#-----------------------------------------------------------------------------------------------------------------------rename_column

        df.rename(columns={'case_id': 'No.'}, inplace=True)
        df.rename(columns={'team': 'Team'}, inplace=True)
        df.rename(columns={'document_id': 'Document No.\n/ เลขที่ใบคำขอ'}, inplace=True)
        df.rename(columns={'contract_officer': 'Contract Officer \n/ เจ้าหน้าที่ทำสัญญา'}, inplace=True)
        df.rename(columns={'job_id': 'JOB No.'}, inplace=True)
        df.rename(columns={'name': 'Customer Name \n/ ชื่อลูกค้า'}, inplace=True)
        df.rename(columns={'car_license': 'Licence Plate No. \n/ หมายเลขป้ายทะเบียน'}, inplace=True)
        df.rename(columns={'car_province': 'Province \n/ จังหวัดป้ายทะเบียน'}, inplace=True)
        df.rename(columns={'case_status': 'Case Status \n/ สถานะเคส'}, inplace=True)
        df.rename(columns={'status': 'Case Status \n/ สถานะเคส'}, inplace=True)
        df.rename(columns={'receive_date': '[ 1 ]\nReceive Date \n/ วันที่รับเคส\n( 1 days )'}, inplace=True)
        df.rename(columns={'contact_customer_date': '[ 2 ]\nContact Customer Date \n/ วันที่ติดต่อลูกค้า\n( {} )'.format(orange_red['contact_customer'])}, inplace=True)
        df.rename(columns={'account_closing_date': '[ 3 ]\nAccount Closing Date \n/ วันที่ปิดเล่ม\n( {} )'.format(orange_red['account_closing'])}, inplace=True)
        df.rename(columns={'transfer_doc_received_date': '[ 4 ]\nTransfer Doc. Received \n/ วันรับชุดโอน\n( {} )'.format(orange_red['transfer_doc_received'])}, inplace=True)
        df.rename(columns={'transfer_doc_submitted_date': '[ 5 ]\nTransfer Doc. Submitted \n/ วันยื่นชุดโอน\n( {} )'.format(orange_red['transfer_doc_submitted'])}, inplace=True)
        df.rename(columns={'book_received_date': '[ 6 ]\nBook Received Date \n/ วันที่ได้รับเล่ม\n( {} )'.format(orange_red['book_received'])}, inplace=True)
        df.rename(columns={'submit_book_transfer_date': '[ 7 ]\nSubmit Book Transfer Date \n/ วันที่ส่งงานโอนทะเบียน\n( {} )'.format(orange_red['submit_book_transfer'])}, inplace=True)
        df.rename(columns={'car_check_up_date': '[ 8 ]\nCar Check-Up \n/ วันตรวจสภาพรถ\n( {} )'.format(orange_red['car_check_up'])}, inplace=True)
        df.rename(columns={'book_transfer_date': '[ 9 ]\nBook Transfer Date \n/ โอนเล่มทะเบียน\n( {} )'.format(orange_red['book_transfer'])}, inplace=True)
        df.rename(columns={'book_copy_received_date': '[ 10 ]\nBook Copy Received \n/ รับสำเนาเล่ม\n( {} )'.format(orange_red['book_copy_received'])}, inplace=True)
        df.rename(columns={'deposit_doc_to_new_bank_date': '[ 11 ]\nส่งเอกสารเบิกเงินธนาคารใหม่\n( {} )'.format(orange_red['deposit_doc_to_new_bank'])}, inplace=True)
        df.rename(columns={'submit_book_deposit_return_date': '[ 15 ]\nSubmit Book Deposit Return \n/ ทำเรื่องเบิกมัดจำคืน\n( {} )'.format(orange_red['submit_book_deposit_return'])}, inplace=True)
        df.rename(columns={'book_received_back_date': '[ 13 ]\nBook Received \n/ รับเล่มคืน\n( {} )'.format(orange_red['book_received_back'])}, inplace=True)
        df.rename(columns={'cash_received_date': '[ 12 ]\nCash Received \n/ เงินเข้าบัญชีคาร์ทรัส\n( {} )'.format(orange_red['cash_received'])}, inplace=True)
        df.rename(columns={'book_deposit_received_date': '[ 16 ]\nBook Deposit Received \n/ เงินมัดจำคืนเข้าบัญชี\n( {} )'.format(orange_red['book_deposit_received'])}, inplace=True)
        df.rename(columns={'submit_book_to_new_finance_date': '[ 14 ]\nSubmit Book to New Finance \n/ ส่งเล่มให้ไฟแนนซ์ใหม่\n( {} )'.format(orange_red['submit_book_to_new_finance'])}, inplace=True)
        df.rename(columns={'old_bank': 'ไฟแนนซ์เดิม'}, inplace=True)
        df.rename(columns={'finance_staff': 'InputStaff'}, inplace=True)
        df.rename(columns={'approve_amount': 'Approved Amount \n/ ยอดจัด'}, inplace=True)
        df.rename(columns={'note_status': 'Update Note'}, inplace=True)
        df.rename(columns={'case_type': 'Case Type \n/ ประเภทเคส'}, inplace=True)
        df.rename(columns={'case_receiver': 'Case Receiver \n/ ผู้ลงข้อมูล'}, inplace=True)
        df.rename(columns={'case_source': 'Case Source / รับเคสจาก'}, inplace=True)
        df.rename(columns={'car_brand': 'Brand / ยี่ห้อ'}, inplace=True)
        df.rename(columns={'car_model': 'Model / รุ่นรถ'}, inplace=True)
        df.rename(columns={'date_create': 'Date Create'}, inplace=True)
        df.rename(columns={'date_update': 'Date Update'}, inplace=True)

        strIO = io.BytesIO()
        excel_writer = pd.ExcelWriter(strIO, engine="openpyxl")
        df.to_excel(excel_writer, sheet_name=condition)
        df2.to_excel(excel_writer, sheet_name='Average')
        ws = excel_writer.sheets[condition]
        ws2 = excel_writer.sheets['Average']

# manage_excel 2--------------------------------------------------------------------------------------------------------

        ws2.delete_cols(1)
        ws2.delete_rows(1)
        ws2.insert_cols(1, 1)
        ws2.insert_rows(1, 1)

        thin_border = Border(left=Side(style='thin'),
                             right=Side(style='thin'),
                             top=Side(style='thin'),
                             bottom=Side(style='thin'))

        for i in range(2, dict_summary['len_data_1'] + 1):
            ws2['B{}'.format(str(i))].fill = PatternFill(start_color='C6BF98', end_color='C6BF98', fill_type='solid')
            ws2['C{}'.format(str(i))].fill = PatternFill(start_color='EEECE1', end_color='EEECE1', fill_type='solid')
            ws2['D{}'.format(str(i))].fill = PatternFill(start_color='F6DAD9', end_color='F6DAD9', fill_type='solid')
            ws2['E{}'.format(str(i))].fill = PatternFill(start_color='FFFFCC', end_color='FFFFCC', fill_type='solid')
            ws2['F{}'.format(str(i))].fill = PatternFill(start_color='DAE7B8', end_color='DAE7B8', fill_type='solid')
            ws2['G{}'.format(str(i))].fill = PatternFill(start_color='DCE6F1', end_color='DCE6F1', fill_type='solid')
            ws2['H{}'.format(str(i))].fill = PatternFill(start_color='DAEEF3', end_color='DAEEF3', fill_type='solid')
            ws2['I{}'.format(str(i))].fill = PatternFill(start_color='B7DEE8', end_color='B7DEE8', fill_type='solid')
            if i != 2 :
                ws2['G{}'.format(str(i))].number_format = '#,##0.00'
                ws2['H{}'.format(str(i))].number_format = '#,##0.00'
                ws2['I{}'.format(str(i))].number_format = '#,##0.00'

        for i in dict_summary['list_col_fill']:
            for j in "BCDEFGH":
                ws2['{}{}'.format(j, str(i))].fill = PatternFill(start_color='DEDAC8', end_color='DEDAC8',
                                                                 fill_type='solid')
                ws2['{}{}'.format(j, str(i))].border = thin_border
                ws2['{}2'.format(j)].border = thin_border

        for i in range(2, dict_summary['len'] + 1):
            for j in "BCDEFGHI":
                ws2['{}{}'.format(j, str(i))].font = Font(name='Calibri', size=10)
                ws2['{}{}'.format(j, str(i))].alignment = Alignment(horizontal='center')

        for j in "BCDEFGHI":
            ws2.column_dimensions[j].width = 12

        ws2['I2'].border = thin_border

######################################################################################################################## color_fill

        blueFill = PatternFill(start_color='CCFFFF', end_color='CCFFFF', fill_type='solid')
        blue2Fill = PatternFill(start_color='33CCCC', end_color='33CCCC', fill_type='solid')
        greenFill = PatternFill(start_color='CCFFCC', end_color='CCFFCC', fill_type='solid')
        green2Fill = PatternFill(start_color='00FF00', end_color='00FF00', fill_type='solid')
        green3Fill = PatternFill(start_color='339966', end_color='339966', fill_type='solid')
        yellowFill = PatternFill(start_color='FFFF00', end_color='FFFF00', fill_type='solid')
        redFill = PatternFill(start_color='FFFF0000', end_color='FFFF0000', fill_type='solid')
        orangeFill = PatternFill(start_color='FFFFCC00', end_color='FFFFCC00', fill_type='solid')
        brownFill = PatternFill(start_color='993300', end_color='993300', fill_type='solid')
        greyFill = PatternFill(start_color='969696', end_color='969696', fill_type='solid')

        cgreyFill = PatternFill(start_color='575D62', end_color='575D62', fill_type='solid')
        cblueFill = PatternFill(start_color='A7E8FA', end_color='A7E8FA', fill_type='solid')
        cblue2Fill = PatternFill(start_color='58B8FC', end_color='58B8FC', fill_type='solid')
        profill = PatternFill(start_color='99CCFF', end_color='99CCFF', fill_type='solid')
        cancelfill = PatternFill(start_color='F99C95', end_color='F99C95', fill_type='solid')
        comfill = PatternFill(start_color='CCFFCC', end_color='CCFFCC', fill_type='solid')
        allfill = PatternFill(start_color='FFFF99', end_color='FFFF99', fill_type='solid')
        sumfill = PatternFill(start_color='F9CB3C', end_color='F9CB3C', fill_type='solid')

######################################################################################################################## sheet_1

# column_fill ----------------------------------------------------------------------------------------------------------

        for i in "ABCDEFGHI":
            ws['{}'.format(i + "1")].fill = blueFill
        ws['J1'].fill = greenFill
        ws['K1'].fill = greenFill
        for i in 'LMNOPQRSTUVWSYXZ':
            ws['{}'.format(i + "1")].fill = yellowFill


        ws['AA1'].fill = yellowFill
        ws['AB1'].fill = orangeFill
        ws['AC1'].fill = orangeFill
        ws['AD1'].fill = green2Fill
        ws['AE1'].fill = brownFill
        ws['AF1'].fill = green3Fill
        ws['AG1'].fill = green3Fill
        ws['AH1'].fill = green3Fill
        ws['AI1'].fill = green3Fill
        ws['AJ1'].fill = green3Fill
        ws['AK1'].fill = blue2Fill
        ws['AL1'].fill = blue2Fill

        data_colunm = {"J": "receive",
                       "K": "contact_customer",
                       "L": "account_closing",
                       "M": "transfer_doc_received",
                       "N": "transfer_doc_submitted",
                       "O": "book_received",
                       "P": "submit_book_transfer",
                       "Q": "car_check_up",
                       "R": "book_transfer",
                       "S": "book_copy_received",
                       "T": "deposit_doc_to_new_bank",
                       "U": "submit_book_deposit_return",
                       "V": "book_received_back",
                       "W": "cash_received",
                       "X": "book_deposit_received",
                       "Y": "submit_book_to_new_finance"}

        data_colunm_2 = {"1": "receive",
                       "2": "contact_customer",
                       "3": "account_closing",
                       "4": "transfer_doc_received",
                       "5": "transfer_doc_submitted",
                       "6": "book_received",
                       "7": "submit_book_transfer",
                       "8": "car_check_up",
                       "9": "book_transfer",
                       "10": "book_copy_received",
                       "11": "deposit_doc_to_new_bank",
                       "12": "submit_book_deposit_return",
                       "13": "book_received_back",
                       "14": "cash_received",
                       "15": "book_deposit_received",
                       "16": "submit_book_to_new_finance"}


# set_bold_font --------------------------------------------------------------------------------------------------------

        row = len(data2) + 1
        column = len(data2[0])

        all_column = allColumn(row,column)

        for i in all_column:
            ws['{}'.format(i+"1")].font = Font(name='Calibri', size=10, bold=True)

        for i in range(1, row + 1):
            cell = "K" + str(i)
            cell2 = "J" + str(i)
            case_status_i = ws[cell].value
            process_i = ws[cell2].value

            if process_i == "process":
                ws[cell].fill = yellowFill
            elif process_i == "cancel":
                ws[cell].fill = greyFill

            if case_status_i == "submit_book_to_new_finance":
                ws[cell] = "[16] {}".format(process_i)
            elif case_status_i == "book_deposit_received":
                ws[cell] = "[15] {}".format(process_i)
            elif case_status_i == "cash_received":
                ws[cell] = "[14] {}".format(process_i)
            elif case_status_i == "book_received_back":
                ws[cell] = "[13] {}".format(process_i)
            elif case_status_i == "submit_book_deposit_return":
                ws[cell] = "[12] {}".format(process_i)
            elif case_status_i == "deposit_doc_to_new_bank":
                ws[cell] = "[11] {}".format(process_i)
            elif case_status_i == "book_copy_received":
                ws[cell] = "[10] {}".format(process_i)
            elif case_status_i == "book_transfer":
                ws[cell] = "[9] {}".format(process_i)
            elif case_status_i == "car_check_up":
                ws[cell] = "[8] {}".format(process_i)
            elif case_status_i == "submit_book_transfer":
                ws[cell] = "[7] {}".format(process_i)
            elif case_status_i == "book_received":
                ws[cell] = "[6] {}".format(process_i)
            elif case_status_i == "transfer_doc_submitted":
                ws[cell] = "[5] {}".format(process_i)
            elif case_status_i == "transfer_doc_received":
                ws[cell] = "[4] {}".format(process_i)
            elif case_status_i == "account_closing":
                ws[cell] = "[3] {}".format(process_i)
            elif case_status_i == "contact_customer":
                ws[cell] = "[2] {}".format(process_i)
            elif case_status_i == "receive":
                ws[cell] = "[1] {}".format(process_i)


        ws.delete_cols(10)
        ws.insert_cols(27, 3)

        def compareDate(date_1, date_2):
            if date_1 == "" or date_2 == "":
                return 0
            else:
                date1 = datetime.strptime(date_1, "%d/%m/%Y")
                date2 = datetime.strptime(date_2, "%d/%m/%Y")
                return (date1 - date2).days


# set_width_height -----------------------------------------------------------------------------------------------------

        ws.delete_cols(1)

# KPI ------------------------------------------------------------------------------------------------------------------

        for i_row in range(2, len_data + 2):
            ws['Z{}'.format(i_row)] = compareDate(ws['U{}'.format(i_row)].value, ws['L{}'.format(i_row)].value)
            ws['Z{}'.format(i_row)].font = Font(name='Calibri', size=10)
            ws['AA{}'.format(i_row)] = compareDate(ws['U{}'.format(i_row)].value, ws['J{}'.format(i_row)].value)
            ws['AA{}'.format(i_row)].font = Font(name='Calibri', size=10)
            ws['AB{}'.format(i_row)] = compareDate(ws['W{}'.format(i_row)].value, ws['J{}'.format(i_row)].value)
            ws['AB{}'.format(i_row)].font = Font(name='Calibri', size=10)

            ws['AE{}'.format(i_row)].alignment = Alignment(horizontal='right', vertical='center')

        ws['Z1'] = "KPI U-L"
        ws['AA1'] = "KPI U-J"
        ws['AB1'] = "KPI W-J"
        ws['Z1'].fill = PatternFill(start_color='F18560', end_color='F18560', fill_type='solid')
        ws['AA1'].fill = PatternFill(start_color='F18560', end_color='F18560', fill_type='solid')
        ws['AB1'].fill = PatternFill(start_color='F18560', end_color='F18560', fill_type='solid')
        ws['Z1'].font = Font(name='Calibri', size=10, bold=True)
        ws['AA1'].font = Font(name='Calibri', size=10, bold=True)
        ws['AB1'].font = Font(name='Calibri', size=10, bold=True)
        ws.column_dimensions['Z'].width = 9
        ws.column_dimensions['AA'].width = 9
        ws.column_dimensions['AB'].width = 9
        ws['Z1'].alignment = Alignment(horizontal='center', vertical='center')
        ws['AA1'].alignment = Alignment(horizontal='center', vertical='center')
        ws['AB1'].alignment = Alignment(horizontal='center', vertical='center')

        for column_3 in 'ABCDEFGHIJKLMNOPQRSTUVWXYXZ':
            ws['{}1'.format(column_3)].alignment = Alignment(horizontal='center', vertical='center', wrapText=True)
        for column_5 in 'ABCDEFGHIJKLMN':
            ws['A{}1'.format(column_5)].alignment = Alignment(horizontal='center', vertical='center', wrapText=True)

        ws.row_dimensions[1].height = 87

        for i in range(2, len(data) + 2):
            ws.row_dimensions[i].height = 21

        ws.column_dimensions['A'].width = 8
        ws.column_dimensions['B'].width = 9
        ws.column_dimensions['C'].width = 12
        ws.column_dimensions['D'].width = 18
        ws.column_dimensions['E'].width = 12
        ws.column_dimensions['F'].width = 20
        ws.column_dimensions['G'].width = 20
        ws.column_dimensions['H'].width = 20
        ws.column_dimensions['I'].width = 12

        for i in 'JKLMNOPQRSTUVWSY':
            ws.column_dimensions[i].width = 15

        ws.column_dimensions['Z'].width = 9
        ws.column_dimensions['AA'].width = 9
        ws.column_dimensions['AB'].width = 9
        ws.column_dimensions['AC'].width = 12
        ws.column_dimensions['AD'].width = 10
        ws.column_dimensions['AE'].width = 15
        ws.column_dimensions['AF'].width = 82
        ws.column_dimensions['AG'].width = 15
        ws.column_dimensions['AH'].width = 15
        ws.column_dimensions['AI'].width = 15
        ws.column_dimensions['AJ'].width = 15
        ws.column_dimensions['AK'].width = 15
        ws.column_dimensions['AL'].width = 15
        ws.column_dimensions['AM'].width = 15

        ws['I1'].fill = PatternFill(start_color='58D68D', end_color='58D68D', fill_type='solid')

        for i in range(2,len_data+2) :
            for j in ['A','B','C','E','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC',
                      'AD','AE','AG','AH','AI','AJ','AK','AL','AM']:
                ws['{}{}'.format(j,i)].alignment = Alignment(horizontal='center')

        for i_row_2 in range(2, len_data + 2):
            for i_col_2 in ['Z','AA','AB']:
                cell_2 = ws['{}{}'.format(i_col_2,i_row_2)]
                if cell_2.value != "" and cell_2.value != None:
                    day_2 = int(cell_2.value)
                    if day_2 >= 45:
                        cell_2.fill = redFill
                    elif day_2 >= 30:
                        cell_2.fill = orangeFill
                    elif day_2 >= 21:
                        cell_2.fill = yellowFill

# blank_to_grey_and_font -----------------------------------------------------------------------------------------------

        row = len(data2) + 1
        column = len(data2[0])

        all_cell = allCell(row, column)
        for cell in all_cell:
            ws['{}'.format(cell)].font = Font(name='Calibri', size=10)
            if ws['{}'.format(cell)].value == "":
                ws[cell].fill = greyFill

# condition_date -------------------------------------------------------------------------------------------------------

        redFill = PatternFill(start_color='FFFF0000', end_color='FFFF0000', fill_type='solid')
        orangeFill = PatternFill(start_color='FFFFCC00', end_color='FFFFCC00', fill_type='solid')

        dict_colunm = {1: "receive",
                       2: "contact_customer",
                       3: "account_closing",
                       5: "transfer_doc_received",
                       4: "transfer_doc_submitted",
                       6: "book_received",
                       7: "submit_book_transfer",
                       8: "car_check_up",
                       9: "book_transfer",
                       10: "book_copy_received",
                       11: "deposit_doc_to_new_bank",
                       12: "submit_book_deposit_return",
                       13: "book_received_back",
                       14: "cash_received",
                       15: "book_deposit_received",
                       16: "submit_book_to_new_finance"}


        for i in red_fill[2]:
            ws['K{}'.format(i + 1)].fill = redFill
            ws['K{}'.format(i + 1)].comment = Comment(note[i-1][dict_colunm[2]], "Author")
        for i in orange_fill[2]:
            ws['K{}'.format(i + 1)].fill = orangeFill
            ws['K{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[2]], "Author")

        for i in red_fill[3]:
            ws['L{}'.format(i + 1)].fill = redFill
            ws['L{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[3]], "Author")
        for i in orange_fill[3]:
            ws['L{}'.format(i + 1)].fill = orangeFill
            ws['L{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[3]], "Author")

        for i in red_fill[4]:
            ws['N{}'.format(i + 1)].fill = redFill
            ws['N{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[4]], "Author")
        for i in orange_fill[4]:
            ws['N{}'.format(i + 1)].fill = orangeFill
            ws['N{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[4]], "Author")

        for i in red_fill[5]:
            ws['M{}'.format(i + 1)].fill = redFill
            ws['M{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[5]], "Author")
        for i in orange_fill[5]:
            ws['M{}'.format(i + 1)].fill = orangeFill
            ws['M{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[5]], "Author")

        for i in red_fill[6]:
            ws['O{}'.format(i + 1)].fill = redFill
            ws['O{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[6]], "Author")
        for i in orange_fill[6]:
            ws['O{}'.format(i + 1)].fill = orangeFill
            ws['O{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[6]], "Author")

        for i in red_fill[7]:
            ws['P{}'.format(i + 1)].fill = redFill
            ws['P{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[7]], "Author")
        for i in orange_fill[7]:
            ws['P{}'.format(i + 1)].fill = orangeFill
            ws['P{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[7]], "Author")

        for i in red_fill[8]:
            ws['Q{}'.format(i + 1)].fill = redFill
            ws['Q{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[8]], "Author")
        for i in orange_fill[5]:
            ws['Q{}'.format(i + 1)].fill = orangeFill
            ws['Q{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[8]], "Author")

        for i in red_fill[9]:
            ws['R{}'.format(i + 1)].fill = redFill
            ws['R{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[9]], "Author")
        for i in orange_fill[9]:
            ws['R{}'.format(i + 1)].fill = orangeFill
            ws['R{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[9]], "Author")

        for i in red_fill[10]:
            ws['S{}'.format(i + 1)].fill = redFill
            ws['S{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[10]], "Author")
        for i in orange_fill[10]:
            ws['S{}'.format(i + 1)].fill = orangeFill
            ws['S{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[10]], "Author")

        for i in red_fill[11]:
            ws['T{}'.format(i + 1)].fill = redFill
            ws['T{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[11]], "Author")
        for i in orange_fill[11]:
            ws['T{}'.format(i + 1)].fill = orangeFill
            ws['T{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[11]], "Author")

        for i in red_fill[12]:
            ws['X{}'.format(i + 1)].fill = redFill
            ws['X{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[12]], "Author")
        for i in orange_fill[12]:
            ws['X{}'.format(i + 1)].fill = orangeFill
            ws['X{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[12]], "Author")

        for i in red_fill[13]:
            ws['V{}'.format(i + 1)].fill = redFill
            ws['V{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[13]], "Author")
        for i in orange_fill[13]:
            ws['V{}'.format(i + 1)].fill = orangeFill
            ws['V{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[13]], "Author")

        for i in red_fill[14]:
            ws['U{}'.format(i + 1)].fill = redFill
            ws['U{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[14]], "Author")
        for i in orange_fill[14]:
            ws['U{}'.format(i + 1)].fill = orangeFill
            ws['U{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[14]], "Author")

        for i in red_fill[15]:
            ws['Y{}'.format(i + 1)].fill = redFill
            ws['Y{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[15]], "Author")
        for i in orange_fill[15]:
            ws['Y{}'.format(i + 1)].fill = orangeFill
            ws['Y{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[15]], "Author")

        for i in red_fill[16]:
            ws['W{}'.format(i + 1)].fill = redFill
            ws['W{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[16]], "Author")
        for i in orange_fill[16]:
            ws['W{}'.format(i + 1)].fill = orangeFill
            ws['W{}'.format(i + 1)].comment = Comment(note[i - 1][dict_colunm[16]], "Author")

#average

        b = []
        e = []
        for i in range(2, len_data + 2):
            month_year = str(ws['J{}'.format(i)].value).split("/")
            if len(month_year) == 3:
                if month_year[1:3] not in e:
                    e.append(month_year[1:3])
                    b.append([int(month_year[2]), int(month_year[1]), i])

        b = sorted(b)

        for i in b:
            i.insert(0, i[1])
            i.pop(2)

        c = b

        for i in range(len(c)):
            if i != len(c) - 1:
                c[i].append(c[i + 1][2] - 1)
            else:
                c[i].append(len_data + 2)

        for i in range(len(c)):
            ws2['G{}'.format(i + 3)] = "=AVERAGE('{}'!AA{}:'{}'!AA{})".format(condition, c[i][2], condition, c[i][3])
            ws2['H{}'.format(i + 3)] = "=AVERAGE('{}'!Z{}:'{}'!Z{})".format(condition, c[i][2], condition, c[i][3])
            ws2['I{}'.format(i + 3)] = "=AVERAGE('{}'!AB{}:'{}'!AB{})".format(condition, c[i][2], condition, c[i][3])

        ws.column_dimensions['X'].hidden = True
        ws.column_dimensions['Y'].hidden = True
        ws.column_dimensions['B'].hidden = True


        for i in range(1,len(data2)+2):
            ws['AM{}'.format(i)].font = Font(name='Calibri', size=10)


# save_and_send --------------------------------------------------------------------------------------------------------

        excel_writer.save()
        excel_data = strIO.getvalue()
        strIO.seek(0)
        now = datetime.now().strftime("%d-%m-%Y")
        return send_file(strIO, attachment_filename="case {} {}.xlsx".format(condition,now), as_attachment=True)

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR