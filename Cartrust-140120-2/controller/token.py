import base64
from datetime import time
import time
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP,PKCS1_v1_5
from Crypto import Random
import json
from flask import *
from functools import wraps
from werkzeug.contrib.cache import SimpleCache

cache = SimpleCache()

ACCESS_EXPIRE_TIME = 60

def generate_RSA(bits=2048):
    new_key = RSA.generate(bits, e=65537)
    public_key = new_key.publickey().exportKey("PEM")
    private_key = new_key.exportKey("PEM")
    return private_key, public_key

private_key, public_key = generate_RSA()

def encryption_message(message) :
    key = RSA.importKey(public_key)
    cipher = PKCS1_v1_5.new(key)
    encrypted_message = cipher.encrypt(message.encode('ascii'))
    return base64.b64encode(encrypted_message)

def generate_access_token(username,password):
    issue = time.time()
    access_expire = str(issue + (ACCESS_EXPIRE_TIME * 60))
    access_token_claims = username + password + str(issue)
    access_token = encryption_message(access_token_claims)
    return access_token, access_expire

# def decryption_message(encrypted_message):
#     encrypted_message = base64.b64encode(encrypted_message)
#     print("base64")
#     private_key_object = RSA.importKey(private_key)
#     print("private key")
#     decrypted_message = private_key_object.decrypt(encrypted_message)
#     print("decrypt")
#     result = {}
#     try:
#         result = json.loads(decrypted_message)
#         return result
#     except:
#         return result


def checkToken(f):
    @wraps(f)
    def decorated_function(*args , **kwargs):
        access_token = request.headers.get('Authorization')
        if access_token is not None:
            cache_access_token = cache.get(request.headers.get('Authorization'))
            if cache_access_token is not None:
                return f(*args, **kwargs)
            return jsonify({"message" : "token expires"})
        return jsonify({"message" : "token please"})
    return decorated_function

