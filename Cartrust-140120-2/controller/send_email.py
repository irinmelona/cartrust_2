import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from controller.all_function import *

import yaml

with open(r'configs/configs.yaml',"r") as data:
    data_configs = yaml.load(data, Loader=yaml.FullLoader)

def setHTML(to_email,password):
    html = """<!DOCTYPE html>
                <html>
                    <body>
                        <p><b>เรียน ผู้ใช้งานระบบ CARTRUST</b></p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;แจ้งให้ทราบเรื่องการเปลี่ยนรหัสผ่านเข้าใช้งานของท่าน</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;รหัสผ่านใหม่ : <u><b>{}</b></u></p>
                        <p style="color: red;"><b>!! โปรดลบจดหมายนี้ เพื่อป้องกันการเข้าถึงข้อมูลของท่าน </b></p>
                    </body>
                </html>""".format(password)
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "[ CARTRUST ] - แจ้งเปลี่ยนรหัสผ่าน"
    msg['From'] = data_configs["email_system"]["address"]
    msg['To'] = to_email
    htmlPart = MIMEText(html, 'html', 'UTF-8')
    msg.attach(htmlPart)
    return msg

def sendMail(to_email,password) :
    mail_address = {"yahoo": 'smtp.mail.yahoo.com',
                    "hotmail": 'smtp-mail.outlook.com',
                    "outlook": 'smtp-mail.outlook.com',
                    "gmail": 'smtp.gmail.com'}
    host_address = ""

    from_email = data_configs["email_system"]["address"]

    for key,val in mail_address.items():
        if from_email.find(key):
            host_address = val
    if host_address != "":
        s = smtplib.SMTP(host=host_address, port=587)
        s.starttls()
        s.login(data_configs["email_system"]["address"], data_configs["email_system"]["password"])
        s.sendmail(from_email, to_email, setHTML(to_email,password).as_string())
        s.close()
        return "success"
    return 'Please use Gmail,Outlook,Hotmail,Yahoo'

def setHTML2(to_email,username,password):
    html = """<!DOCTYPE html>
                <html>
                    <body>
                        <p><b>เรียน ผู้ใช้งานระบบ CARTRUST</b></p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;แจ้งให้ทราบเรื่องรหัสผ่านเข้าใช้งานของท่าน</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Username : <u><b>{}</b></u></p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Password : <u><b>{}</b></u></p>
                        <p style="color: red;"><b>!! โปรดลบจดหมายนี้ เพื่อป้องกันการเข้าถึงข้อมูลของท่าน </b></p>
                    </body>
                </html>""".format(username,password)
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "[ CARTRUST ] - แจ้งรหัสผ่านการเข้าใช้งาน"
    msg['From'] = data_configs["email_system"]["address"]
    msg['To'] = to_email
    htmlPart = MIMEText(html, 'html', 'UTF-8')
    msg.attach(htmlPart)
    return msg

def sendMail2(to_email,username,password) :
    mail_address = {"yahoo": 'smtp.mail.yahoo.com',
                    "hotmail": 'smtp-mail.outlook.com',
                    "outlook": 'smtp-mail.outlook.com',
                    "gmail": 'smtp.gmail.com'}
    host_address = ""

    from_email = data_configs["email_system"]["address"]

    for key,val in mail_address.items():
        if from_email.find(key):
            host_address = val
    if host_address != "":
        s = smtplib.SMTP(host=host_address, port=587)
        s.starttls()
        s.login(data_configs["email_system"]["address"], data_configs["email_system"]["password"])
        s.sendmail(from_email, to_email, setHTML2(to_email,username,password).as_string())
        s.close()
        return "success"
    return 'Please use Gmail,Outlook,Hotmail,Yahoo'