# -*- coding: utf-8 -*-
import datetime
import sys

from flask import *
from flask_api import status
from controller.all_function import *

key_list = ["firstname", "lastname", "tel", "email", "line","birthday","license_id","address","province","post_code"]

sql_text = """SELECT    id,
                                        customer_id,
                                        firstname,
                                        lastname,
                                        tel,
                                        tel2,
                                        license_id,
                                        email,
                                        line,
                                        address,
                                        province,
                                        post_code,
                                        birthday,
                                        CAST(YEAR(CURDATE()) - YEAR(birthday) as INT) as age
                                        FROM customer"""

def addCustomer():
    try:
        data_json = request.json
        created = datetime.now()
        customer_id = createId("CTM","customer_id","customer")
        check = True
        while check == True:
            customer_id = createId("CTM","customer_id","customer")
            if checkData('customer_id', customer_id, 'customer') == True:
                check = False
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)

        if data_json['birthday'] == "":
            sql_customer = """INSERT INTO customer
                                        (customer_id,firstname,lastname,tel,tel2,license_id,email,line,date_create,date_update,address,province,post_code)
                                        VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}');""".format(
                customer_id,
                data_json['firstname'],
                data_json['lastname'],
                data_json['tel'],
                data_json['tel2'],
                data_json['license_id'],
                data_json['email'],
                data_json['line'],
                created,
                created,
                data_json['address'],
                data_json['province'],
                data_json['post_code']
            )
            mycursor.execute(sql_customer, )
            mydb.commit()

        else:
            birthday = data_json['birthday']

            sql_customer = """INSERT INTO customer
                                (customer_id,firstname,lastname,tel,tel2,license_id,email,line,date_create,date_update,birthday,address,province,post_code)
                                VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}');""".format(
                customer_id,
                data_json['firstname'],
                data_json['lastname'],
                data_json['tel'],
                data_json['tel2'],
                data_json['license_id'],
                data_json['email'],
                data_json['line'],
                created,
                created,
                birthday,
                data_json['address'],
                data_json['province'],
                data_json['post_code']
            )
            mycursor.execute(sql_customer, )
            mydb.commit()
        mydb.close()
        return jsonify({"message": "success_addCustomer", "customer_id" : customer_id, "status": 200})

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showCustomerId():
    try:
        customer_id = request.args['customer_id']
        if checkData("customer_id",customer_id,"customer") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_customer = """{} WHERE customer_id = '{}'""".format(sql_text,customer_id)
            mycursor.execute(sql_customer, )
            customer = mycursor.fetchone()
            mydb.close()
            return jsonify({"message": customer, "status": 200})
        else:
            return jsonify({"message": "customer id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showCustomerAll():
    try:
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_customer = "{} order by id DESC".format(sql_text)
        mycursor.execute(sql_customer, )
        customer = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": customer, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showCustomerLimit():
    try:
        page = request.args['page']
        size = request.args['size']
        start = (int(page) - 1) * int(size)
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_customer = "{}  order by id DESC LIMIT {},{}".format(sql_text,start,size)
        mycursor.execute(sql_customer, )
        customer = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": customer, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def editCustomer():
    try:
        data_json = request.json
        customer_id = request.args['customer_id']
        if checkData("customer_id",customer_id,"customer") == False:
            update = datetime.now()

            if checkDataEdit('license_id', 'customer', data_json['license_id'], 'customer_id', customer_id) == True:
                if checkDataEdit('email', 'customer', data_json['email'], 'customer_id', customer_id) == True:
                    if checkDataEdit('line', 'customer', data_json['line'],  'customer_id', customer_id) == True:
                        if checkDataEdit('tel', 'customer', data_json['tel'], 'customer_id', customer_id) == True:
                            if checkDataEdit('tel2', 'customer', data_json['tel2'], 'customer_id', customer_id) == True:
                                mydb = connectsql()
                                mycursor = mydb.cursor(dictionary=True)
                                sql_customer = """UPDATE customer
                                                SET firstname='{}', lastname='{}', tel ='{}',tel2='{}',license_id='{}', email='{}', line='{}',
                                                date_update='{}' , birthday='{}', address = '{}', province='{}',post_code='{}' WHERE customer_id = '{}';""".format(
                                    data_json['firstname'],
                                    data_json['lastname'],
                                    data_json['tel'],
                                    data_json['tel2'],
                                    data_json['license_id'],
                                    data_json['email'],
                                    data_json['line'],
                                    update,
                                    data_json['birthday'],
                                    data_json['address'],
                                    data_json['province'],
                                    data_json['post_code'],
                                    customer_id)
                                mycursor.execute(sql_customer, )
                                mydb.commit()
                                mydb.close()
                                return jsonify({"message": "success_editCustomer", "status": 200})
                            return jsonify({"message": "tel2 already", "status": 500})
                        return jsonify({"message": "tel already", "status": 500})
                    return jsonify({"message": "line already", "status": 500})
                return jsonify({"message": "email already", "status": 500})
            return jsonify({"message": "license id already", "status": 500})

        else:
            return jsonify({"message": "customer id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def deleteCustomer():
    try:
        customer_id = request.args['customer_id']
        if checkData("customer_id",customer_id,"customer") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_customer = "DELETE FROM customer WHERE customer_id = '{}';".format(customer_id)
            mycursor.execute(sql_customer, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_deleteCustomer", "status": 200})
        else:
            return jsonify({"message": "customer id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def searchCustomer():
    try:
        value = request.args['keyword']
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_customer = """{}
                        WHERE           
                                        customer_id LIKE '%{}%' or
                                        firstname LIKE '%{}%' or
                                        lastname LIKE '%{}%' or
                                        tel LIKE '%{}%' or
                                        email LIKE '%{}%' or
                                        line LIKE '%{}%' or
                                        address LIKE '%{}%' or
                                        province LIKE '%{}%' or
                                        post_code LIKE '%{}%' ;""".format(sql_text,value,value,value,value,value,value,value,value,value,value,value,value,value,value,value)
        mycursor.execute(sql_customer, )
        data = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": data, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR
