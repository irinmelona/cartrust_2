# -*- coding: utf-8 -*-
from flask import Blueprint
from model.customer import customer

vm_routes_customer = Blueprint('/controller_customer', __name__)

#----------------------------------------------------------------------------------------------------------------WEEK_1
# [1] เพิ่ม customer
vm_routes_customer.add_url_rule('add_customer', 'customer/add', customer.addCustomer, methods=['POST'],)
# [2] แสดง customer ระบุ ID
vm_routes_customer.add_url_rule('customer', 'customer/show', customer.showCustomerId, methods=['GET'],)
# [3] แสดง customer ทั้งหมเด
vm_routes_customer.add_url_rule('customer_all', 'customer/show_all', customer.showCustomerAll, methods=['GET'],)
# [4] แสดง customer ในแต่ละหน้า
vm_routes_customer.add_url_rule('customer_limit', 'customer/show_limit', customer.showCustomerLimit, methods=['GET'],)
# [5] แก้ไข customer
vm_routes_customer.add_url_rule('edit_customer', 'customer/edit', customer.editCustomer, methods=['PUT'],)
# [6] ลบ customer
vm_routes_customer.add_url_rule('delete_customer', 'customer/delete', customer.deleteCustomer, methods=['DELETE'],)

#----------------------------------------------------------------------------------------------------------------WEEK_2
# [7] ค้นหา customer
vm_routes_customer.add_url_rule('search_customer', 'customer/search', customer.searchCustomer, methods=['GET'],)