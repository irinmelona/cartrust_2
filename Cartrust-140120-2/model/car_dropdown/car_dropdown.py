import datetime
import sys

from flask import *
from flask_api import status
from controller.all_function import *

# def addFinanceStaff():
#     try:
#         data_json = request.json
#         created = datetime.now()
#
#         fs_id = createId("FS","fs_id","finance_staff")
#         check = True
#         while check == True:
#             fs_id = createId("FS","fs_id","finance_staff")
#             if checkData('fs_id', fs_id, 'finance_staff') == True:
#                 check = False
#
#         if checkData('fs_name', data_json['name'], 'finance_staff') == True:
#             if checkData('tel', data_json['tel'], 'finance_staff') == True:
#                 if checkData('line', data_json['line'], 'finance_staff') == True:
#                     mydb = connectsql()
#                     mycursor = mydb.cursor(dictionary=True)
#                     sql = """INSERT INTO finance_staff
#                                         (fs_id,fs_name,tel,line,date_create,date_update)
#                                         VALUES ('{}','{}','{}','{}','{}','{}');""".format(
#                         fs_id,
#                         data_json['name'],
#                         data_json['tel'],
#                         data_json['line'],
#                         created,
#                         created)
#                     mycursor.execute(sql, )
#                     mydb.commit()
#                     mydb.close()
#                     return jsonify({"message": "success_addDropdown" , "id" : fs_id, "status": 200})
#                 else:
#                     return jsonify({"message": "line already", "status": 500})
#             else:
#                 return jsonify({"message": "tel already", "status": 500})
#         else:
#             return jsonify({"message": "fs name already", "status": 500})
#
#     except Exception as e:
#         exc_type, exc_obj, exc_tb = sys.exc_info()
#         fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
#         print(exc_type, fname, exc_tb.tb_lineno,e)
#         return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showBrand():
    try:
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql = "SELECT brand from car_price_database group by brand"
        mycursor.execute(sql, )
        data = mycursor.fetchall()
        mydb.close()
        brand = []
        for i in range(len(data)):
            brand.append(data[i]['brand'])
        return jsonify({"data" : brand})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showModel():
    try:
        brand = request.json['brand']
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql = "SELECT model from car_price_database WHERE brand = '{}' group by model".format(brand)
        mycursor.execute(sql, )
        data = mycursor.fetchall()
        mydb.close()
        model = []
        for i in range(len(data)):
            model.append(data[i]['model'])
        return jsonify({"data" : model})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showYear():
    try:
        brand = request.json['brand']
        model = request.json['model']
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql = "SELECT year from car_price_database WHERE brand = '{}' and model = '{}' group by year".format(brand,model)
        mycursor.execute(sql, )
        data = mycursor.fetchall()
        mydb.close()
        year = []
        for i in range(len(data)):
            year.append(data[i]['year'])
        return jsonify({"data" : year})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showSubModel():
    try:
        brand = request.json['brand']
        model = request.json['model']
        year = request.json['year']
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql = "SELECT sub_model from car_price_database WHERE brand = '{}' and model = '{}' and year = '{}' group by sub_model".format(brand,model,year)
        mycursor.execute(sql, )
        data = mycursor.fetchall()
        mydb.close()
        sub_model = []
        for i in range(len(data)):
            sub_model.append(data[i]['sub_model'])
        return jsonify({"data" : sub_model})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def addCarPrice():
    try:
        data_json = request.json

        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql = """INSERT INTO car_price_database
                            (brand,model,year,sub_model,kkusedprice)
                            VALUES ('{}','{}','{}','{}','{}');""".format(
            data_json['brand'],
            data_json['model'],
            data_json['year'],
            data_json['sub_model'],
            data_json['kkusedprice'])
        mycursor.execute(sql, )
        mydb.commit()
        mydb.close()
        return jsonify({"message": "success_addDropdownCar" ,"status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR