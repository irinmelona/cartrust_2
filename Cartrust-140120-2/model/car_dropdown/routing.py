# -*- coding: utf-8 -*-
from flask import Blueprint
from model.car_dropdown import car_dropdown

vm_routes_cardd = Blueprint('/controller_cardd', __name__)

#----------------------------------------------------------------------------------------------------------------WEEK_1

vm_routes_cardd.add_url_rule('brand', 'cardd/brand', car_dropdown.showBrand, methods=['GET'],)

vm_routes_cardd.add_url_rule('model', 'cardd/model', car_dropdown.showModel, methods=['POST'],)

vm_routes_cardd.add_url_rule('year', 'cardd/year', car_dropdown.showYear, methods=['POST'],)

vm_routes_cardd.add_url_rule('sub_model', 'cardd/sub_model', car_dropdown.showSubModel, methods=['POST'],)

vm_routes_cardd.add_url_rule('car_dropdown', 'cardd/add', car_dropdown.addCarPrice, methods=['POST'],)