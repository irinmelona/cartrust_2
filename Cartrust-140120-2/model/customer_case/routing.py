# -*- coding: utf-8 -*-
from flask import Blueprint
from model.customer_case import customer_case

vm_routes_case = Blueprint('/controller_case', __name__)

#----------------------------------------------------------------------------------------------------------------WEEK_2

# [1] แสดง case ระบุ ID
vm_routes_case.add_url_rule('case', 'case/show', customer_case.showCaseId, methods=['GET'],)
# [2] แสดง case ทั้งหมเด
vm_routes_case.add_url_rule('case_all', 'case/show_all', customer_case.showCaseAll, methods=['GET'],)
# [3] แสดง case ในแต่ละหน้า
vm_routes_case.add_url_rule('case_limit', 'case/show_limit', customer_case.showCaseLimit, methods=['GET'],)


# [4] เพิ่ม case [ CASE 1 ]
vm_routes_case.add_url_rule('add_case', 'case/add', customer_case.addCase, methods=['POST'],)

# [5] แก้ไข case [ Other CASE ]
vm_routes_case.add_url_rule('tracking_case', 'case/tracking', customer_case.editOtherCase, methods=['PUT'],)
# [6] แก้ไข case [ CASE 3 ]
vm_routes_case.add_url_rule('tracking_case3', 'case/tracking3', customer_case.editCase3, methods=['PUT'],)

# [6] แก้ไข case [ CASE 11 ]
vm_routes_case.add_url_rule('tracking_case11', 'case/tracking11', customer_case.editCase11, methods=['PUT'],)
# [7] แก้ไข case [ CASE 15 ]
vm_routes_case.add_url_rule('tracking_case15', 'case/tracking15', customer_case.editCase15, methods=['PUT'],)

# [8] ลบ case
vm_routes_case.add_url_rule('delete_case', 'case/delete', customer_case.deleteCase, methods=['DELETE'],)

# [9] ค้นหา case
vm_routes_case.add_url_rule('search_case', 'case/search', customer_case.searchCase, methods=['GET'],)

# [10] F2
vm_routes_case.add_url_rule('F2', 'case/calculate', customer_case.addF2, methods=['POST'],)

# [11] fast tracking
vm_routes_case.add_url_rule('fast_tracking', 'case/fastTrack', customer_case.fastTracking, methods=['POST'],)

vm_routes_case.add_url_rule('fast_tracking2', 'case/fastTrack2', customer_case.fastTracking2, methods=['POST'],)

# [12] save note
vm_routes_case.add_url_rule('note', 'case/saveNote', customer_case.saveNote, methods=['POST'],)

# [13] condition date
vm_routes_case.add_url_rule('date', 'case/date', customer_case.showConditionDate, methods=['GET'],)

# [12] edit date
vm_routes_case.add_url_rule('edit_date', 'case/editdate', customer_case.editDate, methods=['POST'],)

# [12] add picture
vm_routes_case.add_url_rule('picture', 'case/picture', customer_case.addPicture, methods=['POST'],)

# [12] add picture
vm_routes_case.add_url_rule('status_date', 'case/statusDate', customer_case.editDateStatus, methods=['POST'],)

vm_routes_case.add_url_rule('cancel', 'case/cancel', customer_case.cancelCase, methods=['PUT'],)

vm_routes_case.add_url_rule('edit_contract', 'contract/edit', customer_case.editContract, methods=['PUT'],)

vm_routes_case.add_url_rule('date_tracking', 'date/tracking', customer_case.editDateTracking, methods=['PUT'],)

vm_routes_case.add_url_rule('case_1_5', 'case/1_15', customer_case.showCase1_5, methods=['GET'],)
vm_routes_case.add_url_rule('case_6_9', 'case/6_9', customer_case.showCase6_9, methods=['GET'],)
vm_routes_case.add_url_rule('case_10_16', 'case/10_16', customer_case.showCase10_16, methods=['GET'],)
vm_routes_case.add_url_rule('cartrust_dealer', 'case/cartrust_dealer', customer_case.showCaseCartrustDealer, methods=['GET'],)
vm_routes_case.add_url_rule('case_dealer', 'case/dealer', customer_case.showCaseDealer, methods=['GET'],)
vm_routes_case.add_url_rule('case_cartrust', 'case/cartrust', customer_case.showCaseCartrust, methods=['GET'],)

vm_routes_case.add_url_rule('case_1_3', 'case/1_3', customer_case.showCase1_3, methods=['GET'],)
vm_routes_case.add_url_rule('case_4_14', 'case/4_14', customer_case.showCase4_14, methods=['GET'],)
vm_routes_case.add_url_rule('case_15_16', 'case/15_16', customer_case.showCase15_16, methods=['GET'],)





