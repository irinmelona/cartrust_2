# -*- coding: utf-8 -*-
# import datetime
import sys
from datetime import timedelta

from flask import *
from flask_api import status

from controller.all_function import *

list_tk_dep = [
    "account_closing",
    "book_received",
    "submit_book_transfer",
    "car_check_up",
    "book_transfer",
    "book_copy_received",
    "deposit_doc_to_new_bank",
    "cash_received",
    "book_received_back",
    "submit_book_to_new_finance",
    "submit_book_deposit_return",
    "book_deposit_received",
    "complete"
]
list_tk_nodep = [
    "account_closing",
    "transfer_doc_submitted",
    "book_received",
    "submit_book_transfer",
    "car_check_up",
    "book_transfer",
    "book_copy_received",
    "deposit_doc_to_new_bank",
    "cash_received",
    "book_received_back",
    "submit_book_to_new_finance",
    "complete"
]
list_cd_dep = [
    "account_closing",
    "transfer_doc_submitted",
    "book_received",
    "deposit_doc_to_new_bank",
    "cash_received",
    "submit_book_deposit_return",
    "book_deposit_received",
    "complete"
]
list_cd_nodep = [
    "account_closing",
    "transfer_doc_submitted",
    "book_received",
    "submit_book_transfer",
    "car_check_up",
    "book_transfer",
    "book_received_back",
    "deposit_doc_to_new_bank",
    "cash_received",
    "complete"
]
next_text = {
    "contact_customer": "รอติดต่อลูกค้า",
    "account_closing": "รอปิดเล่ม",
    "transfer_doc_submitted": "รอยื่นชุดโอน",
    "book_received": "รอรับเล่ม",
    "submit_book_transfer": "รอส่งงานโอนทะเบียน",
    "car_check_up": "รอตรวจสภาพรถ",
    "book_transfer": "รอโอนเล่มทะเบียน",
    "book_copy_received": "รอรับสำเนาเล่ม",
    "deposit_doc_to_new_bank": "รอส่งเอกสารเบิกเงินธนาคารใหม่",
    "submit_book_deposit_return": "รอทำเรื่องเบิกมัดจำคืน",
    "book_received_back": "รอรับเล่มคืน",
    "cash_received": "รอเงินเข้าบัญชีคาร์ทรัส",
    "book_deposit_received": "รอเงินมัดจำคืนเข้าบัญชี",
    "submit_book_to_new_finance": "รอส่งเล่มให้ไฟแนนซ์ใหม่",
    "complete": "สำเร็จ"
}


def calTotalDate(data):
    now = datetime.now()
    if type(data) == list:
        for case in data:
            list_date = []
            for key,val in case.items():
                if key.endswith("_date") == True and type(val) == datetime:
                    list_date.append(val)
            list_total = []
            for date in list_date:
                list_total.append((now-date).days)
            if case['process'] == 'complete' or case['process'] == 'cancel':
                case['total_date'] = max(list_total) - min(list_total)
            else:
                case['total_date'] = max(list_total)
            print(list_date)
            print(list_total)
    else:
        list_date = []
        for key, val in data.items():
            if key.endswith("_date") == True and type(val) == datetime:
                list_date.append(val)
        list_total = []
        for date in list_date:
            list_total.append((now - date).days)
        if data['process'] == 'complete' or data['process'] == 'cancel':
            data['total_date'] = max(list_total) - min(list_total)
        else:
            data['total_date'] = max(list_total)
        print(list_date)
        print(list_total)
    return data
def addNextStatus(data):
    if type(data) == list:
        for i in range(len(data)):
            if data[i]['status'] == 'receive':
                data[i]['next_status'] = 'contact_customer'
                data[i]['next_status_text'] = next_text[data[i]['next_status']]
            elif data[i]['status'] == 'contact_customer':
                data[i]['next_status'] = 'account_closing'
                data[i]['next_status_text'] = next_text[data[i]['next_status']]
            else:
                if data[i]['f2_deposit_12'] != 0 and (
                        data[i]['case_source'] == "Kiatnakin" or data[i]['case_source'] == "Thanachart"):
                    data[i]['next_status'] = list_tk_dep[list_tk_dep.index(data[i]['status']) + 1]
                    data[i]['next_status_text'] = next_text[data[i]['next_status']]
                elif data[i]['f2_deposit_12'] == 0 and (
                        data[i]['case_source'] == "Kiatnakin" or data[i]['case_source'] == "Thanachart"):
                    data[i]['next_status'] = list_tk_nodep[list_tk_nodep.index(data[i]['status']) + 1]
                    data[i]['next_status_text'] = next_text[data[i]['next_status']]
                elif data[i]['f2_deposit_12'] != 0 and (
                        data[i]['case_source'] == "Cartrust" or data[i]['case_source'] == "Dealer"):
                    data[i]['next_status'] = list_cd_dep[list_cd_dep.index(data[i]['status']) + 1]
                    data[i]['next_status_text'] = next_text[data[i]['next_status']]
                elif data[i]['f2_deposit_12'] == 0 and (
                        data[i]['case_source'] == "Cartrust" or data[i]['case_source'] == "Dealer"):
                    data[i]['next_status'] = list_cd_nodep[list_cd_nodep.index(data[i]['status']) + 1]
                    data[i]['next_status_text'] = next_text[data[i]['next_status']]
                else:
                    data[i]['next_status'] = ""
                    data[i]['next_status_text'] = ""
    else:
        if data['status'] == 'receive':
            data['next_status'] = 'contact_customer'
            data['next_status_text'] = next_text[data['next_status']]
        elif data['status'] == 'contact_customer':
            data['next_status'] = 'account_closing'
            data['next_status_text'] = next_text[data['next_status']]
        else:
            if data['f2_deposit_12'] != 0 and (
                    data['case_source'] == "Kiatnakin" or data['case_source'] == "Thanachart"):
                data['next_status'] = list_tk_dep[list_tk_dep.index(data['status']) + 1]
                data['next_status_text'] = next_text[data['next_status']]
            elif data['f2_deposit_12'] == 0 and (
                    data['case_source'] == "Kiatnakin" or data['case_source'] == "Thanachart"):
                data['next_status'] = list_tk_nodep[list_tk_nodep.index(data['status']) + 1]
                data['next_status_text'] = next_text[data['next_status']]
            elif data['f2_deposit_12'] != 0 and (data['case_source'] == "Cartrust" or data['case_source'] == "Dealer"):
                data['next_status'] = list_cd_dep[list_cd_dep.index(data['status']) + 1]
                data['next_status_text'] = next_text[data['next_status']]
            elif data['f2_deposit_12'] == 0 and (data['case_source'] == "Cartrust" or data['case_source'] == "Dealer"):
                data['next_status'] = list_cd_nodep[list_cd_nodep.index(data['status']) + 1]
                data['next_status_text'] = next_text[data['next_status']]
            else:
                data['next_status'] = ""
                data['next_status_text'] = ""
    return data
def setDataBlankAll(data_all):
    for i in range(len(data_all)):
        for key, val in data_all[i].items():
            if val == None and (key == "close_amount" or key == "down_amount" or "key" == "approve_amount"
                                or key == "cheque" or key == "f2_deposit" or key == "f2_deposit_12" or key.endswith(
                        "_fee")):
                data_all[i][key] = 0
            elif val == None:
                data_all[i][key] = ""
    return data_all
def setDataBlank(data):
    for key, val in data.items():
        if val == None and (key == "close_amount" or key == "down_amount" or "key" == "approve_amount"
                            or key == "cheque" or key == "f2_deposit" or key == "f2_deposit_12" or key.endswith(
                    "_fee")):
            data[key] = 0
        elif val == None:
            data[key] = ""
    return data

sql_text = sql_case = """SELECT 
             car.id as id,
             car.car_id as car_id,
             car.car_brand as car_brand,
             car.car_model as car_model,
             car.car_name as car_carname,
             car.car_license as car_license,
             car.car_province as car_province,
             car.car_year as car_year,
             car.car_sub_model as car_sub_model,
             car.detail as car_detail,
             car.date_create as car_date_create,
             car.date_update as car_date_update,
             
             customercase.vendor AS vendor,
             customercase.customer_id as customer_id,
             customercase.process as process,
             customercase.case_id as case_id,
             customercase.user_id as user_id,
             customercase.document_id as document_id,
             customercase.job_id as job_id,
             customercase.old_bank as old_bank,
             customercase.new_bank as new_bank,
             customercase.contact_user_id as contact_user_id,
             customercase.status as status,
             customercase.case_status as case_status,
             customercase.note_status as note_status,
             customercase.old_finance as old_finance,
             customercase.team as team,
             customercase.contract_officer as contract_officer,
             CONCAT (user.firstname," ",user.lastname) as finance_staff_name,
             customercase.case_type as case_type,
             customercase.case_receiver as case_receiver,
             customercase.case_source as case_source,
             customercase.take_car_picture as take_car_picture,
             customercase.car_license_book_picture as car_license_book_picture,
             customercase.license_id_picture as license_id_picture,
             customercase.date_create as date_create,
             customercase.date_update as date_update,
             customercase.hub as hub,
             customercase.cqc_team as cqc_team,
             customercase.cartrust_lead_refer as cartrust_lead_refer,
             
             customercase.approve_amount as approve_amount,
             customercase.close_amount as close_amount,
             customercase.down_amount as down_amount,

             customercase.F2_status as F2_status,
             customercase.old_finance_closing_fee as f2_old_finance_closing_fee,
             customercase.old_finance_transfer_fee as f2_old_finance_transfer_fee,
             customercase.book_closing_fee as f2_book_closing_fee,
             customercase.vat7_fee as f2_vat7_fee,
             customercase.transfer_fee as f2_transfer_fee,
             customercase.duty_fee as f2_duty_fee,
             customercase.car_shield_fee as f2_car_shield_fee,
             customercase.car_insurance_fee as f2_car_insurance_fee,
             customercase.transfer_service_fee as f2_transfer_service_fee,
             customercase.contract_fee as f2_contract_fee,
             customercase.outside_transfer_fee as f2_outside_transfer_fee,
             customercase.deposit as f2_deposit,
             customercase.deposit_12 as f2_deposit_12,
             customercase.cheque as f2_cheque,
             customercase.cheque_receiver as f2_cheque_receiver,
             customercase.deposit_receiver as f2_deposit_receiver,
             customercase.cartrust_total_cost as f2_cartrust_total_cost,
             customercase.new_finance_total_cost as f2_new_finance_total_cost,
             customercase.total_cost as f2_amount_received,
             customercase.amount_received as f2_amount_received,
             customercase.old_finance_total_cost as f2_old_finance_total_cost,
             customercase.car_check_con as f2_car_check_con,
             customercase.doc_storage_con as f2_doc_storage_con,
             customercase.margin_account as f2_margin_account,
             customercase.margin_account_no as f2_margin_account_no,
             
             customercase.old_finance_closing_fee_note as f2_old_finance_closing_fee_note,
             customercase.old_finance_transfer_fee_note as f2_old_finance_transfer_fee_note,
             customercase.book_closing_fee_note as f2_book_closing_fee_note,
             customercase.vat7_fee_note as f2_vat7_fee_note,
             customercase.transfer_fee_note as f2_transfer_fee_note,
             customercase.duty_fee_note as f2_duty_fee_note,
             customercase.car_shield_fee_note as f2_car_shield_fee_note,
             customercase.car_insurance_fee_note as f2_car_insurance_fee_note,
             customercase.transfer_service_fee_note as f2_transfer_service_fee_note,
             customercase.contract_fee_note as f2_contract_fee_note,
             customercase.outside_transfer_fee_note as f2_outside_transfer_fee_note,
             
             customercase.cartrust_other_fee as f2_cartrust_other_fee,
             customercase.cartrust_other_fee_note as f2_cartrust_other_fee_note,
             customercase.newfinance_other_fee as f2_newfinance_other_fee,
             customercase.newfinance_other_fee_note as f2_newfinance_other_fee_note,
             
             customercase.difference as f2_difference,
             customercase.difference_fee as f2_difference_fee,
             customercase.province as f2_province,
             customercase.dealer as dealer,
             
             tracking.receive_date as receive_date,
             tracking.receive_note as receive_note,
             tracking.receive_id as receive_id,
             tracking.contact_customer_date as contact_customer_date,
             tracking.contact_customer_note as contact_customer_note,
             tracking.contact_customer_id as contact_customer_id,
             tracking.account_closing_date as account_closing_date,
             tracking.account_closing_note as account_closing_note,
             tracking.account_closing_id as account_closing_id,
             tracking.transfer_doc_received_date as transfer_doc_received_date,
             tracking.transfer_doc_received_note as transfer_doc_received_note,
             tracking.transfer_doc_received_id as transfer_doc_received_id,
             tracking.transfer_doc_submitted_date as transfer_doc_submitted_date,
             tracking.transfer_doc_submitted_note as transfer_doc_submitted_note,
             tracking.transfer_doc_submitted_id as transfer_doc_submitted_id,
             tracking.book_received_date as book_received_date,
             tracking.book_received_note as book_received_note,
             tracking.book_received_id as book_received_id,
             tracking.submit_book_transfer_date as submit_book_transfer_date,
             tracking.submit_book_transfer_note as submit_book_transfer_note,
             tracking.submit_book_transfer_id as submit_book_transfer_id,
             tracking.car_check_up_date as car_check_up_date,
             tracking.car_check_up_note as car_check_up_note,
             tracking.car_check_up_id as car_check_up_id,
             tracking.book_transfer_date as book_transfer_date,
             tracking.book_transfer_note as book_transfer_note,
             tracking.book_transfer_id as book_transfer_id,
             tracking.book_copy_received_date as book_copy_received_date,
             tracking.book_copy_received_note as book_copy_received_note,
             tracking.book_copy_received_id as book_copy_received_id,
             tracking.deposit_doc_to_new_bank_date as deposit_doc_to_new_bank_date,
             tracking.deposit_doc_to_new_bank_note as deposit_doc_to_new_bank_note,
             tracking.deposit_doc_to_new_bank_id as deposit_doc_to_new_bank_id,
             tracking.submit_book_deposit_return_date as submit_book_deposit_return_date,
             tracking.submit_book_deposit_return_note as submit_book_deposit_return_note,
             tracking.submit_book_deposit_return_id as submit_book_deposit_return_id,
             tracking.book_received_back_date as book_received_back_date,
             tracking.book_received_back_note as book_received_back_note,
             tracking.book_received_back_id as book_received_back_id,
             tracking.cash_received_date as cash_received_date,
             tracking.cash_received_note as cash_received_note,
             tracking.cash_received_id as cash_received_id,
             tracking.book_deposit_received_date as book_deposit_received_date,
             tracking.book_deposit_received_note as book_deposit_received_note,
             tracking.book_deposit_received_id as book_deposit_received_id,
             tracking.submit_book_to_new_finance_date as submit_book_to_new_finance_date,
             tracking.submit_book_to_new_finance_note as submit_book_to_new_finance_note,
             tracking.submit_book_to_new_finance_id as submit_book_to_new_finance_id,
             
             tracking.contact_customer_yn as contact_customer_yn,
             tracking.account_closing_yn as account_closing_yn,
             tracking.transfer_doc_received_yn as transfer_doc_received_yn,
             tracking.transfer_doc_submitted_yn as transfer_doc_submitted_yn,
             tracking.book_received_yn as book_received_yn,
             tracking.submit_book_transfer_yn as submit_book_transfer_yn,
             tracking.car_check_up_yn as car_check_up_yn,
             tracking.book_transfer_yn as book_transfer_yn,
             tracking.book_copy_received_yn as book_copy_received_yn,
             tracking.deposit_doc_to_new_bank_yn as deposit_doc_to_new_bank_yn,
             tracking.submit_book_deposit_return_yn as submit_book_deposit_return_yn,
             tracking.book_received_back_yn as book_received_back_yn,
             tracking.cash_received_yn as cash_received_yn,
             tracking.book_deposit_received_yn as book_deposit_received_yn,
             tracking.submit_book_to_new_finance_yn as submit_book_to_new_finance_yn,
             
             tracking.receive_id as receive_nickname,
             tracking.contact_customer_id as contact_customer_nickname,
             tracking.account_closing_id as account_closing_nickname,
             tracking.transfer_doc_received_id as transfer_doc_received_nickname,
             tracking.transfer_doc_submitted_id as transfer_doc_submitted_nickname,
             tracking.book_received_id as book_received_nickname,
             tracking.submit_book_transfer_id as submit_book_transfer_nickname,
             tracking.car_check_up_id as car_check_up_nickname,
             tracking.book_transfer_id as book_transfer_nickname,
             tracking.book_copy_received_id as book_copy_received_nickname,
             tracking.deposit_doc_to_new_bank_id as deposit_doc_to_new_bank_nickname,
             tracking.submit_book_deposit_return_id as submit_book_deposit_return_nickname,
             tracking.book_received_back_id as book_received_back_nickname,
             tracking.cash_received_id as cash_received_nickname,
             tracking.book_deposit_received_id as book_deposit_received_nickname,
             tracking.submit_book_to_new_finance_id as submit_book_to_new_finance_nickname,
             
             customercase.finance_staff as finance_staff,
             CONCAT (customer.firstname," ",customer.lastname) as name,
             customer.tel as cus_tel,
             customer.tel2 as cus_tel2,
             customer.license_id as cus_license_id,
             customer.email as cus_email,
             customer.line as cus_line,
             CAST(YEAR(CURDATE()) - YEAR(customer.birthday) as INT) as cus_age,
             customer.address as cus_address,
             customer.birthday as cus_birthday,
             customer.province as cus_province,
             customer.post_code as cus_post_code,
             
             user.tel as finance_staff_tel,
             user.line as finance_staff_line,
             contract_officer.tel as contract_officer_tel,
             contract_officer.line as contract_officer_line,
             cartrust_lead.cl_id4 as cartrust_lead_refer_id,
             cartrust_lead.tel as cartrust_lead_refer_tel,
             cartrust_lead.line as cartrust_lead_refer_line,
             dealer.tel as dealer_tel,
             dealer.line as dealer_line
             
                     FROM customercase 
                 LEFT JOIN tracking ON customercase.case_id = tracking.case_id 
                 LEFT JOIN car ON customercase.car_id = car.car_id 
                 LEFT JOIN customer ON customercase.customer_id = customer.customer_id 
                 
                 LEFT JOIN user ON customercase.finance_staff = user.user_id 
                 LEFT JOIN contract_officer ON customercase.contract_officer = contract_officer.co_name 
                 LEFT JOIN cartrust_lead ON customercase.cartrust_lead_refer = cartrust_lead.cl_name 
                 LEFT JOIN dealer ON customercase.dealer = dealer.d_name """

def editContract():
    try:
        data_json = request.json
        update = datetime.now()
        case_id = request.args['case_id']
        customer_id = data_json['customer_id']
        car_id = data_json['car_id']

        if checkData("case_id", case_id, "customercase") == False:
            if checkData("customer_id", customer_id, "customer") == False:
                if checkData("car_id", car_id, "car") == False:
                    mydb = connectsql()
                    mycursor = mydb.cursor(dictionary=True)
                    sql_tracking = """UPDATE tracking SET receive_date = '{}' WHERE case_id = '{}';""".format(
                        data_json['receive_date'],
                        case_id)
                    mycursor.execute(sql_tracking, )
                    mydb.commit()
                    sql_case = """UPDATE customercase SET       date_update = '{}',
                                                                case_type = '{}',
                                                                old_bank = '{}',
                                                                new_bank = '{}',
                                                                approve_amount = '{}',
                                                                close_amount = '{}',
                                                                down_amount = '{}',
                                                                old_finance_total_cost = '{}',
                                                                total_cost = '{}',
                                                                amount_received = '{}' WHERE case_id = '{}';""".format(
                        update,
                        data_json['case_type'],
                        data_json['old_bank'],
                        data_json['new_bank'],
                        data_json['approve_amount'],
                        data_json['close_amount'],
                        data_json['down_amount'],
                        data_json['old_finance_total_cost'],
                        data_json['total_cost'],
                        data_json['amount_received'],
                        case_id)
                    mycursor.execute(sql_case, )
                    mydb.commit()
                    sql_customer = """UPDATE customer SET date_update = '{}',firstname = '{}', lastname = '{}', tel = '{}', tel2 = '{}' WHERE customer_id = '{}';""".format(
                        update,
                        data_json['firstname'],
                        data_json['lastname'],
                        data_json['tel'],
                        data_json['tel2'],
                        customer_id)
                    mycursor.execute(sql_customer, )
                    mydb.commit()
                    sql_tracking = """UPDATE car SET    date_update = '{}',
                                                        car_license = '{}',
                                                        car_province = '{}',
                                                        car_brand = '{}',
                                                        car_model = '{}',
                                                        car_sub_model = '{}',
                                                        car_year = '{}' WHERE car_id = '{}';""".format(
                        update,
                        data_json['car_license'],
                        data_json['car_province'],
                        data_json['car_brand'],
                        data_json['car_model'],
                        data_json['car_sub_model'],
                        data_json['car_year'],
                        car_id)
                    mycursor.execute(sql_tracking, )
                    mydb.commit()
                    mydb.close()
                    return jsonify({"message": "success edit_contract", "status": 200})
                else:
                    return jsonify({"message": "car id not found", "status": 500})
            else:
                return jsonify({"message": "customer id not found", "status": 500})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
# def addF2Function(case_id, data, update):
#     try:
#         data_json = data
#         mydb = connectsql()
#         mycursor = mydb.cursor(dictionary=True)
#         f2_status = data_json['f2_status']
#         data_cal = calculateCost(data)
#         sql_case = """UPDATE customercase SET                       province = '{}',
#                                                                     finance_staff = '{}',
#                                                                     old_finance_closing_fee_note = '{}',
#                                                                     old_finance_transfer_fee_note = '{}',
#                                                                     book_closing_fee_note = '{}',
#                                                                     vat7_fee_note = '{}',
#                                                                     transfer_fee_note = '{}',
#                                                                     duty_fee_note = '{}',
#                                                                     discount_fee_note = '{}',
#                                                                     car_shield_fee_note = '{}',
#                                                                     car_insurance_fee_note = '{}',
#                                                                     transfer_service_fee_note = '{}',
#                                                                     contract_fee_note = '{}',
#                                                                     outside_transfer_fee_note = '{}',
#                                                                     tax_renewal_fee_note = '{}',
#                                                                     act_renewal_fee_note = '{}',
#                                                                     car_check_con = '{}',
#                                                                     doc_storage_con = '{}',
#                                                                     margin_account = '{}',
#                                                                     margin_account_no = '{}',
#                                                                     deposit = '{}',
#                                                                     cheque_receiver = '{}',
#                                                                     deposit_receiver = '{}',
#                                                                     cheque = '{}',
#                                                                     F2_status = '{}',
#                                                                     approve_amount='{}',
#                                                                     old_finance_closing_fee = '{}',
#                                                                     old_finance_transfer_fee = '{}',
#                                                                     book_closing_fee = '{}',
#                                                                     vat7_fee = '{}',
#                                                                     transfer_fee = '{}',
#                                                                     duty_fee = '{}',
#                                                                     discount_fee = '{}',
#
#                                                                     car_shield_fee = '{}',
#                                                                     car_insurance_fee = '{}',
#                                                                     transfer_service_fee = '{}',
#                                                                     contract_fee = '{}',
#                                                                     outside_transfer_fee = '{}',
#                                                                     tax_renewal_fee = '{}',
#                                                                     act_renewal_fee = '{}',
#
#                                                                     cartrust_total_cost = '{}',
#                                                                     new_finance_total_cost = '{}',
#                                                                     total_cost = '{}',
#                                                                     amount_received = '{}',
#                                                                     old_finance_total_cost = '{}',
#                                                                     date_update = '{}'
#
#                                                                     WHERE case_id = '{}';""".format(
#             data_json['province'],
#             data_json['finance_staff'],
#             data_json['old_finance_closing_fee_note'],
#             data_json['old_finance_transfer_fee_note'],
#             data_json['book_closing_fee_note'],
#             data_json['vat7_fee_note'],
#             data_json['transfer_fee_note'],
#             data_json['duty_fee_note'],
#             data_json['discount_fee_note'],
#             data_json['car_shield_fee_note'],
#             data_json['car_insurance_fee_note'],
#             data_json['transfer_service_fee_note'],
#             data_json['contract_fee_note'],
#             data_json['outside_transfer_fee_note'],
#             data_json['tax_renewal_fee_note'],
#             data_json['act_renewal_fee_note'],
#             data_json['car_check_con'],
#             data_json['doc_storage_con'],
#             data_json['margin_account'],
#             data_json['margin_account_no'],
#             data_json['deposit'],
#             data_json['cheque_receiver'],
#             data_json['deposit_receiver'],
#             data_json['cheque'],
#             f2_status,
#             data_json['approve_amount'],
#             data_json['old_finance_closing_fee'],
#             data_json['old_finance_transfer_fee'],
#             data_json['book_closing_fee'],
#             data_json['vat7_fee'],
#             data_json['transfer_fee'],
#             data_json['duty_fee'],
#             data_json['discount_fee'],
#             data_json['car_shield_fee'],
#             data_json['car_insurance_fee'],
#             data_json['transfer_service_fee'],
#             data_json['contract_fee'],
#             data_json['outside_transfer_fee'],
#             data_json['tax_renewal_fee'],
#             data_json['act_renewal_fee'],
#             data_cal['cartrust_total_cost'],
#             data_cal['new_finance_total_cost'],
#             data_cal['total_cost'],
#             data_cal['amount_received'],
#             data_cal['old_finance_total_cost'],
#             update,
#             case_id)
#         mycursor.execute(sql_case, )
#         mydb.commit()
#         mydb.close()
#     except Exception as e:
#         print(e)
def addF2():
    case_id = request.args['case_id']
    update = datetime.now()
    data_json = request.json
    try:
        if checkData("case_id", case_id, "customercase") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            f2_status = data_json['f2_status']
            data_cal = calculateCost(data_json)
            sql_case = """UPDATE customercase SET                               difference = '{}',
                                                                                difference_fee = '{}',
                                                                                cartrust_other_fee = '{}',
                                                                                cartrust_other_fee_note = '{}',
                                                                                newfinance_other_fee = '{}',
                                                                                newfinance_other_fee_note = '{}',
                                                                                province = '{}',
                                                                                finance_staff = '{}',
                                                                                old_finance_closing_fee_note = '{}',
                                                                                old_finance_transfer_fee_note = '{}',
                                                                                book_closing_fee_note = '{}',
                                                                                vat7_fee_note = '{}',
                                                                                transfer_fee_note = '{}',
                                                                                duty_fee_note = '{}',
                                                                                car_shield_fee_note = '{}',
                                                                                car_insurance_fee_note = '{}',
                                                                                transfer_service_fee_note = '{}',
                                                                                contract_fee_note = '{}',
                                                                                outside_transfer_fee_note = '{}',
                                                                                car_check_con = '{}',
                                                                                doc_storage_con = '{}',
                                                                                margin_account = '{}',
                                                                                margin_account_no = '{}',
                                                                                deposit = '{}',
                                                                                cheque_receiver = '{}',
                                                                                deposit_receiver = '{}',
                                                                                cheque = '{}',
                                                                                F2_status = '{}',
                                                                                approve_amount = '{}',
                                                                                close_amount = '{}',
                                                                                down_amount = '{}',
                                                                                old_finance_closing_fee = '{}',
                                                                                old_finance_transfer_fee = '{}',
                                                                                book_closing_fee = '{}',
                                                                                vat7_fee = '{}',
                                                                                transfer_fee = '{}',
                                                                                duty_fee = '{}',

                                                                                car_shield_fee = '{}',
                                                                                car_insurance_fee = '{}',
                                                                                transfer_service_fee = '{}',
                                                                                contract_fee = '{}',
                                                                                outside_transfer_fee = '{}',

                                                                                cartrust_total_cost = '{}',
                                                                                new_finance_total_cost = '{}',
                                                                                total_cost = '{}',
                                                                                amount_received = '{}',
                                                                                old_finance_total_cost = '{}',
                                                                                date_update = '{}'

                                                                                WHERE case_id = '{}';""".format(
                data_json['difference'],
                data_json['difference_fee'],
                data_json['cartrust_other_fee'],
                data_json['cartrust_other_fee_note'],
                data_json['newfinance_other_fee'],
                data_json['newfinance_other_fee_note'],
                data_json['province'],
                data_json['finance_staff'],
                data_json['old_finance_closing_fee_note'],
                data_json['old_finance_transfer_fee_note'],
                data_json['book_closing_fee_note'],
                data_json['vat7_fee_note'],
                data_json['transfer_fee_note'],
                data_json['duty_fee_note'],
                data_json['car_shield_fee_note'],
                data_json['car_insurance_fee_note'],
                data_json['transfer_service_fee_note'],
                data_json['contract_fee_note'],
                data_json['outside_transfer_fee_note'],
                data_json['car_check_con'],
                data_json['doc_storage_con'],
                data_json['margin_account'],
                data_json['margin_account_no'],
                data_json['deposit'],
                data_json['cheque_receiver'],
                data_json['deposit_receiver'],
                data_json['cheque'],
                f2_status,
                data_json['approve_amount'],
                data_json['close_amount'],
                data_json['down_amount'],
                data_json['old_finance_closing_fee'],
                data_json['old_finance_transfer_fee'],
                data_json['book_closing_fee'],
                data_json['vat7_fee'],
                data_json['transfer_fee'],
                data_json['duty_fee'],
                data_json['car_shield_fee'],
                data_json['car_insurance_fee'],
                data_json['transfer_service_fee'],
                data_json['contract_fee'],
                data_json['outside_transfer_fee'],
                data_cal['cartrust_total_cost'],
                data_cal['new_finance_total_cost'],
                data_cal['total_cost'],
                data_cal['amount_received'],
                data_cal['old_finance_total_cost'],
                update,
                case_id)
            mycursor.execute(sql_case, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_addF2", "status": 200})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def addCase():
    try:
        data_json = request.json
        created = datetime.now()

        case_id = createId("C", "case_id", "customercase")
        check = True
        while check == True:
            case_id = createId("C", "case_id", "customercase")
            if checkData('case_id', case_id, 'customercase') == True:
                check = False

        if checkData('customer_id', data_json['customer_id'], 'customer') == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)

            car_id = createId("CAR", "car_id", "car")
            check = True
            while check == True:
                car_id = createId("CAR", "car_id", "car")
                if checkData('car_id', car_id, 'car') == True:
                    check = False

            sql_car = """INSERT INTO car (car_id,car_brand,car_model,car_name,car_year,car_license,car_province,detail,
                        date_create,date_update,car_sub_model)
                        VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}');""".format(
                car_id,
                data_json['car_brand'],
                data_json['car_model'],
                data_json['car_name'],
                data_json['car_year'],
                data_json['car_license'],
                data_json['car_province'],
                data_json['car_detail'],
                data_json['receive_date'],
                data_json['receive_date'],
                data_json['car_sub_model'])
            mycursor.execute(sql_car, )
            mydb.commit()
            string_column, string_value = caseStatusToYesNo(data_json['case_status'])

            if data_json['case_status'] == "ติดต่อลูกค้าไม่ได้":
                process = "process"
                data_status = "receive"
            elif data_json['case_status'] == "ลูกค้ายกเลิก":
                process = "cancel"
                data_status = "cancel"
            elif data_json['case_status'] == "Case Close / ปิด Job":
                process = "complete"
                data_status = "complete"
            else:
                data_status = string_column.split(",")[len(string_column.split(",")) - 1].replace("_yn", "")
                process = "process"

            sql_user = "SELECT tel FROM user WHERE user_id = '{}'".format(data_json['user_id'])
            mycursor.execute(sql_user, )
            user = mycursor.fetchone()
            if user == None:
                tel = ""
            else:
                tel = user['tel']
            sql_case = """INSERT INTO customercase
                                (case_id,user_id,customer_id,document_id,old_bank,new_bank,contact_user_id,
                                status,note_status,team,contract_officer,finance_staff,car_id,
                                case_type,case_receiver,case_source,date_create,date_update,
                                case_status,process,take_car_picture,car_license_book_picture,license_id_picture,
                                hub,cqc_team,cartrust_lead_refer,province)
                                VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}',
                                '{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}');""".format(
                case_id,
                data_json['user_id'],
                data_json['customer_id'],
                data_json['document_id'],
                data_json['old_bank'],
                data_json['new_bank'],
                tel,
                data_status,
                data_json['note_status'],
                data_json['team'],
                data_json['contract_officer'],
                data_json['finance_staff'],
                car_id,
                data_json['case_type'],
                data_json['case_receiver'],
                data_json['case_source'],
                data_json['receive_date'], data_json['receive_date'],
                data_json['case_status'],
                process,
                data_json['take_car_picture'],
                data_json['car_license_book_picture'],
                data_json['license_id_picture'],
                data_json['hub'],
                data_json['cqc_team'],
                data_json['cartrust_lead_refer'],
                data_json['province'])
            mycursor.execute(sql_case, )
            mydb.commit()

            sql_tracking = """INSERT INTO tracking
                                (case_id,receive_date,receive_note,receive_id{})
                                VALUES ('{}','{}','{}','{}'{});""".format(
                string_column,
                case_id,
                data_json['receive_date'],
                data_json['note_status'],
                data_json['user_id'],
                string_value)
            mycursor.execute(sql_tracking, )
            mydb.commit()

            f2_status = data_json['f2_status']
            # data_cal = calculateCost(data_json)

            approve_amount = int(data_json['approve_amount'])
            old_finance_closing_fee = int(data_json['old_finance_closing_fee'])
            old_finance_transfer_fee = int(data_json['old_finance_transfer_fee'])
            book_closing_fee = int(data_json['book_closing_fee'])
            vat7_fee = int(data_json['vat7_fee'])
            transfer_fee = int(data_json['transfer_fee'])
            duty_fee = int(data_json['duty_fee'])
            cartrust_other_fee = int(data_json['cartrust_other_fee'])
            car_shield_fee = int(data_json['car_shield_fee'])
            car_insurance_fee = int(data_json['car_insurance_fee'])
            transfer_service_fee = int(data_json['transfer_service_fee'])
            contract_fee = int(data_json['contract_fee'])
            outside_transfer_fee = int(data_json['outside_transfer_fee'])
            newfinance_other_fee = int(data_json['newfinance_other_fee'])

            cartrust_total_cost = old_finance_closing_fee + old_finance_transfer_fee + book_closing_fee + vat7_fee + transfer_fee + duty_fee + cartrust_other_fee
            new_finance_total_cost = car_shield_fee + car_insurance_fee + transfer_service_fee + contract_fee + outside_transfer_fee + newfinance_other_fee
            total_cost = cartrust_total_cost + new_finance_total_cost
            amount_received = approve_amount - total_cost
            old_finance_total_cost = old_finance_transfer_fee + old_finance_closing_fee
            data_cal = {"cartrust_total_cost": cartrust_total_cost,
                        "new_finance_total_cost": new_finance_total_cost,
                        "total_cost": total_cost,
                        "amount_received": amount_received,
                        "old_finance_total_cost": old_finance_total_cost}

            sql_case = """UPDATE customercase SET                               difference = '{}',
                                                                                difference_fee = '{}',
                                                                                cartrust_other_fee = '{}',
                                                                                cartrust_other_fee_note = '{}',
                                                                                newfinance_other_fee = '{}',
                                                                                newfinance_other_fee_note = '{}',
                                                                                old_finance_closing_fee_note = '{}',
                                                                                old_finance_transfer_fee_note = '{}',
                                                                                book_closing_fee_note = '{}',
                                                                                vat7_fee_note = '{}',
                                                                                transfer_fee_note = '{}',
                                                                                duty_fee_note = '{}',
                                                                                car_shield_fee_note = '{}',
                                                                                car_insurance_fee_note = '{}',
                                                                                transfer_service_fee_note = '{}',
                                                                                contract_fee_note = '{}',
                                                                                outside_transfer_fee_note = '{}',
                                                                                car_check_con = '{}',
                                                                                doc_storage_con = '{}',
                                                                                margin_account = '{}',
                                                                                margin_account_no = '{}',
                                                                                deposit = '{}',
                                                                                cheque_receiver = '{}',
                                                                                deposit_receiver = '{}',
                                                                                cheque = '{}',
                                                                                F2_status = '{}',
                                                                                approve_amount = '{}',
                                                                                close_amount = '{}',
                                                                                down_amount = '{}',
                                                                                old_finance_closing_fee = '{}',
                                                                                old_finance_transfer_fee = '{}',
                                                                                book_closing_fee = '{}',
                                                                                vat7_fee = '{}',
                                                                                transfer_fee = '{}',
                                                                                duty_fee = '{}',

                                                                                car_shield_fee = '{}',
                                                                                car_insurance_fee = '{}',
                                                                                transfer_service_fee = '{}',
                                                                                contract_fee = '{}',
                                                                                outside_transfer_fee = '{}',

                                                                                cartrust_total_cost = '{}',
                                                                                new_finance_total_cost = '{}',
                                                                                total_cost = '{}',
                                                                                amount_received = '{}',
                                                                                old_finance_total_cost = '{}',
                                                                                date_update = '{}'

                                                                                WHERE case_id = '{}';""".format(
                data_json['difference'],
                data_json['difference_fee'],
                data_json['cartrust_other_fee'],
                data_json['cartrust_other_fee_note'],
                data_json['newfinance_other_fee'],
                data_json['newfinance_other_fee_note'],
                data_json['old_finance_closing_fee_note'],
                data_json['old_finance_transfer_fee_note'],
                data_json['book_closing_fee_note'],
                data_json['vat7_fee_note'],
                data_json['transfer_fee_note'],
                data_json['duty_fee_note'],
                data_json['car_shield_fee_note'],
                data_json['car_insurance_fee_note'],
                data_json['transfer_service_fee_note'],
                data_json['contract_fee_note'],
                data_json['outside_transfer_fee_note'],
                data_json['car_check_con'],
                data_json['doc_storage_con'],
                data_json['margin_account'],
                data_json['margin_account_no'],
                data_json['deposit'],
                data_json['cheque_receiver'],
                data_json['deposit_receiver'],
                data_json['cheque'],
                f2_status,
                data_json['approve_amount'],
                data_json['close_amount'],
                data_json['down_amount'],
                data_json['old_finance_closing_fee'],
                data_json['old_finance_transfer_fee'],
                data_json['book_closing_fee'],
                data_json['vat7_fee'],
                data_json['transfer_fee'],
                data_json['duty_fee'],
                data_json['car_shield_fee'],
                data_json['car_insurance_fee'],
                data_json['transfer_service_fee'],
                data_json['contract_fee'],
                data_json['outside_transfer_fee'],
                data_cal['cartrust_total_cost'],
                data_cal['new_finance_total_cost'],
                data_cal['total_cost'],
                data_cal['amount_received'],
                data_cal['old_finance_total_cost'],
                data_json['receive_date'],
                case_id)
            mycursor.execute(sql_case, )
            mydb.commit()

            mydb.close()

            return jsonify({"message": "success_addCase", "case_id": case_id, "status": 200})
        else:
            return jsonify({"message": "customer id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def editDateTracking():
    try:
        data_json = request.json
        update = datetime.now()
        case_id = request.args['case_id']

        if checkData("case_id", case_id, "customercase") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_tracking = """UPDATE tracking SET {}_date = '{}',  {}_id = '{}' WHERE case_id = '{}';""".format(
                data_json['tracking'],
                data_json['date'],
                data_json['tracking'],
                data_json['user_id'],
                case_id)
            mycursor.execute(sql_tracking, )
            mydb.commit()
            sql_case = """UPDATE customercase SET date_update='{}'WHERE case_id = '{}';""".format(
                update,
                case_id)
            mycursor.execute(sql_case, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_fastTrack_{}".format(data_json['tracking']), "status": 200})

        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def fastTracking2():
    try:
        data_json = request.json
        update = datetime.now()
        case_id = request.args['case_id']

        if checkData("case_id", case_id, "customercase") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_note = "SELECT {}_note AS note FROM tracking WHERE case_id = '{}'".format(data_json['tracking'],case_id)
            mycursor.execute(sql_note, )
            note = mycursor.fetchone()['note']
            if data_json['note'] != "":
                if note == "" or note == None:
                    note = data_json['note']
                else:
                    note = note + "/" + data_json['note']
            sql_tracking = """UPDATE tracking SET {}_date = '{}',  {}_id = '{}', {}_note = '{}' WHERE case_id = '{}';""".format(
                data_json['tracking'],
                data_json['date'],
                data_json['tracking'],
                data_json['user_id'],
                data_json['tracking'],
                note,
                case_id)
            mycursor.execute(sql_tracking, )
            mydb.commit()
            sql_case = """UPDATE customercase SET date_update='{}'WHERE case_id = '{}';""".format(
                update,
                case_id)
            mycursor.execute(sql_case, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_fastTrack_{}".format(data_json['tracking']), "status": 200})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def fastTracking():
    try:
        data_json = request.json
        update = datetime.now()
        case_id = request.args['case_id']

        if checkData("case_id", case_id, "customercase") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)

            sql = "SELECT status FROM customercase WHERE case_id = '{}';".format(
                case_id)
            mycursor.execute(sql, )
            data = mycursor.fetchone()
            status = data['status']

            if data_json['tracking'] == "car_check_up":
                if data_json['yes_no'] == "yes":
                    sql_tracking = """UPDATE tracking SET {}_date = '{}',  {}_id = '{}', {}_yn = '{}' WHERE case_id = '{}';""".format(
                        data_json['tracking'],
                        data_json['date'],
                        data_json['tracking'],
                        data_json['user_id'],
                        data_json['tracking'],
                        "yes",
                        case_id)
                    mycursor.execute(sql_tracking, )
                    mydb.commit()
                elif data_json['yes_no'] == "no":
                    sql = "SELECT submit_book_transfer_date AS date FROM tracking WHERE case_id = '{}'".format(case_id)
                    mycursor.execute(sql, )
                    date1 = mycursor.fetchone()['date']
                    print(type(date1))
                    date1_str = date1.strftime("%d/%m/%Y")
                    date1 = datetime.strptime(date1_str, "%d/%m/%Y")
                    date2 = date1 + timedelta(days=1)
                    sql_tracking = """UPDATE tracking SET {}_date = '{}',  {}_id = '{}', {}_yn = '{}' WHERE case_id = '{}';""".format(
                        data_json['tracking'],
                        date2,
                        data_json['tracking'],
                        data_json['user_id'],
                        data_json['tracking'],
                        "no",
                        case_id)
                    mycursor.execute(sql_tracking, )
                    mydb.commit()
                else:
                    return "yes_no not found"
                # process = "process"
                tracking = data_json['tracking']

            elif data_json['tracking'] == "submit_book_deposit_return":
                if data_json['deposit_12'] == "0" or data_json['deposit_12'] == 0:
                    sql = "SELECT deposit_doc_to_new_bank_date AS date FROM tracking WHERE case_id = '{}'".format(
                        case_id)
                    mycursor.execute(sql, )
                    date1 = mycursor.fetchone()['date']
                    if date1 != None:
                        date1_str = date1.strftime("%d/%m/%Y")
                        date1 = datetime.strptime(date1_str, "%d/%m/%Y")
                        date2 = date1 + timedelta(days=1)
                    else:
                        date2 = "0000-00-00"
                    sql_tracking = """UPDATE tracking SET {}_date = '{}',  {}_id = '{}', {}_yn = '{}' WHERE case_id = '{}';""".format(
                        data_json['tracking'],
                        date2,
                        data_json['tracking'],
                        data_json['user_id'],
                        data_json['tracking'],
                        "no",
                        case_id)
                    mycursor.execute(sql_tracking, )
                    mydb.commit()
                else:
                    sql_tracking = """UPDATE tracking SET {}_date = '{}',  {}_id = '{}', {}_yn = '{}' WHERE case_id = '{}';""".format(
                        data_json['tracking'],
                        data_json['date'],
                        data_json['tracking'],
                        data_json['user_id'],
                        data_json['tracking'],
                        "yes",
                        case_id)
                    mycursor.execute(sql_tracking, )
                    mydb.commit()
                sql_case2 = """UPDATE customercase SET deposit_12 = '{}' WHERE case_id = '{}';""".format(
                    data_json['deposit_12'],
                    case_id)
                mycursor.execute(sql_case2, )
                mydb.commit()
                # process = "process"
                tracking = data_json['tracking']

            elif data_json['tracking'] == "submit_book_to_new_finance":
                sql_tracking = """UPDATE tracking SET {}_date = '{}',  {}_id = '{}', {}_yn = '{}' WHERE case_id = '{}';""".format(
                    data_json['tracking'],
                    data_json['date'],
                    data_json['tracking'],
                    data_json['user_id'],
                    data_json['tracking'],
                    "yes",
                    case_id)
                mycursor.execute(sql_tracking, )
                mydb.commit()
                # process = "close"
                tracking = data_json['tracking']


            else:
                sql_tracking = """UPDATE tracking SET {}_date = '{}',  {}_id = '{}', {}_yn = '{}' WHERE case_id = '{}';""".format(
                    data_json['tracking'],
                    data_json['date'],
                    data_json['tracking'],
                    data_json['user_id'],
                    data_json['tracking'],
                    "yes",
                    case_id)
                mycursor.execute(sql_tracking, )
                mydb.commit()

                # process = "process"
                tracking = data_json['tracking']
# ---------------------------------------------------------------------------------------------------------------process

            sql_dep = "SELECT deposit_12 , case_source FROM customercase WHERE case_id = '{}'".format(case_id)
            mycursor.execute(sql_dep, )
            data_dep = mycursor.fetchone()
            deposit_12 = data_dep['deposit_12']
            case_source = data_dep['case_source']

            if deposit_12 != 0 and (case_source == "Kiatnakin" or case_source == "Thanachart"):
                if data_json['tracking'] == 'book_deposit_received':
                    process = 'complete'
                else:
                    process = 'process'
            elif deposit_12 == 0 and (case_source == "Kiatnakin" or case_source == "Thanachart"):
                if data_json['tracking'] == 'submit_book_to_new_finance':
                    process = 'complete'
                else:
                    process = 'process'
            elif deposit_12 != 0 and (case_source == "Cartrust" or case_source == "Dealer"):
                if data_json['tracking'] == 'book_deposit_received':
                    process = 'complete'
                else:
                    process = 'process'
            elif deposit_12 == 0 and (case_source == "Cartrust" or case_source == "Dealer"):
                if data_json['tracking'] == 'cash_received':
                    process = 'complete'
                else:
                    process = 'process'
            else:
                process = 'process'

# ---------------------------------------------------------------------------------------------------------------process
            sql_case = """UPDATE customercase SET status='{}',date_update='{}', process = '{}' WHERE case_id = '{}';""".format(
                tracking,
                update,
                process,
                case_id)
            mycursor.execute(sql_case, )
            mydb.commit()

            job_id = ""

            if data_json['tracking'] == "account_closing":
                sql2 = "SELECT case_source FROM customercase WHERE case_id = '{}'".format(case_id)
                mycursor.execute(sql2, )
                data = mycursor.fetchone()

                if data['case_source'] == None:
                    case_source = ""
                else:
                    case_source = data['case_source']

                job_id = createJobId(case_source, mydb, mycursor)

                no = int(job_id.split("/")[0][2:])
                year = job_id.split("/")[1]

                sql_case2 = """UPDATE customercase SET job_id = '{}',no='{}',year='{}',deposit_12='{}'WHERE case_id = '{}';""".format(
                    job_id, no, year, data_json['deposit_12'],
                    case_id)
                mycursor.execute(sql_case2, )
                mydb.commit()

            elif data_json['tracking'] == "submit_book_transfer":
                sql_case2 = """UPDATE customercase SET vendor = '{}' WHERE case_id = '{}';""".format(
                    data_json['vendor'],
                    case_id)
                mycursor.execute(sql_case2, )
                mydb.commit()

            mydb.close()
            return jsonify({"message": "success_fastTrack_{}".format(data_json['tracking']), "job_id": job_id,
                            "status": 200})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def editOtherCase():
    try:
        data_json = request.json
        update = datetime.now()
        case_id = request.args['case_id']

        if checkData("case_id", case_id, "customercase") == False:
            str_date = data_json['date'] + " " + str(update.hour) + ":" + str(update.minute)
            date = str(datetime.strptime(str_date, "%Y-%m-%d %H:%M"))
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_tracking = """UPDATE tracking SET {}_date = '{}', {}_note = '{}', {}_id = '{}', {}_yn = '{}' WHERE case_id = '{}';""".format(
                data_json['tracking'],
                date,
                data_json['tracking'],
                data_json['note_status'],
                data_json['tracking'],
                data_json['user_id'],
                data_json['tracking'],
                data_json['yes_no'],
                case_id)
            mycursor.execute(sql_tracking, )
            mydb.commit()

            tracking = ""
            if data_json['tracking'] == "submit_book_to_new_finance":
                process = "close"
                tracking = data_json['tracking']
            else:
                process = "process"
                tracking = data_json['tracking']

            sql_case = """UPDATE customercase SET status='{}',note_status='{}',date_update='{}', process = '{}' WHERE case_id = '{}';""".format(
                tracking,
                data_json['note_status'],
                update,
                process,
                case_id)
            mycursor.execute(sql_case, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_{}".format(data_json['tracking']), "status": 200})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def editCase3():
    try:
        data_json = request.json
        update = datetime.now()
        case_id = request.args['case_id']

        if checkData("case_id", case_id, "customercase") == False:
            str_date = data_json['date'] + " " + str(update.hour) + ":" + str(update.minute)
            date = str(datetime.strptime(str_date, "%Y-%m-%d %H:%M"))
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_tracking = """UPDATE tracking SET {}_date = '{}', {}_note = '{}', {}_id = '{}', {}_yn = '{}' WHERE case_id = '{}';""".format(
                data_json['tracking'],
                date,
                data_json['tracking'],
                data_json['note_status'],
                data_json['tracking'],
                data_json['user_id'],
                data_json['tracking'],
                "yes",
                case_id)
            mycursor.execute(sql_tracking, )
            mydb.commit()

            process = "process"

            sql = "SELECT case_source FROM customercase WHERE case_id = '{}'".format(case_id)
            mycursor.execute(sql, )
            data = mycursor.fetchone()

            if data['case_source'] == None:
                case_source = ""
            else:
                case_source = data['case_source']

            job_id = createJobId(case_source, mydb, mycursor)

            no = int(job_id.split("/")[0][2:])
            year = job_id.split("/")[1]

            sql_case = """UPDATE customercase SET job_id = '{}',no='{}',year='{}', status='{}',note_status='{}',date_update='{}',process='{}' WHERE case_id = '{}';""".format(
                job_id, no, year,
                data_json['tracking'],
                data_json['note_status'],
                update,
                process,
                case_id)
            mycursor.execute(sql_case, )
            mydb.commit()
            mydb.close()

            return jsonify({"message": "success_{}".format(data_json['tracking']), "job_id": job_id, "status": 200})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def editCase11():
    try:
        data_json = request.json
        update = datetime.now()
        case_id = request.args['case_id']
        if checkData("case_id", case_id, "customercase") == False:
            str_date = data_json['date'] + " " + str(update.hour) + ":" + str(update.minute)
            date = str(datetime.strptime(str_date, "%Y-%m-%d %H:%M"))
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_tracking = """UPDATE tracking SET {}_date = '{}', {}_note = '{}', {}_id = '{}', {}_yn = '{}' WHERE case_id = '{}';""".format(
                data_json['tracking'],
                date,
                data_json['tracking'],
                data_json['note_status'],
                data_json['tracking'],
                data_json['user_id'],
                data_json['tracking'],
                data_json['yes_no'],
                case_id)
            mycursor.execute(sql_tracking, )
            mydb.commit()
            process = '{}'
            sql_case = """UPDATE customercase SET status='{}',note_status='{}',date_update='{}',close_amount = '{}',process='{}' WHERE case_id = '{}';""".format(
                data_json['tracking'],
                data_json['note_status'],
                update,
                data_json['close_amount'],
                process,
                case_id)
            mycursor.execute(sql_case, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_{}".format(data_json['tracking']), "status": 200})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def editCase15():
    try:
        data_json = request.json
        update = datetime.now()
        case_id = request.args['case_id']
        if checkData("case_id", case_id, "customercase") == False:
            str_date = data_json['date'] + " " + str(update.hour) + ":" + str(update.minute)
            date = str(datetime.strptime(str_date, "%Y-%m-%d %H:%M"))
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_tracking = """UPDATE tracking SET {}_date = '{}', {}_note = '{}', {}_id = '{}', {}_yn = '{}' WHERE case_id = '{}';""".format(
                data_json['tracking'],
                date,
                data_json['tracking'],
                data_json['note_status'],
                data_json['tracking'],
                data_json['user_id'],
                data_json['tracking'],
                data_json['yes_no'],
                case_id)
            mycursor.execute(sql_tracking, )
            mydb.commit()
            process = "process"
            sql_case = """UPDATE customercase SET status='{}',note_status='{}',date_update='{}', deposit = '{}',process='{}' WHERE case_id = '{}';""".format(
                data_json['tracking'],
                data_json['note_status'],
                update,
                data_json['deposit'],
                process,
                case_id)
            mycursor.execute(sql_case, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_{}".format(data_json['tracking']), "status": 200})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def showCaseId():
    try:
        case_id = request.args['case_id']
        if checkData("case_id", case_id, "customercase") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_case = """{} 
             WHERE customercase.case_id = '{}'""".format(sql_text, case_id)
            mycursor.execute(sql_case, )
            case = mycursor.fetchone()
            sql_user = "select user_id,nickname from user"
            mycursor.execute(sql_user, )
            user = mycursor.fetchall()
            for key, val in case.items():
                if key.endswith("_nickname"):
                    for k in user:
                        if k['user_id'] == val:
                            case[key] = k['nickname']
            mydb.close()
            case = setDataBlank(case)
            case = addNextStatus(case)
            now = datetime.now()
            list_date = []
            for key, val in case.items():
                if key.endswith("_date") == True and type(val) == datetime:
                    list_date.append(val)
            list_total = []
            for date in list_date:
                list_total.append((now - date).days)
            if case['process'] == 'complete' or case['process'] == 'cancel':
                case['total_date'] = max(list_total) - min(list_total)
            else:
                case['total_date'] = max(list_total)
            print(list_date)
            print(list_total)
            return jsonify({"message": case, "status": 200})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def showCaseAll():
    try:
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_case = """{} ORDER BY customercase.id DESC;""".format(sql_text)
        mycursor.execute(sql_case, )
        case = mycursor.fetchall()
        sql_user = "select user_id,nickname from user"
        mycursor.execute(sql_user, )
        user = mycursor.fetchall()
        for i in case:
            for key, val in i.items():
                if key.endswith("_nickname"):
                    for k in user:
                        if k['user_id'] == val:
                            i[key] = k['nickname']
        mydb.close()
        case = setDataBlankAll(case)
        case = addNextStatus(case)
        case = calTotalDate(case)
        return jsonify({"message": case, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def showCaseLimit():
    try:
        page = request.args['page']
        size = request.args['size']
        start = (int(page) - 1) * int(size)
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_case = """{} ORDER BY customercase.id DESC
             LIMIT {},{} ;""".format(sql_text, start, size)
        mycursor.execute(sql_case, )
        case = mycursor.fetchall()
        sql_user = "select user_id,nickname from user"
        mycursor.execute(sql_user, )
        user = mycursor.fetchall()
        for i in case:
            for key, val in i.items():
                if key.endswith("_nickname"):
                    for k in user:
                        if k['user_id'] == val:
                            i[key] = k['nickname']
        mydb.close()
        case = setDataBlankAll(case)
        case = addNextStatus(case)
        case = calTotalDate(case)
        return jsonify({"message": case, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def deleteCase():
    try:
        case_id = request.args['case_id']
        if checkData("case_id", case_id, "customercase") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_case = "DELETE FROM customercase WHERE case_id = '{}';".format(case_id)
            sql_tracking = "DELETE FROM tracking WHERE case_id = '{}';".format(case_id)
            mycursor.execute(sql_tracking, )
            mydb.commit()
            mycursor.execute(sql_case, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_deleteCase", "status": 200})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def searchCase():
    try:
        parameter = request.args['parameter']
        value = request.args['value']
        if parameter == "date_update":
            value = value.split("-")
            value[0] = datetime.strptime(value[0], "%d/%m/%Y")
            value[1] = datetime.strptime(str(value[1] + " 23:59:59"), "%d/%m/%Y %H:%M:%S")
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_case = """{} 
             WHERE customercase.date_update BETWEEN '{}' AND '{}';""".format(sql_text, value[0], value[1])
            mycursor.execute(sql_case, )
            data = mycursor.fetchall()
            data = setDataBlankAll(data)
            data = addNextStatus(data)
            mydb.close()
            return jsonify({"message": data, "status": 200})
        else:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_customer = """{} 
                 WHERE 
                 customer.firstname LIKE '%{}%' or 
                 customer.lastname LIKE '%{}%' or 
                 customer.tel LIKE '%{}%' or 
                 customer.email LIKE '%{}%' or 
                 customer.line LIKE '%{}%' or 
                 customer.license_id LIKE '%{}%' or 
                 customer.province LIKE '%{}%' or
                 car.car_license LIKE '%{}%' or 
                 customercase.document_id LIKE '%{}%' or
                 customercase.job_id LIKE '%{}%' or
                 customercase.old_bank LIKE '%{}%' or
                 customercase.new_bank LIKE '%{}%' or
                 customercase.status LIKE '%{}%' or
                 customercase.team LIKE '%{}%' or
                 customercase.contract_officer LIKE '%{}%' or
                 customercase.finance_staff LIKE '%{}%' or
                 customercase.case_type LIKE '%{}%' or
                 customercase.case_receiver LIKE '%{}%' or
                 customercase.case_source LIKE '%{}%' 
                 ORDER BY customercase.id DESC;""".format(sql_text, value, value, value, value, value, value, value,
                                                          value, value, value, value, value, value, value, value, value,
                                                          value, value, value, value, value)
            mycursor.execute(sql_customer, )
            data = mycursor.fetchall()
            sql_user = "select user_id,nickname from user"
            mycursor.execute(sql_user, )
            user = mycursor.fetchall()
            for i in data:
                for key, val in i.items():
                    if key.endswith("_nickname"):
                        for k in user:
                            if k['user_id'] == val:
                                i[key] = k['nickname']
            mydb.close()
            data = setDataBlankAll(data)
            data = addNextStatus(data)
            data = calTotalDate(data)
            return jsonify({"message": data, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def calculateCost(data):
    data_json = data
    approve_amount = int(data_json['approve_amount'])
    old_finance_closing_fee = int(data_json['old_finance_closing_fee'])
    old_finance_transfer_fee = int(data_json['old_finance_transfer_fee'])
    book_closing_fee = int(data_json['book_closing_fee'])
    vat7_fee = int(data_json['vat7_fee'])
    transfer_fee = int(data_json['transfer_fee'])
    duty_fee = int(data_json['duty_fee'])
    cartrust_other_fee = int(data_json['cartrust_other_fee'])
    car_shield_fee = int(data_json['car_shield_fee'])
    car_insurance_fee = int(data_json['car_insurance_fee'])
    transfer_service_fee = int(data_json['transfer_service_fee'])
    contract_fee = int(data_json['contract_fee'])
    outside_transfer_fee = int(data_json['outside_transfer_fee'])
    newfinance_other_fee = int(data_json['newfinance_other_fee'])

    cartrust_total_cost = old_finance_closing_fee + old_finance_transfer_fee + book_closing_fee + vat7_fee + transfer_fee + duty_fee + cartrust_other_fee
    new_finance_total_cost = car_shield_fee + car_insurance_fee + transfer_service_fee + contract_fee + outside_transfer_fee + newfinance_other_fee
    total_cost = cartrust_total_cost + new_finance_total_cost
    amount_received = approve_amount - total_cost
    old_finance_total_cost = old_finance_transfer_fee + old_finance_closing_fee
    return {"cartrust_total_cost": cartrust_total_cost,
            "new_finance_total_cost": new_finance_total_cost,
            "total_cost": total_cost,
            "amount_received": amount_received,
            "old_finance_total_cost": old_finance_total_cost}
def saveNote():
    try:
        data_json = request.json
        update = datetime.now()
        case_id = request.args['case_id']

        if checkData("case_id", case_id, "customercase") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_tracking = """UPDATE tracking SET  {}_note = '{}', {}_id = '{}' WHERE case_id = '{}';""".format(
                data_json['tracking'],
                data_json['note_status'],
                data_json['tracking'],
                data_json['user_id'],
                case_id)
            mycursor.execute(sql_tracking, )
            mydb.commit()

            sql_case = """UPDATE customercase SET note_status='{}',date_update='{}' WHERE case_id = '{}';""".format(
                data_json['note_status'],
                update,
                case_id)
            mycursor.execute(sql_case, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_note_{}".format(data_json['tracking']), "status": 200})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERRO
def showConditionDate():
    try:
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_date = "SELECT * FROM conditiondate"
        mycursor.execute(sql_date, )
        case = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": case, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def editDate():
    try:
        data_json = request.json
        case_type = request.args['case']
        if checkData("case_type", case_type, "conditiondate") == False:
            update = datetime.now()
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)

            sql_date = """UPDATE conditiondate
                           SET red='{}', orange='{}', date_update='{}'
                           WHERE case_type = '{}';""".format(
                data_json['red'],
                data_json['orange'],
                update, case_type)
            mycursor.execute(sql_date, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_editdate", "status": 200})
        return jsonify({"message": "case type not found", "status": 500})

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def addPicture():
    try:
        data_json = request.json
        update = datetime.now()
        case_id = request.args['case_id']
        if checkData("case_id", case_id, "customercase") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_case = """UPDATE customercase SET take_car_picture='{}', car_license_book_picture='{}', license_id_picture='{}', date_update='{}' WHERE case_id = '{}';""".format(
                data_json['take_car_picture'],
                data_json['car_license_book_picture'],
                data_json['license_id_picture'],
                update,
                case_id)
            mycursor.execute(sql_case, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_addPicture", "status": 200})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def editDateStatus():
    try:
        data_json = request.json
        case_id = request.args['case_id']
        if checkData("case_id", case_id, "tracking") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)

            sql_tracking = """UPDATE tracking
                           SET {}_date='{}'
                           WHERE case_id = '{}';""".format(
                data_json['status'],
                data_json['date'],
                case_id)
            mycursor.execute(sql_tracking, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_editStatusDate", "status": 200})
        return jsonify({"message": "case type not found", "status": 500})

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def cancelCase():
    try:
        data_json = request.json
        update = datetime.now()
        case_id = request.args['case_id']
        if checkData("case_id", case_id, "customercase") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)

            sql = "SELECT process, case_id, job_id FROM customercase WHERE case_id = '{}'".format(case_id)
            mycursor.execute(sql, )
            data = mycursor.fetchone()

            if data['process'] == 'cancel':
                return jsonify(
                    {"message": "case has been canceled", "status": 200})
            else:
                if data['job_id'] != None and data['job_id'] != "":
                    case_id_cancel = data['case_id']
                    job_id_cancel = data['job_id']
                    year = job_id_cancel.split("/")[1]
                    no = int(data['job_id'].split("/")[0][2:])

                    process = "cancel"
                    job_id = ""
                    sql_case = """UPDATE customercase SET process='{}', job_id = '{}', date_update='{}' WHERE case_id = '{}';""".format(
                        process,
                        job_id,
                        update,
                        case_id)
                    mycursor.execute(sql_case, )
                    mydb.commit()

                    sql_cancel = "INSERT INTO cancel_case (case_id,job_id,year,no) VALUES ('{}','{}','{}','{}');".format(
                        case_id_cancel, job_id_cancel, year, no)
                    mycursor.execute(sql_cancel, )
                    mydb.commit()
                    mydb.close()

                    return jsonify({"message": "success_cancel - {}".format(case_id_cancel), "job_id": job_id_cancel,
                                    "status": 200})
                else:
                    process = "cancel"
                    job_id = ""
                    sql_case = """UPDATE customercase SET process='{}', job_id = '{}', date_update='{}' WHERE case_id = '{}';""".format(
                        process,
                        job_id,
                        update,
                        case_id)
                    mycursor.execute(sql_case, )
                    mydb.commit()
                    mydb.close()
                    return jsonify(
                        {"message": "success_cancel - {}".format(case_id), "job_id": "null", "status": 200})
        else:
            return jsonify({"message": "case id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def showCase1_5():
    try:
        cs = request.args['case_source']
        if cs == "":
            sql_where = ""
        else:
            sql_where = "customercase.case_source = '{}' AND".format(cs)
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_case = """{}    WHERE {}
                            (customercase.status = 'receive' or
                            customercase.status = 'contact_customer' or
                            customercase.status = 'account_closing' or
                            customercase.status = 'transfer_doc_received' or
                            customercase.status = 'transfer_doc_submitted') 
                            ORDER BY customercase.id DESC;""".format(sql_text, sql_where)
        mycursor.execute(sql_case, )
        case = mycursor.fetchall()
        sql_user = "select user_id,nickname from user"
        mycursor.execute(sql_user, )
        user = mycursor.fetchall()
        for i in case:
            for key, val in i.items():
                if key.endswith("_nickname"):
                    for k in user:
                        if k['user_id'] == val:
                            i[key] = k['nickname']
        mydb.close()
        case = setDataBlankAll(case)
        case = addNextStatus(case)
        case = calTotalDate(case)
        return jsonify({"message": case, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def showCase6_9():
    try:
        cs = request.args['case_source']
        if cs == "":
            sql_where = ""
        else:
            sql_where = "customercase.case_source = '{}' AND".format(cs)
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_case = """{}    WHERE {}
                            (customercase.status = 'book_received' or
                            customercase.status = 'submit_book_transfer' or
                            customercase.status = 'car_check_up' or
                            customercase.status = 'book_transfer') 
                            ORDER BY customercase.id DESC;""".format(sql_text, sql_where)
        mycursor.execute(sql_case, )
        case = mycursor.fetchall()
        sql_user = "select user_id,nickname from user"
        mycursor.execute(sql_user, )
        user = mycursor.fetchall()
        for i in case:
            for key, val in i.items():
                if key.endswith("_nickname"):
                    for k in user:
                        if k['user_id'] == val:
                            i[key] = k['nickname']
        mydb.close()
        case = setDataBlankAll(case)
        case = addNextStatus(case)
        case = calTotalDate(case)
        return jsonify({"message": case, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def showCase10_16():
    try:
        cs = request.args['case_source']
        if cs == "":
            sql_where = ""
        else:
            sql_where = "customercase.case_source = '{}' AND".format(cs)
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_case = """{}    WHERE {}
                            (customercase.status = 'book_copy_received' or
                            customercase.status = 'deposit_doc_to_new_bank' or
                            customercase.status = 'submit_book_deposit_return' or
                            customercase.status = 'book_received_back' or
                            customercase.status = 'cash_received' or
                            customercase.status = 'book_deposit_received' or
                            customercase.status = 'submit_book_to_new_finance')
                            ORDER BY customercase.id DESC;""".format(sql_text, sql_where)
        mycursor.execute(sql_case, )
        case = mycursor.fetchall()
        sql_user = "select user_id,nickname from user"
        mycursor.execute(sql_user, )
        user = mycursor.fetchall()
        for i in case:
            for key, val in i.items():
                if key.endswith("_nickname"):
                    for k in user:
                        if k['user_id'] == val:
                            i[key] = k['nickname']
        mydb.close()
        case = setDataBlankAll(case)
        case = addNextStatus(case)
        case = calTotalDate(case)
        return jsonify({"message": case, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def showCaseCartrustDealer():
    try:
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_case = """{}    WHERE
                            customercase.case_source = 'Cartrust'  or customercase.case_source = 'Dealer'
                            ORDER BY customercase.id DESC;""".format(sql_text)
        mycursor.execute(sql_case, )
        case = mycursor.fetchall()
        sql_user = "select user_id,nickname from user"
        mycursor.execute(sql_user, )
        user = mycursor.fetchall()
        for i in case:
            for key, val in i.items():
                if key.endswith("_nickname"):
                    for k in user:
                        if k['user_id'] == val:
                            i[key] = k['nickname']
        mydb.close()
        case = setDataBlankAll(case)
        case = addNextStatus(case)
        case = calTotalDate(case)
        return jsonify({"message": case, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def showCaseDealer():
    try:
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_case = """{}    WHERE
                            customercase.case_source = 'Dealer' 
                            ORDER BY customercase.id DESC;""".format(sql_text)
        mycursor.execute(sql_case, )
        case = mycursor.fetchall()
        sql_user = "select user_id,nickname from user"
        mycursor.execute(sql_user, )
        user = mycursor.fetchall()
        for i in case:
            for key, val in i.items():
                if key.endswith("_nickname"):
                    for k in user:
                        if k['user_id'] == val:
                            i[key] = k['nickname']
        mydb.close()
        case = setDataBlankAll(case)
        case = addNextStatus(case)
        case = calTotalDate(case)
        return jsonify({"message": case, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def showCaseCartrust():
    try:
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_case = """{}    WHERE
                            customercase.case_source = 'Cartrust' or customercase.case_source = 'cartrust'
                            ORDER BY customercase.id DESC;""".format(sql_text)
        mycursor.execute(sql_case, )
        case = mycursor.fetchall()
        sql_user = "select user_id,nickname from user"
        mycursor.execute(sql_user, )
        user = mycursor.fetchall()
        for i in case:
            for key, val in i.items():
                if key.endswith("_nickname"):
                    for k in user:
                        if k['user_id'] == val:
                            i[key] = k['nickname']
        mydb.close()
        case = setDataBlankAll(case)
        case = addNextStatus(case)
        case = calTotalDate(case)
        return jsonify({"message": case, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def showCase1_3():
    try:
        cs = request.args['case_source']
        if cs == "":
            sql_where = ""
        else:
            sql_where = "customercase.case_source = '{}' AND".format(cs)
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_case = """{}    WHERE {}
                            (customercase.status = 'receive' or
                            customercase.status = 'contact_customer' or
                            customercase.status = 'account_closing') 
                            ORDER BY customercase.id DESC;""".format(sql_text, sql_where)
        mycursor.execute(sql_case, )
        case = mycursor.fetchall()

        sql_user = "select user_id,nickname from user"
        mycursor.execute(sql_user, )
        user = mycursor.fetchall()
        for i in case:
            for key, val in i.items():
                if key.endswith("_nickname"):
                    for k in user:
                        if k['user_id'] == val:
                            i[key] = k['nickname']
        mydb.close()

        case = setDataBlankAll(case)
        case = addNextStatus(case)
        case = calTotalDate(case)
        return jsonify({"message": case, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def showCase4_14():
    try:
        cs = request.args['case_source']
        if cs == "":
            sql_where = ""
        else:
            sql_where = "customercase.case_source = '{}' AND".format(cs)
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_case = """{}    WHERE {}
                            (
                            customercase.status = 'transfer_doc_received' or
                            customercase.status = 'transfer_doc_submitted' or
                            customercase.status = 'book_received' or
                            customercase.status = 'submit_book_transfer' or
                            customercase.status = 'car_check_up' or
                            customercase.status = 'book_transfer' or
                            customercase.status = 'book_copy_received' or
                            customercase.status = 'deposit_doc_to_new_bank' or
                            customercase.status = 'book_received_back' or
                            customercase.status = 'cash_received' or
                            customercase.status = 'submit_book_to_new_finance') 
                            ORDER BY customercase.id DESC;""".format(sql_text, sql_where)
        mycursor.execute(sql_case, )
        case = mycursor.fetchall()

        sql_user = "select user_id,nickname from user"
        mycursor.execute(sql_user, )
        user = mycursor.fetchall()
        for i in case:
            for key, val in i.items():
                if key.endswith("_nickname"):
                    for k in user:
                        if k['user_id'] == val:
                            i[key] = k['nickname']
        mydb.close()

        case = setDataBlankAll(case)
        case = addNextStatus(case)
        case = calTotalDate(case)
        return jsonify({"message": case, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
def showCase15_16():
    try:
        cs = request.args['case_source']
        if cs == "":
            sql_where = ""
        else:
            sql_where = "customercase.case_source = '{}' AND".format(cs)
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_case = """{}    WHERE {}
                            (
                            customercase.status = 'submit_book_deposit_return' or
                            customercase.status = 'book_deposit_received')
                            ORDER BY customercase.id DESC;""".format(sql_text, sql_where)
        mycursor.execute(sql_case, )
        case = mycursor.fetchall()

        sql_user = "select user_id,nickname from user"
        mycursor.execute(sql_user, )
        user = mycursor.fetchall()
        for i in case:
            for key, val in i.items():
                if key.endswith("_nickname"):
                    for k in user:
                        if k['user_id'] == val:
                            i[key] = k['nickname']

        mydb.close()
        case = setDataBlankAll(case)
        case = addNextStatus(case)
        case = calTotalDate(case)
        return jsonify({"message": case, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
