# -*- coding: utf-8 -*-
from flask import Blueprint
from model.dashboard import dashboard

vm_routes_dashboard = Blueprint('/controller_dashboard', __name__)

#----------------------------------------------------------------------------------------------------------------WEEK_1

vm_routes_dashboard.add_url_rule('dashboard_count', 'dashboard/count', dashboard.countTable, methods=['GET'],)

vm_routes_dashboard.add_url_rule('dashboard_case', 'dashboard/count_case', dashboard.countCase, methods=['GET'],)