# -*- coding: utf-8 -*-
import datetime
import sys

from flask import *
from flask_api import status
from controller.all_function import *

def countTable():
    try:
        table = request.args['table']
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql = "SELECT COUNT(id) as {} FROM {}".format(table,table)
        mycursor.execute(sql, )
        count = mycursor.fetchone()[table]
        return jsonify({"table" : table, "count" : count, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR

def countCase():
    try:
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql = """SELECT     COUNT(receive_date) as receive,
                            COUNT(contact_customer_date) as contact_customer,
                            COUNT(account_closing_date) as account_closing,
                            COUNT(transfer_doc_received_date) as transfer_doc_received,
                            COUNT(transfer_doc_submitted_date) as transfer_doc_submitted,
                            COUNT(book_received_date) as book_received,
                            COUNT(submit_book_transfer_date) as submit_book_transfer,
                            COUNT(car_check_up_date) as car_check_up,
                            COUNT(book_transfer_date) as book_transfer,
                            COUNT(book_copy_received_date) as book_copy_received,
                            COUNT(deposit_doc_to_new_bank_date) as deposit_doc_to_new_bank,
                            COUNT(submit_book_deposit_return_date) as submit_book_deposit_return,
                            COUNT(book_received_back_date) as book_received_back,
                            COUNT(cash_received_date) as cash_received,
                            COUNT(book_deposit_received_date) as book_deposit_received,
                            COUNT(submit_book_to_new_finance_date) as submit_book_to_new_finance
                FROM tracking"""
        mycursor.execute(sql, )
        tracking = mycursor.fetchone()
        return jsonify({"tracking" : tracking, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno, e)
        return str(str(exc_type) + " " + str(fname) + " " + str(exc_tb.tb_lineno) + " " + str(
            e)), status.HTTP_500_INTERNAL_SERVER_ERROR
