import sys
import datetime
from flask_api import status
from flask import *

from controller.all_function import *
from controller.token import generate_access_token, cache, checkToken


def checkDataAlready():
    table = request.args['table']
    key = request.args['key']
    value = request.args['value']
    if value == "":
        jsonify({"message" : True , "data" : ""})
    mydb = connectsql()
    mycursor = mydb.cursor(dictionary=True)
    sql = "SELECT * FROM {} WHERE {} = '{}';".format(table, key, value)
    mycursor.execute(sql, )
    data = mycursor.fetchone()
    mydb.close()
    if data == None:
        return jsonify({"message" : True , "data" : ""})
    return jsonify({"message" : False , "data" : data , "address" : data['address']})

def genTokenFromPassword():
    try :
        password = request.json['password']
        if password == "123":
            token, expires = generate_access_token("love", "123")
            access_token = str(token)[2:len(token)-1]
            expires_date = timeStampToDatetime(expires)
            cache.set(access_token, expires_date, timeout=60*60*8)
            return jsonify({"expires_in": expires_date, "token": access_token, "status": 200})
        else:
            return jsonify({"message": "password incorrect", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

@checkToken
def getUserAll():
    try :
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_user = """SELECT u.id,
                                u.user_id,
                                u.username,
                                u.firstname,
                                u.lastname,
                                u.picture,
                                u.position,
                                u.type,
                                u.tel,
                                u.email,
                                u.address,
                                u.date_create,
                                u.date_update,
                                tc.team_id,
                                t.team_name,
                                r.attribute,
                                r.role_1,
                                r.role_2,
                                r.role_3
                            FROM user AS u
                            LEFT JOIN role AS r ON u.role = r.id
                            LEFT JOIN team_connecting AS tc ON u.team = tc.id
                            LEFT JOIN team AS t ON tc.team_id = t.id"""
        mycursor.execute(sql_user, )
        user_all = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": user_all, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR