# -*- coding: utf-8 -*-
from flask import Blueprint
from model.other_function import other_function

vm_routes_other = Blueprint('/controller_other', __name__)

vm_routes_other.add_url_rule('check', 'customer/check', other_function.checkDataAlready, methods=['GET'],)

vm_routes_other.add_url_rule('gen_token', 'gen/token', other_function.genTokenFromPassword, methods=['POST'],)

vm_routes_other.add_url_rule('get_user', 'get/user', other_function.getUserAll, methods=['GET'],)