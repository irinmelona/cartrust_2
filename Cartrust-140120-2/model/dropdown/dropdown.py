# -*- coding: utf-8 -*-
import datetime
import sys

from flask import *
from flask_api import status
from controller.all_function import *


def addFinanceStaff():
    try:
        data_json = request.json
        created = datetime.now()

        fs_id = createId("FS","fs_id","finance_staff")
        check = True
        while check == True:
            fs_id = createId("FS","fs_id","finance_staff")
            if checkData('fs_id', fs_id, 'finance_staff') == True:
                check = False

        if checkData('fs_name', data_json['name'], 'finance_staff') == True:
            if checkData('tel', data_json['tel'], 'finance_staff') == True:
                if checkData('line', data_json['line'], 'finance_staff') == True:
                    mydb = connectsql()
                    mycursor = mydb.cursor(dictionary=True)
                    sql = """INSERT INTO finance_staff
                                        (fs_id,fs_name,tel,line,date_create,date_update)
                                        VALUES ('{}','{}','{}','{}','{}','{}');""".format(
                        fs_id,
                        data_json['name'],
                        data_json['tel'],
                        data_json['line'],
                        created,
                        created)
                    mycursor.execute(sql, )
                    mydb.commit()
                    mydb.close()
                    return jsonify({"message": "success_addDropdown" , "id" : fs_id, "status": 200})
                else:
                    return jsonify({"message": "line already", "status": 500})
            else:
                return jsonify({"message": "tel already", "status": 500})
        else:
            return jsonify({"message": "fs name already", "status": 500})

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR
def addMarginAccount():
    try:
        data_json = request.json
        created = datetime.now()

        ma_id = createId("MA","ma_id","margin_account")
        check = True
        while check == True:
            ma_id = createId("MA","ma_id","margin_account")
            if checkData('ma_id', ma_id, 'margin_account') == True:
                check = False

        if checkData('ma_name', data_json['name'], 'margin_account') == True:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql = """INSERT INTO margin_account
                                (ma_id,ma_name,date_create,date_update)
                                VALUES ('{}','{}','{}','{}');""".format(
                ma_id,
                data_json['name'],
                created,
                created)
            mycursor.execute(sql, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_addDropdown" , "id" : ma_id, "status": 200})
        else:
            return jsonify({"message": "ma name already", "status": 500})

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR
def addCQCTeam():
    try:
        data_json = request.json
        created = datetime.now()

        cqc_id = createId("CQC","cqc_id","cqc_team")
        check = True
        while check == True:
            cqc_id = createId("CQC","cqc_id","cqc_team")
            if checkData('cqc_id', cqc_id, 'cqc_team') == True:
                check = False

        if checkData('cqc_name', data_json['name'], 'cqc_team') == True:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql = """INSERT INTO cqc_team
                                (cqc_id,cqc_name,date_create,date_update)
                                VALUES ('{}','{}','{}','{}');""".format(
                cqc_id,
                data_json['name'],
                created,
                created)
            mycursor.execute(sql, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_addDropdown" , "id" : cqc_id, "status": 200})
        else:
            return jsonify({"message": "cqc name already", "status": 500})

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR
def addHub():
    try:
        data_json = request.json
        created = datetime.now()

        hub_id = createId("HUB","hub_id","hub")
        check = True
        while check == True:
            cqc_id = createId("HUB","hub_id","hub")
            if checkData('hub_id', hub_id, 'hub') == True:
                check = False

        if checkData('hub_name', data_json['name'], 'hub') == True:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql = """INSERT INTO hub
                                (hub_id,hub_name,date_create,date_update)
                                VALUES ('{}','{}','{}','{}');""".format(
                hub_id,
                data_json['name'],
                created,
                created)
            mycursor.execute(sql, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_addDropdown" , "id" : hub_id, "status": 200})
        else:
            return jsonify({"message": "hub name already", "status": 500})

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR
def addCartrustLead():
    try:
        data_json = request.json
        created = datetime.now()

        cl_id = createId("CL","cl_id","cartrust_lead")
        check = True
        while check == True:
            cl_id = createId("CL","cl_id","cartrust_lead")
            if checkData('cl_id', cl_id, 'cartrust_lead') == True:
                check = False

        if checkData('cl_name', data_json['name'], 'cartrust_lead') == True:
            if checkData('cl_id4', data_json['cl_id4'], 'cartrust_lead') == True:
                if checkData('tel', data_json['tel'], 'cartrust_lead') == True:
                    if checkData('line', data_json['line'], 'cartrust_lead') == True:
                        mydb = connectsql()
                        mycursor = mydb.cursor(dictionary=True)
                        sql = """INSERT INTO cartrust_lead
                                            (cl_id,cl_id4,cl_name,tel,line,date_create,date_update)
                                            VALUES ('{}','{}','{}','{}','{}','{}','{}');""".format(
                            cl_id,
                            data_json['cl_id4'],
                            data_json['name'],
                            data_json['tel'],
                            data_json['line'],
                            created,
                            created)
                        mycursor.execute(sql, )
                        mydb.commit()
                        mydb.close()
                        return jsonify({"message": "success_addDropdown" , "id" : cl_id, "status": 200})
                    else:
                        return jsonify({"message": "line already", "status": 500})
                else:
                    return jsonify({"message": "tel already", "status": 500})
            else:
                return jsonify({"message": "cl id4 already", "status": 500})
        else:
            return jsonify({"message": "cl name already", "status": 500})

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR
def addDealer():
    try:
        data_json = request.json
        created = datetime.now()

        d_id = createId("D","d_id","dealer")
        check = True
        while check == True:
            d_id = createId("D","d_id","dealer")
            if checkData('d_id', d_id, 'dealer') == True:
                check = False

        if checkData('d_name', data_json['name'], 'dealer') == True:
            if checkData('tel', data_json['tel'], 'dealer') == True:
                if checkData('line', data_json['line'], 'dealer') == True:
                    mydb = connectsql()
                    mycursor = mydb.cursor(dictionary=True)
                    sql = """INSERT INTO dealer
                                        (d_id,d_name,tel,line,date_create,date_update)
                                        VALUES ('{}','{}','{}','{}','{}','{}');""".format(
                        d_id,
                        data_json['name'],
                        data_json['tel'],
                        data_json['line'],
                        created,
                        created)
                    mycursor.execute(sql, )
                    mydb.commit()
                    mydb.close()
                    return jsonify({"message": "success_addDropdown" , "id" : d_id, "status": 200})
                else:
                    return jsonify({"message": "line already", "status": 500})
            else:
                return jsonify({"message": "tel already", "status": 500})
        else:
            return jsonify({"message": "d name already", "status": 500})

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR
def addContractOfficer():
    try:
        data_json = request.json
        created = datetime.now()

        co_id = createId("CO","co_id","contract_officer")
        check = True
        while check == True:
            co_id = createId("CO","co_id","contract_officer")
            if checkData('co_id', co_id, 'contract_officer') == True:
                check = False

        if checkData('co_name', data_json['name'], 'contract_officer') == True:
            if checkData('tel', data_json['tel'], 'contract_officer') == True:
                if checkData('line', data_json['line'], 'contract_officer') == True:
                    mydb = connectsql()
                    mycursor = mydb.cursor(dictionary=True)
                    sql = """INSERT INTO contract_officer
                                        (co_id,co_name,tel,line,date_create,date_update,type)
                                        VALUES ('{}','{}','{}','{}','{}','{}','{}');""".format(
                        co_id,
                        data_json['name'],
                        data_json['tel'],
                        data_json['line'],
                        created,
                        created,
                        data_json['type'])
                    mycursor.execute(sql, )
                    mydb.commit()
                    mydb.close()
                    return jsonify({"message": "success_addDropdown" , "id" : co_id, "status": 200})
                else:
                    return jsonify({"message": "line already", "status": 500})
            else:
                return jsonify({"message": "tel already", "status": 500})
        else:
            return jsonify({"message": "co name already", "status": 500})

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR
def addVendor():
    try:
        data_json = request.json
        created = datetime.now()

        vendor_id = createId("V","vendor_id","vendor")
        check = True
        while check == True:
            vendor_id = createId("V","vendor_id","vendor")
            if checkData('vendor_id', vendor_id, 'vendor') == True:
                check = False

        if checkData('vendor_name', data_json['name'], 'vendor') == True:
            if checkData('tel', data_json['tel'], 'vendor') == True:
                mydb = connectsql()
                mycursor = mydb.cursor(dictionary=True)
                sql = """INSERT INTO vendor
                                    (vendor_id,vendor_name,tel,contact_name,date_create,date_update)
                                    VALUES ('{}','{}','{}','{}','{}','{}');""".format(
                    vendor_id,
                    data_json['name'],
                    data_json['tel'],
                    data_json['contact_name'],
                    created,
                    created)
                mycursor.execute(sql, )
                mydb.commit()
                mydb.close()
                return jsonify({"message": "success_addDropdown" , "id" : vendor_id, "status": 200})
            else:
                return jsonify({"message": "tel already", "status": 500})
        else:
            return jsonify({"message": "vendor name already", "status": 500})

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showDropdownAll():
    try:
        table = request.args['table']
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        if table == "finance_staff" :
            sql = """SELECT nickname as fs_name, 
                                        tel,line,user_id
                                        from user"""
            mycursor.execute(sql, )
            data = mycursor.fetchall()
        elif table == "contract_officer":
            sql = "SELECT * from contract_officer WHERE type = 'TB';"
            mycursor.execute(sql, )
            data = mycursor.fetchall()
        elif table == "contract_officer_kk":
            sql = "SELECT * from contract_officer WHERE type = 'KK';"
            mycursor.execute(sql, )
            data = mycursor.fetchall()
        else:
            sql = "SELECT * from {}".format(table)
            mycursor.execute(sql, )
            data = mycursor.fetchall()

        mydb.close()
        return jsonify({"message": data, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR