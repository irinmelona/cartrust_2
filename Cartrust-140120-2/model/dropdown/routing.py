# -*- coding: utf-8 -*-
from flask import Blueprint
from model.dropdown import dropdown

vm_routes_dropdown = Blueprint('/controller_dd', __name__)

vm_routes_dropdown.add_url_rule('finance_staff', 'fs/add', dropdown.addFinanceStaff, methods=['POST'],)

vm_routes_dropdown.add_url_rule('margin_account', 'ma/add', dropdown.addMarginAccount, methods=['POST'],)

vm_routes_dropdown.add_url_rule('cqc_team', 'cqc/add', dropdown.addCQCTeam, methods=['POST'],)

vm_routes_dropdown.add_url_rule('hub', 'hub/add', dropdown.addHub, methods=['POST'],)

vm_routes_dropdown.add_url_rule('cartrust_lead', 'cl/add', dropdown.addCartrustLead, methods=['POST'],)

vm_routes_dropdown.add_url_rule('dealer', 'dealer/add', dropdown.addDealer, methods=['POST'],)

vm_routes_dropdown.add_url_rule('contract_officer', 'contract_officer/add', dropdown.addContractOfficer, methods=['POST'],)

vm_routes_dropdown.add_url_rule('vendor', 'vendor/add', dropdown.addVendor, methods=['POST'],)

vm_routes_dropdown.add_url_rule('dropdown', 'dropdown/show', dropdown.showDropdownAll, methods=['GET'],)