# -*- coding: utf-8 -*-
from flask import Blueprint
from model.car import car

vm_routes_car = Blueprint('/controller_car', __name__)

# [1] เพิ่ม car
vm_routes_car.add_url_rule('add_car', 'car/add', car.addCar, methods=['POST'],)
# [2] แสดง car ระบุ ID
vm_routes_car.add_url_rule('car', 'car/show', car.showCarId, methods=['GET'],)
# [3] แสดง car ทั้งหมเด
vm_routes_car.add_url_rule('car_all', 'car/show_all', car.showCarAll, methods=['GET'],)
# [4] แสดง car ในแต่ละหน้า
vm_routes_car.add_url_rule('car_limit', 'car/show_limit', car.showCarLimit, methods=['GET'],)
# [5] แก้ไข car
vm_routes_car.add_url_rule('edit_car', 'car/edit', car.editCar, methods=['PUT'],)
# [6] ลบ car
vm_routes_car.add_url_rule('delete_car', 'car/delete', car.deleteCar, methods=['DELETE'],)