# -*- coding: utf-8 -*-
import datetime
import sys

from flask import *
from flask_api import status
from controller.all_function import *

key_list = ["car_brand","car_model","car_name","car_year","car_license","car_province","detail"]

def addCar():
    try:
        data_json = request.json
        created = datetime.now()

        car_id = createId("CAR","car_id","car")
        check = True
        while check == True:
            car_id = createId("CAR","car_id","car")
            if checkData('car_id', car_id, 'car') == True:
                check = False

        if checkKey(data_json,key_list)['key_lost'] == []:
            if checkData('car_license', data_json['car_license'], 'car') == True:
                mydb = connectsql()
                mycursor = mydb.cursor(dictionary=True)
                sql_car = """INSERT INTO car
                                    (car_id,car_brand,car_model,car_name,car_year,car_license,car_province,detail,date_create,date_update)
                                    VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}');""".format(
                    car_id,
                    data_json['car_brand'],
                    data_json['car_model'],
                    data_json['car_name'],
                    data_json['car_year'],
                    data_json['car_license'],
                    data_json['car_province'],
                    data_json['detail'],
                    created,
                    created)
                mycursor.execute(sql_car, )
                mydb.commit()
                mydb.close()
                return jsonify({"message": "success_addCar" , "car_id" : car_id, "status": 200})
            else:
                return jsonify({"message": "car license already", "status": 500})
        else:
            return jsonify({"message": "key_lost", "list": checkKey(data_json, key_list)['key_lost'], "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showCarId():
    try:
        car_id = request.args['car_id']
        if checkData("car_id",car_id,"car") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_car = "SELECT * FROM car WHERE car_id = '{}'".format(car_id)
            mycursor.execute(sql_car, )
            car = mycursor.fetchone()
            mydb.close()
            return jsonify({"message": car, "status": 200})
        else:
            return jsonify({"message": "car id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showCarAll():
    try:
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_car = "SELECT * FROM car"
        mycursor.execute(sql_car, )
        car = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": car, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showCarLimit():
    try:
        page = request.args['page']
        size = request.args['size']
        start = (int(page) - 1) * int(size)
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_car = "SELECT * FROM car LIMIT {},{}".format(start,size)
        mycursor.execute(sql_car, )
        car = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": car, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def editCar():
    try:
        data_json = request.json
        car_id = request.args['car_id']
        if checkData("car_id",car_id,"car") == False:
            update = datetime.now()
            if checkKey(data_json,key_list)['key_lost'] == []:
                if checkVal(data_json)['val_lost'] == []:
                    if checkDataEdit('car_license', 'car', data_json['car_license'], 'car_id', car_id) == True:
                        mydb = connectsql()
                        mycursor = mydb.cursor(dictionary=True)
                        sql_car = """UPDATE car SET car_brand='{}',car_model='{}',car_name='{}',car_year='{}',
                        car_license='{}',car_province='{}',detail='{}',date_update='{}'
                                         WHERE car_id = '{}';""".format(
                            data_json['car_brand'],
                            data_json['car_model'],
                            data_json['car_name'],
                            data_json['car_year'],
                            data_json['car_license'],
                            data_json['car_province'],
                            data_json['detail'],
                            update,car_id)
                        mycursor.execute(sql_car, )
                        mydb.commit()
                        mydb.close()
                        return jsonify({"message": "success_editCar", "status": 200})
                    return jsonify({"message": "car license already", "status": 500})
                else:
                    return jsonify({"message": "val_lost", "list": checkVal(data_json)['val_lost'], "status": 500})
            else:
                return jsonify({"message": "key_lost", "list": checkKey(data_json, key_list)['key_lost'], "status": 500})
        else:
            return jsonify({"message": "car id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def deleteCar():
    try:
        car_id = request.args['car_id']
        if checkData("car_id",car_id,"car") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_car = "DELETE FROM car WHERE car_id = '{}';".format(car_id)
            mycursor.execute(sql_car, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_deleteCar", "status": 200})
        else:
            return jsonify({"message": "car id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR