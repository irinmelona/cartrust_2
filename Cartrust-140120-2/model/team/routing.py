# -*- coding: utf-8 -*-
from flask import Blueprint
from model.team import team

vm_routes_team = Blueprint('/controller_team', __name__)

#----------------------------------------------------------------------------------------------------------------WEEK_1
# [1] เพิ่ม team
vm_routes_team.add_url_rule('add_team', 'team/add', team.addTeam, methods=['POST'],)
# [2] แสดง team ระบุ ID
vm_routes_team.add_url_rule('team', 'team/show', team.showTeamId, methods=['GET'],)
# [3] แสดง team ทั้งหมเด
vm_routes_team.add_url_rule('team_all', 'team/show_all', team.showTeamAll,methods=['GET'],)
# [4] แสดง team ในแต่ละหน้า
vm_routes_team.add_url_rule('team_limit', 'team/show_limit', team.showTeamLimit, methods=['GET'],)
# [5] แก้ไข team
vm_routes_team.add_url_rule('edit_team', 'team/edit', team.editTeam, methods=['PUT'],)
# [6] ลบ team
vm_routes_team.add_url_rule('delete_team', 'team/delete', team.deleteTeam, methods=['DELETE'],)