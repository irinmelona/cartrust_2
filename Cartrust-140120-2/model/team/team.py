# -*- coding: utf-8 -*-
import datetime
import sys

from flask import *
from controller.all_function import *
from flask_api import status
key_list = ["team_name"]

def addTeam():
    try:
        data_json = request.json
        created = datetime.now()

        team_id = createId("T", "team_id", "team")
        check = True
        while check == True:
            team_id = createId("T", "team_id", "team")
            if checkData('team_id', team_id, 'team') == True:
                check = False

        if checkData('team_name', data_json['team_name'], 'team') == True:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_team = """INSERT INTO team
                                    (team_id,team_name,date_create,date_update)
                                    VALUES ('{}','{}','{}','{}');""".format(team_id,
                                                                            data_json['team_name'],
                                                                            created,
                                                                            created)
            mycursor.execute(sql_team, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_addTeam", "status": 200})
        else:
            return jsonify({"message": "team name already", "status": 500})

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showTeamId():
    try:
        team_id = request.args['team_id']
        if checkData("team_id",team_id,"team") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_team = "SELECT * FROM team WHERE team_id = '{}'".format(team_id)
            mycursor.execute(sql_team, )
            team = mycursor.fetchone()
            mydb.close()
            return jsonify({"message": team, "status": 200})
        else:
            return jsonify({"message": "team id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showTeamAll():
    try:
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_team = "SELECT * FROM team"
        mycursor.execute(sql_team, )
        team = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": team, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def showTeamLimit():
    try:
        page = request.args['page']
        size = request.args['size']
        start = (int(page)-1) * int(size)
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_team = "SELECT * FROM team LIMIT {},{}".format(start, size)
        mycursor.execute(sql_team, )
        team = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": team, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def editTeam():
    try:
        data_json = request.json
        team_id = request.args['team_id']
        if checkData("team_id",team_id,"team") == False:
            update = datetime.now()

            if checkKey(data_json,key_list)['key_lost'] == []:
                if checkVal(data_json)['val_lost'] == []:
                    if checkDataEdit('team_name', data_json['team_name'], 'team', 'team_id', team_id) == True:
                        mydb = connectsql()
                        mycursor = mydb.cursor(dictionary=True)

                        sql_team = """UPDATE team
                                    SET team_name='{}', date_update='{}'
                                    WHERE team_id = '{}';""".format(data_json['team_name'],
                                                                    update, team_id)
                        mycursor.execute(sql_team, )
                        mydb.commit()
                        mydb.close()
                        return jsonify({"message": "success_editTeam", "status": 200})
                    return jsonify({"message": "team name already", "status": 500})
                else:
                    return jsonify({"message": "val_lost", "list": checkVal(data_json)['val_lost'], "status": 500})
            else:
                return jsonify({"message": "key_lost", "list": checkKey(data_json, key_list)['key_lost'], "status": 500})
        else:
            return jsonify({"message": "team id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def deleteTeam():
    try:
        team_id = request.args['team_id']
        if checkData("team_id",team_id,"team") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_team = "DELETE FROM team WHERE team_id = '{}';".format(team_id)
            mycursor.execute(sql_team, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_deleteTeam", "status": 200})
        else:
            return jsonify({"message": "team id not found", "status": 500})
    except mysql.connector.errors.IntegrityError :
        return jsonify({"message": "team id can't delete", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR