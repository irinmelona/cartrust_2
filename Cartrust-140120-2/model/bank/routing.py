# -*- coding: utf-8 -*-
from flask import Blueprint
from model.bank import bank

vm_routes_bank = Blueprint('/controller_bank', __name__)

# [1] เพิ่ม bank
vm_routes_bank.add_url_rule('add_bank', 'bank/add', bank.addBank, methods=['POST'],)
# [2] แสดง bank ระบุ ID
vm_routes_bank.add_url_rule('bank', 'bank/show', bank.showBankId, methods=['GET'],)
# [3] แสดง bank ทั้งหมเด
vm_routes_bank.add_url_rule('bank_all', 'bank/show_all', bank.showBankAll, methods=['GET'],)
# [4] แสดง bank ในแต่ละหน้า
vm_routes_bank.add_url_rule('bank_limit','bank/show_limit', bank.showBankLimit, methods=['GET'],)
# [5] แก้ไข bank
vm_routes_bank.add_url_rule('edit_bank', 'bank/edit', bank.editBank, methods=['PUT'],)
# [6] ลบ bank
vm_routes_bank.add_url_rule('delete_bank', 'bank/delete', bank.deleteBank, methods=['DELETE'],)