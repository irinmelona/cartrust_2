# -*- coding: utf-8 -*-
import datetime
import sys

from flask import *
from flask_api import status
from controller.all_function import *

key_list = ["bank_name", "detail"]


def addBank():
    try:
        data_json = request.json
        created = datetime.now()

        bank_id = "B_" + createId()
        check = True
        while check == True:
            bank_id = "B_" + createId()
            if checkData('bank_id', bank_id, 'bank') == True:
                check = False

        if checkKey(data_json, key_list)['key_lost'] == []:

            if checkData('bank_name', data_json['bank_name'], 'bank') == True:
                mydb = connectsql()
                mycursor = mydb.cursor(dictionary=True)
                sql_bank = """INSERT INTO bank
                                    (bank_id,bank_name,detail,date_create,date_update)
                                    VALUES ('{}','{}','{}','{}','{}');""".format(bank_id,
                                                                                 data_json['bank_name'],
                                                                                 data_json['detail'],
                                                                                 created,
                                                                                 created)
                mycursor.execute(sql_bank, )
                mydb.commit()
                mydb.close()
                return jsonify({"message": "success_addBank", "status": 200})
            else:
                return jsonify({"message": "bank name already", "status": 500})

        else:
            return jsonify({"message": "key_lost",
                            "list": checkKey(data_json, key_list)['key_lost'], "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR


def showBankId():
    try:
        bank_id = request.args['bank_id']
        if checkData("bank_id",bank_id,"bank") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_bank = "SELECT * FROM bank WHERE bank_id = '{}'".format(bank_id)
            mycursor.execute(sql_bank, )
            bank = mycursor.fetchone()
            mydb.close()
            return jsonify({"message": bank, "status": 200})
        else:
            return jsonify({"message": "bank id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR


def showBankAll():
    try:
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_bank = "SELECT * FROM bank"
        mycursor.execute(sql_bank, )
        bank = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": bank, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR


def showBankLimit():
    try:
        page = request.args['page']
        size = request.args['size']
        start = (int(page) - 1) * int(size)
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_bank = "SELECT * FROM bank LIMIT {},{}".format(start, size)
        mycursor.execute(sql_bank, )
        bank = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": bank, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR


def editBank():
    try:
        data_json = request.json
        bank_id = request.args['bank_id']
        if checkData("bank_id",bank_id,"bank") == False:
            update = datetime.now()

            if checkKey(data_json, key_list)['key_lost'] == []:
                if checkDataEdit('bank_name', data_json['bank_name'], 'bank', 'bank_id', bank_id) == True:
                    mydb = connectsql()
                    mycursor = mydb.cursor(dictionary=True)

                    sql_bank = """UPDATE bank
                                   SET bank_name='{}', detail='{}', date_update='{}'
                                   WHERE bank_id = '{}';""".format(
                        data_json['bank_name'],
                        data_json['detail'],
                        update, bank_id)
                    mycursor.execute(sql_bank, )
                    mydb.commit()
                    mydb.close()
                    return jsonify({"message": "success_editBank", "status": 200})
                return jsonify({"message": "bank name already", "status": 500})

            else:
                return jsonify({"message": "key_lost",
                                "list": checkKey(data_json, key_list)['key_lost'], "status": 500})
        else:
            return jsonify({"message": "bank id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR


def deleteBank():
    try:
        bank_id = request.args['bank_id']
        if checkData("bank_id", bank_id, "bank") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_bank = "DELETE FROM bank WHERE bank_id = '{}';".format(bank_id)
            mycursor.execute(sql_bank, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_deleteBank", "status": 200})
        else:
            return jsonify({"message": "bank id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR
