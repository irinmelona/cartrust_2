# -*- coding: utf-8 -*-
from flask import Blueprint
from model.login import login

vm_routes_login = Blueprint('/controller_login', __name__)

#----------------------------------------------------------------------------------------------------------------WEEK_1
# [1] user เข้าสู่ระบบ
vm_routes_login.add_url_rule('login', 'user/login', login.userLogin, methods=['POST'],)
# [2] user ออกจากระบบ
vm_routes_login.add_url_rule('logout', 'user/logout', login.userLogout, methods=['POST'],)