# -*- coding: utf-8 -*-
import sys
from flask import *
from flask_api import status
from controller.all_function import *
from controller.token import generate_access_token, cache

ACCESS_TOKEN_TIME = 60

def userLogin():
    try:
        data_json = request.json
        username = data_json['username']
        password = data_json['password']
        if username == "" or password == "" :
            return jsonify({"message": "username&password invalid","status": 500})
        if checkData("username",username,"user") == False :
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_user = """SELECT u.id,
                        u.user_id,
                        u.username,
                        u.firstname,
                        u.lastname,
                        u.password,
                        u.picture,
                        u.position,
                        u.type,
                        u.tel,
                        u.email,
                        u.line,
                        u.nickname,
                        u.address,
                        u.date_create,
                        u.date_update,
                        t.team_name,
                        t.team_id
                    FROM user AS u
                    LEFT JOIN team AS t ON u.team = t.team_id WHERE u.username = '{}'""".format(username)
            mycursor.execute(sql_user)
            user = mycursor.fetchone()
            mydb.close()
            if user != None :
                user = createRole(user)
                check_password = verify_password(user['password'], password)
                if check_password == True:
                    token, expires = generate_access_token(user['username'],user['password'])
                    access_token = str(token)[2:]
                    expires_date = timeStampToDatetime(expires)
                    cache.set(access_token,expires_date,timeout=60*ACCESS_TOKEN_TIME)
                    user['password'] = ""
                    return jsonify({"expires_in": expires_date,"token" : access_token ,"user" : user, "status": 200})
                else:
                    return jsonify({"message": "password incorrect", "status": 500})
        else:
            return jsonify({"message": "username not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def userLogout():
    try:
        cache.delete(request.headers.get('Authorization'))
        return jsonify({"message": "success_logout", "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR
