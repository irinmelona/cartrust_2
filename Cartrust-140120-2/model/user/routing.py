# -*- coding: utf-8 -*-
from flask import Blueprint
from model.user import user

vm_routes_user = Blueprint('/controller_user', __name__)

#----------------------------------------------------------------------------------------------------------------WEEK_1
# [1] เพิ่ม user (ลงทะเบียนผู้ใช้)
vm_routes_user.add_url_rule('register', 'user/add', user.addUser, methods=['POST'],)
# [2] แสดง user ระบุ ID
vm_routes_user.add_url_rule('user', 'user/show', user.showUserId, methods=['GET'],)
# [3] แสดง user ทั้งหมเด
vm_routes_user.add_url_rule('user_all', 'user/show_all', user.showUserAll, methods=['GET'],)
# [4] แสดง user ในแต่ละหน้า
vm_routes_user.add_url_rule('user_limit', 'user/show_limit', user.showUserLimit, methods=['GET'],)
# [5] แก้ไข user
vm_routes_user.add_url_rule('edit_user', 'user/edit', user.editUser, methods=['PUT'],)
# [6] ลบ user
vm_routes_user.add_url_rule('delete_user', 'user/delete', user.deleteUser, methods=['DELETE'],)
# [7] เปลี่ยน password ผู้ใช้
vm_routes_user.add_url_rule('change_password', 'user/change_password', user.changePassword, methods=['PUT'],)

#----------------------------------------------------------------------------------------------------------------WEEK_2
# [8] ลืม password ผู้ใช้
vm_routes_user.add_url_rule('forgot_password', 'user/forgot_password', user.forgotPassword, methods=['PUT'],)
# [9] ค้นหา user
vm_routes_user.add_url_rule('search_user', 'user/search', user.searchUser, methods=['GET'],)