# -*- coding: utf-8 -*-
import datetime
import sys

from flask import *
from controller.all_function import *
from controller.send_email import *
from controller.token import *
from flask_api import status

key_list_add = ["username","firstname","lastname","team","picture","position","type","address","line","nickname"]
key_list_edit = ["firstname","lastname","team","picture","position","type","address","email","line","nickname"]
key_list_password = ["old_password","new_password_1","new_password_2"]

sql_text = """SELECT u.id,
                            u.user_id,
                            u.username,
                            u.firstname,
                            u.lastname,
                            u.picture,
                            u.position,
                            u.type,
                            u.address,
                            u.tel,
                            u.email,
                            u.line,
                            u.nickname,
                            u.date_create,
                            u.date_update,
                            t.team_name,
                            t.team_id
                        FROM user AS u
                        LEFT JOIN team AS t ON u.team = t.team_id"""


def addUser():
    try:
        data_json = request.json
        created = datetime.now()

        if checkData("team_id", data_json['team'], "team") == True:
            return jsonify({"message": "team id not found", "status": 500})

        user_id = createId("ACC","user_id","user")
        check = True
        while check == True:
            user_id = createId("ACC","user_id","user")
            if checkData('user_id', user_id, 'user') == True:
                check = False

        password = createPassword()


        if checkKey(data_json,key_list_add)['key_lost'] == []:

            password_hashed = hash_password(password)
            data_json = booleanToDemical(data_json)

            if checkData('username', data_json['username'], 'user') == True:
                if checkData('email', data_json['email'], 'user') == True:
                    if checkData('tel', data_json['tel'], 'user') == True:
                        if checkData('line', data_json['line'], 'user') == True:

                            sendMail2(data_json['email'], data_json['username'], password)

                            mydb = connectsql()
                            mycursor = mydb.cursor(dictionary=True)

                            sql_user = """INSERT INTO user
                                                    (user_id,username,password,firstname,lastname,team,picture,position,date_create,date_update,type,address,tel,email,line,nickname)
                                                    VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}');""".format(
                                user_id,
                                data_json['username'],
                                password_hashed,
                                data_json['firstname'],
                                data_json['lastname'],
                                data_json['team'],
                                data_json['picture'],
                                data_json['position'],
                                created, created,
                                data_json['type'],
                                data_json['address'],
                                data_json['tel'],
                                data_json['email'],
                                data_json['line'],
                                data_json['nickname'])

                            mycursor.execute(sql_user, )
                            mydb.commit()
                            mydb.close()

                            return jsonify({"message": "success_addUser","user_id" : user_id, "status": 200})
                        else:
                            return jsonify({"message": "line already", "status": 500})
                    else:
                        return jsonify({"message": "tel already", "status": 500})
                else:
                    return jsonify({"message": "email already", "status": 500})
            else:
                return jsonify({"message": "username already", "status": 500})
        else:
            return jsonify({"message": "key_lost", "list": checkKey(data_json, key_list_add)['key_lost'], "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR


def showUserId():
    try:
        user_id = request.args['user_id']
        if checkData("user_id",user_id,"user") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_user = """{}
                        WHERE u.user_id = '{}'""".format(sql_text,user_id)
            mycursor.execute(sql_user, )
            user = mycursor.fetchone()
            mydb.close()
            if user != None :
                return jsonify({"message": user, "status": 200})
            return jsonify({"message": None, "status": 200})
        else:
            return jsonify({"message": "user id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR


def showUserAll():
    try:
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_user = """{}""".format(sql_text)
        mycursor.execute(sql_user, )
        user_all = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": user_all, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR


def showUserLimit():
    try:
        page = request.args['page']
        size = request.args['size']
        start = (int(page) - 1) * int(size)
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_user = """{} LIMIT {},{}""".format(sql_text,start,size)
        mycursor.execute(sql_user, )
        user_all = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": user_all, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR


def editUser():
    try:
        data_json = request.json
        data_json = booleanToDemical(data_json)
        user_id = request.args['user_id']
        if checkData("team_id", data_json['team'], "team") == True:
            return jsonify({"message": "team id not found", "status": 500})

        if checkData("user_id",user_id,"user") == False:
            update = datetime.now()

            if checkKey(data_json,key_list_edit)['key_lost'] == []:

                mydb = connectsql()
                mycursor = mydb.cursor(dictionary=True)
                sql_user = """UPDATE user
                                SET firstname='{}', lastname='{}',team = '{}', picture='{}', position='{}', date_update='{}', 
                                type='{}', address='{}',line = '{}', email='{}',nickname = '{}'
                                WHERE user_id = '{}';""".format(
                    data_json['firstname'],
                    data_json['lastname'],
                    data_json['team'],
                    data_json['picture'],
                    data_json['position'],
                    update,
                    data_json['type'],
                    data_json['address'],
                    data_json['line'],
                    data_json['email'],
                    data_json['nickname'],
                    user_id)
                mycursor.execute(sql_user, )
                mydb.commit()
                mydb.close()
                return jsonify({"message": "success_editUser", "status": 200})

            else:
                return jsonify({"message": "key_lost", "list": checkKey(data_json, key_list_edit)['key_lost'], "status": 500})
        else:
            return jsonify({"message": "user id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def editTeam():
    try:
        data_json = request.json
        data_json = booleanToDemical(data_json)
        user_id = request.args['user_id']
        if checkData("team_id", data_json['team'], "team") == True:
            return jsonify({"message": "team id not found", "status": 500})

        if checkData("user_id",user_id,"user") == False:
            update = datetime.now()

            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_user = """UPDATE user
                            SET team = '{}',date_update='{}'WHERE user_id = '{}';""".format(
                data_json['team'],
                update,
                user_id)
            mycursor.execute(sql_user, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_editTeam", "status": 200})

        else:
            return jsonify({"message": "user id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR

def deleteUser():
    try:
        user_id = request.args['user_id']
        if checkData("user_id",user_id,"user") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_user = "DELETE FROM user WHERE user_id = '{}';".format(user_id)
            sql_teamcon = "DELETE FROM team_connecting WHERE user_id = '{}';".format(user_id)
            mycursor.execute(sql_user, )
            mydb.commit()
            mycursor.execute(sql_teamcon, )
            mydb.commit()
            mydb.close()
            return jsonify({"message": "success_deleteUser", "status": 200})
        else:
            return jsonify({"message": "user id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR


def changePassword():
    try:
        user_id = request.args['user_id']
        if checkData("user_id",user_id,"user") == False:

            data_json = request.json

            if checkKey(data_json,key_list_password)['key_lost'] == []:
                old_password = data_json['old_password']
                new_password_1 = data_json['new_password_1']
                new_password_2 = data_json['new_password_2']
                mydb = connectsql()
                mycursor = mydb.cursor(dictionary=True)
                sql_user = "SELECT password FROM user WHERE user_id = '{}'".format(user_id)
                mycursor.execute(sql_user)
                user = mycursor.fetchone()
                mydb.close()
                check_password = verify_password(user['password'], old_password)
                if check_password == True:
                    if new_password_1 == new_password_2:
                        password_hashed = hash_password(new_password_1)
                        update = datetime.now()
                        mydb = connectsql()
                        mycursor = mydb.cursor(dictionary=True)

                        sql_user = """UPDATE user
                                            SET password='{}', date_update='{}'
                                            WHERE user_id = '{}';""".format(
                            password_hashed,
                            update,
                            user_id)
                        mycursor.execute(sql_user, )
                        mydb.commit()
                        mydb.close()
                        return jsonify({"message": "success_changePassword", "status": 200})
                    else:
                        return jsonify({"message": "new password incorrect", "status": 500})
                else:
                    return jsonify({"message": "old password incorrect", "status": 500})

            else:
                return jsonify({"message": "key_lost", "list": checkKey(data_json, key_list_password)['key_lost'], "status": 500})
        else:
            return jsonify({"message": "user id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR


def forgotPassword():
    try:
        user_id = request.args['user_id']
        if checkData("user_id",user_id,"user") == False:
            mydb = connectsql()
            mycursor = mydb.cursor(dictionary=True)
            sql_user = "SELECT email FROM user WHERE user_id = '{}'".format(user_id)
            mycursor.execute(sql_user, )
            user = mycursor.fetchone()
            mydb.close()
            if user != None:
                email = user['email']
                if email != "" :
                    new_password = createPassword()
                    password_hashed = hash_password(new_password)
                    update = datetime.now()
                    mydb = connectsql()
                    mycursor = mydb.cursor(dictionary=True)

                    sql_user = "UPDATE user SET password='{}', date_update='{}'WHERE user_id = '{}';".format(
                        password_hashed,
                        update,
                        user_id)
                    mycursor.execute(sql_user, )
                    mydb.commit()
                    mydb.close()

                    sendMail(email,new_password)
                    return jsonify({"message":"success_forgotPassword", "status": 200})
                return jsonify({"message":"email not found", "status": 500})
            return jsonify({"message":"user_id not found", "status": 500})
        else:
            return jsonify({"message": "user id not found", "status": 500})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR


def searchUser():
    try:
        value = request.args['keyword']
        mydb = connectsql()
        mycursor = mydb.cursor(dictionary=True)
        sql_user = """{}
                            WHERE 
                                u.user_id LIKE '%{}%' or
                                u.username LIKE '%{}%' or
                                u.firstname LIKE '%{}%' or
                                u.lastname LIKE '%{}%' or
                                u.position LIKE '%{}%' or
                                u.type LIKE '%{}%' or
                                u.tel LIKE '%{}%' or
                                u.email LIKE '%{}%' or
                                u.line LIKE '%{}%' or
                                u.nickname LIKE '%{}%' or
                                u.team_name LIKE '%{}%' ;""".format(sql_text,value,value,value,value,value,value,value,value,value,value,value)
        mycursor.execute(sql_user, )
        data = mycursor.fetchall()
        mydb.close()
        return jsonify({"message": data, "status": 200})
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno,e)
        return str(str(exc_type)+" "+str(fname)+" "+str(exc_tb.tb_lineno)+" "+str(e)) , status.HTTP_500_INTERNAL_SERVER_ERROR