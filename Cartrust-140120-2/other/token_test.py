from flask import Flask, jsonify, request, Response
import psutil
import redis
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
import time
import json
import base64
import sys
from functools import wraps
from Crypto.Random import get_random_bytes
cache = redis.Redis (host='127.0.0.1',port=6379)
ACCESS_EXPIRE_TIME = 60

app = Flask(__name__)

@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Expose-Headers','X-Token')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  return response


def json_response(messages=None, status=None, headers=None):
    if headers == None:
        headers = dict()
    headers.update({"Content-Type": "application/json"})
    contents = json.dumps(messages).replace('\'', '"')
    if (status == None):
        status = 200
    resp = Response(response=contents, headers=headers, status=int(status))
    return resp


def generate_rsa(bits=2048):
    new_key = RSA.generate(bits, e=65537)
    gen_public_key = new_key.publickey().exportKey("PEM")
    gen_private_key = new_key.exportKey("PEM")
    return gen_private_key, gen_public_key


private_key, public_key = generate_rsa()


def encryption_message(token_claim):
    public_key_object = RSA.importKey(public_key)
    random_phrase = "M"
    encrypted_message = public_key_object.encrypt(json.dumps(token_claim).replace('\'', '"'), random_phrase)[0]
    return base64.b64encode(encrypted_message)


def generate_access_token(username, password):
    issue = time.time()
    access_expire = str(issue + (ACCESS_EXPIRE_TIME * 60))
    access_token_claims = username + password + str(issue)
    access_token = encryption_message(access_token_claims)
    return access_token, access_expire


# def decryption_message(encrypted_message):
#     encrypted_message = base64.b64encode(encrypted_message)
#     private_key_object = RSA.importKey(private_key)
#     decrypted_message = private_key_object.decrypt(encrypted_message)
#     result = {}
#     try:
#         result = json.loads(decrypted_message)
#         return result
#     except:
#         return result
#
# def check_token(access_level):
#     def decorator(f):
#         @wraps(f)
#         def decorated_function(*args , **kwargs):
#             access_token = request.headers.get('Authorization')
#             print "request url:" + request.url
#             if access_token is not None:
#                 cache_access_token = cache.get("access_token :" + access_token )
#                 if cache_access_token is None:
#                     return "Unauthorization."
#                 else:
#                     print 'check_token : access still exist in memory.'
#                     token_data = decryption_message(access_token)
#                     if len(token_data) > 0 :
#                         if token_data['type'] == "access":
#                             if access_level == "admin" :
#                                 if token_data['role'] != "admin":
#                                     return "Permission Deny."
#                             return f(*args , **kwargs )
#                         else:
#                             return "Invalid Token"
#                     else:
#                             return "Invalid Token"
#             else:
#                 return "missing headers: Authorization"
#         return decorated_function
#     return decorator
